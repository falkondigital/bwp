<?php get_header(); ?>
<?php
global $page_mb;
$page_meta = $page_mb->the_meta();
if(has_post_thumbnail()){
  $jumbo_img_src = wp_get_attachment_image_src(get_post_thumbnail_id(),$size = 'full');//get_template_directory_uri() . '/images/banner-blog.jpg';
  if(is_array($jumbo_img_src))
    $jumbo_img_src = $jumbo_img_src[0];
}
else{
//    $blog_page_id = get_option('page_for_posts');
//    global $page_mb;
//    $page_meta = $page_mb->the_meta();
//    $page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
//    $page_title_tag_line = $page_meta['custom_title_tag_line']? $page_meta['custom_title_tag_line']: false;
  $jumbo_img_src = get_template_directory_uri() . '/images/banner-blog.jpg';
}
$page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
$page_title_tag_line = $page_meta['custom_title_tag_line']? $page_meta['custom_title_tag_line']: false;
if((int)$page_meta['image_id']!=0){
  $jumbo_img_src = wp_get_attachment_image_src((int)$page_meta['image_id'],$size = 'full');//get_template_directory_uri() . '/images/banner-blog.jpg';
  if(is_array($jumbo_img_src))
    $jumbo_img_src = $jumbo_img_src[0];
}

  $page_title = "404";
  $page_title_tag_line = "Page or resource not found";

?>
<div id="jumbotron" class="jumbotron jumbotron-fluid text-white" style="background-image: url('<?php echo $jumbo_img_src;?>'); background-position-x: center;">
  <div class="shadow"></div>
  <div class="container text-center">
    <div class="row">
      <div class="col-12 col-sm-8 offset-sm-2">
        <p class="h1 text-white"><?php echo $page_title;?></p>
      </div>
      <?php
      if($page_title_tag_line!==false){
        echo '<div class="w-100"></div>';
        echo '<div class="col-12">';
        echo '<p class="h3 text-white font-weight-normal header-meta">'.$page_title_tag_line.'</p>';
        echo '</div>';
      }
      ?>
    </div>
  </div>
</div>
<main class="container mt-6">
  <div class="row">
    <div class="col">
<div id="content" role="main">
  <div class="alert alert-warning">
    <h1>
      <i class="fas fa-exclamation-triangle"></i> <?php _e('Error', 'b4st'); ?> 404
    </h1>
    <p class="mt-4"><?php _e('Sorry, we can&rsquo;t find what you were looking for.', 'b4st'); ?></p>
  </div>
</div><!-- /#content -->
    </div>
  </div>
</main>
<?php include(locate_template('includes/blocks/final-cta.php'));?>
<?php get_footer(); ?>
