<?php
/*
YARPP Template: YARPP Custom Falkon (vertical)
Author: ChunkySteveo (Stephen Ludgate)
Description: Custom YARPP template for Falkon Digital, blog posts which are displayed vertically.
*/
global $post;
?>
<div class="bg-light py-8" id="yarpp-related-blog">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <p class="h2 mb-6 text-center font-weight-light">Other posts you might be interested in...</p>
            </div>
        </div>
        <div class="blog-list padd-bottom" id="related-posts">
            <div class="row blog-list-vertical">
                <?php if (have_posts()):?>
                    <?php
                    $xyz = 1;
                    while ( have_posts() ) : the_post();
                    include(locate_template('includes/loops/loop-post-vertical.php'));
                    endwhile;
                    wp_reset_postdata();
                ?>
                <?php else: ?>
                    <?php
                    $args = array(
//                        'post__not_in' => array($post->ID),
                        'posts_per_page'    => 3,
                        'orderby' => 'rand',
//                        'order'   => 'ASC',
                        'ignore_sticky_posts'   => true,
                        );
                    $last_posts = new WP_Query($args);
                    $xyz = 1;
                    while ( $last_posts->have_posts() ) : $last_posts->the_post();
                    include(locate_template('includes/loops/loop-post-vertical.php'));
                    endwhile;
                    wp_reset_postdata();
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
