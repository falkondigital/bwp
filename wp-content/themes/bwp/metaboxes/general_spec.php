<?php
add_action('do_meta_boxes', 'remove_feat_image_box');

function remove_feat_image_box() {
	remove_meta_box( 'postimagediv', 'page', 'side' );
}

if ( is_admin()) add_action( 'admin_enqueue_scripts', 'my_metabox_styles_page' );
function my_metabox_styles_page()
{
//	if ( 'page' == $_POST['post_type'] && !current_user_can( 'ams_edit_pages', $post_id )) {
//		return $post_id;
//	}
	/*else*/if( 'page' == get_post_type( $post_id) or  'case-study' == get_post_type( $post_id)){
		wp_enqueue_style( 'wpalchemy-metabox-page', get_stylesheet_directory_uri() . '/metaboxes/meta-page.css');
		wp_enqueue_script('thickbox');
		wp_enqueue_style('thickbox');
		wp_enqueue_script('media-upload');
		wp_enqueue_media();
		wp_enqueue_script( 'jquery-page-upload', get_template_directory_uri() .'/js/page-upload.js', array('jquery','media-upload','thickbox') );
		wp_localize_script( 'jquery-page-upload', 'jquery_page_upload', array( 'thumbnail_url' => get_template_directory_uri().'/images/default-page.jpg' ) );

		wp_enqueue_script('jquery-frontpage-upload', get_template_directory_uri() . '/js/frontpage-upload.js', array('jquery', 'media-upload', 'thickbox'));
		wp_localize_script('jquery-frontpage-upload', 'jquery_frontpage_upload', array('thumbnail_url' => get_template_directory_uri() . '/images/default-frontpage.jpg'));
	}
}
$wpalchemy_media_access = new WPAlchemy_MediaAccess();
//var_dump($wpalchemy_media_access);

$page_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_general_meta',
	'title' => 'General Page Meta Information',
	'types' => array('page','case-study'), // added only for pages and to custom post type "events"
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'template' => get_stylesheet_directory() . '/metaboxes/general-meta.php',
//	'mode' => WPALCHEMY_MODE_EXTRACT,
	'exclude_post_id'   =>  get_option('page_on_front'),
//	'prefix' => '_page_meta_'
));