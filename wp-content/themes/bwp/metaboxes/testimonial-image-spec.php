<?php

function save_filter_testimonial_image($meta, $post_id){
	//$footMax = $_POST['property_proptype'];
	$tempArray = $_POST['_property_meta'];
	//echo 'sdfds = '.$tempArray['footmax'];
	//print_r($meta);
	//echo '<br>';
	//echo $post_id;
	//echo '<br>';	
	
	
//	if(!$meta['footmax'] or $meta['footmax'] == '' or $meta['footmax'] == 0){
//		//echo $meta['footmin'];
//		$meta['footmax'] = $meta['footmin'];
//
//		//update_post_meta($post_id, '_com_footmax', $tempArray['footmin']);
//	}
//	if(!$meta['footmin'] or $meta['footmin'] == '' or $meta['footmin'] == 0){
//		//echo $meta['footmin'];
//		$meta['footmin'] = $meta['footmax'];
//
//		//update_post_meta($post_id, '_com_footmax', $tempArray['footmin']);
//	}
	//print_r($meta);
	//exit;
	return $meta;
}

function save_action_testimonial_image($meta, $post_id) {
  	// Check permissions
  	if ( 'testimonial' == $_POST['post_type'] && !current_user_can( 'edit_post', $post_id )) {
      		return $post_id;
  	}
 
// 	$ourtaxonomy = $_POST['testimonial_proptype'];
//	wp_set_object_terms( $post_id, $ourtaxonomy, 'proptype' );
//
//	$ourtaxonomy = $_POST['testimonial_propsize'];
//	wp_set_object_terms( $post_id, $ourtaxonomy, 'propsize' );
//
//	$ourtaxonomy = $_POST['testimonial_propsell'];
//	wp_set_object_terms( $post_id, $ourtaxonomy, 'propsell' );
//
//
	//return $ourtaxonomy;
}




if ( is_admin()) add_action( 'admin_enqueue_scripts', 'my_metabox_styles_testimonial' );
function my_metabox_styles_testimonial()
{
	wp_enqueue_style( 'wpalchemy-metabox-artiste', get_stylesheet_directory_uri() . '/metaboxes/meta-artiste.css');
	wp_enqueue_style( 'wpalchemy-metabox-testimonial', get_stylesheet_directory_uri() . '/metaboxes/meta-testimonial.css');
	wp_enqueue_script('thickbox');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('media-upload');
	wp_enqueue_script('wptuts-upload');
	wp_enqueue_media();
	wp_enqueue_script( 'jquery-testimonial-upload', get_template_directory_uri() .'/js/testimonial-upload.js', array('jquery','media-upload','thickbox') );

	wp_localize_script( 'jquery-testimonial-upload', 'jquery_testimonial_upload', array( 'thumbnail_url' => get_template_directory_uri().'/images/default-thumbnail.jpg' ) );
}

$testimonial_image_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_testimonial_image_meta',
	'title' => 'Testimonial Image',
	'types' => array('testimonial'), // added only for pages and to custom post type "events"
	'context' => 'side', // same as above, defaults to "normal"
	'priority' => 'low', // same as above, defaults to "high"
	//'init_action' => 'remove_default_proptypediv',
//	'save_filter' => 'save_filter_testimonial_image', // defaults to NULL
//	'save_action' => 'save_action_testimonial_image',
	'template' => get_stylesheet_directory() . '/metaboxes/testimonial-image-meta.php',
//	'mode' => WPALCHEMY_MODE_EXTRACT,
	'prefix' => '_com_'
));

/* eof */