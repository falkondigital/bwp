<div class="my_meta_control">
<!--    --><?php //var_dump($mb->meta);?>
	<p>Custom fields for all the listings.</p>
    <table class="form-table">
        <tbody>
            <?php /* ?><tr>
                <th scope="row"> <label>Featured:</label></th>
                <td><?php $mb->the_field('featured'); ?>
                    <label for="<?php $mb->the_name(); ?>"><input type="checkbox"id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> Tick to make the listing 'featured'</label>
                    <p class="description">Tick to make the listing featured.</p></td>
            </tr>
            <tr>
                <th scope="row"> <label>Coming Soon:</label></th>
                <td><?php $mb->the_field('coming_soon'); ?>
                    <label for="<?php $mb->the_name(); ?>"><input type="checkbox"id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> Tick to make the listing 'coming soon'</label>
                    <p class="description">Tick to make the listing show as coming soon. It will show a 'Coming Soon' tag on the image instead of the low price and include info in the description.</p></td>
            </tr><?php */ ?>
            <?php /* ?><tr>
                <?php
                $taxonomy = 'transmission';
                $your_taxonomy = get_taxonomy($taxonomy);
                $args = array(	'hide_empty' => 0,);
                $yourtaxonomies = get_terms($taxonomy,$args);
                ?>
                <th scope="row"><label for="<?php echo get_post_type().'_'.$taxonomy;?>"><?php _e($your_taxonomy->labels->singular_name,'mathsiscool');?>:</label></th>
                <td>
                    <?php
                    $names = wp_get_object_terms($post->ID, $taxonomy,array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'slugs'));
                    ?>
                    <select multiple="multiple" aria-multiselectable="true" name="<?php echo get_post_type().'_'.$taxonomy;?>[]" id="<?php echo get_post_type().'_'.$taxonomy;?>">

                        <option class="<?php echo $taxonomy;?>-option" value="" <?php if (!count($names)) echo "selected";?>><?php echo __('None', 'comsite') ?></option>
                        <?php
                        foreach ($yourtaxonomies as $yourtaxonomy) {
                            if ($post->post_type == 'listing') //Add check because this function will also get called on post revision objects, and will result in double counting the newly added taxonomy relationship.
                                echo '<option class="'.$taxonomy.'-option" value="' . $yourtaxonomy->slug . '" '. ( !is_wp_error($names) && !empty($names) && in_array($yourtaxonomy->slug, $names)?'selected':'') .'>' . $yourtaxonomy->name . '</option>\n';
                        }
                        ?>
                    </select>
                </td>
            </tr><?php */ ?>
            <?php /* ?><tr>
                <th scope="row"> <label>Automatic:</label></th>
                <td><?php $mb->the_field('automatic'); ?>
                    <label for="<?php $mb->the_name(); ?>"><input type="checkbox"id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> Tick to make the listing 'automatic'</label>
                    <p class="description">Tick to make the listing automatic transmission. It will show up on if you use the filter, and will also have a automatic info over the image and in the description.</p></td>
            </tr><?php */ ?>
            <tr>
                <?php $mb->the_field('listing_price'); ?>
                <th scope="row"><label for="<?php $mb->the_id(); ?>">List Price: (listing_price)</label></th>
                <td class="cost-from">
                    <span class="pound">£</span><input type="text" class="normal" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id="<?php $mb->the_id(); ?>" maxlength="10" />
                    <p class="description">List price, do not enter any pound (£) symbols. <em>(this is the sale price value the user has set to sell the bike for in the listing)</em></p></td>
            </tr>
            <tr>
                <?php $mb->the_field('listing_mileage'); ?>
                <th scope="row"><label for="<?php $mb->the_name(); ?>"><?php _e('Mileage (Total)','voozodealer');?>: (listing_mileage)</label></th>
                <td>
                    <input type="text"
                           class="text-small"
                           name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id="<?php $mb->the_name(); ?>"
                           maxlength="10" />
                </td>
            </tr>
            <tr>
                <?php $mb->the_field('listing_date_end'); ?>
                <th scope="row"><label for="<?php $mb->the_name(); ?>"><?php _e('Listing End Date','voozodealer');?>: (listing_date_end)</label></th>
                <td>
                    <?php echo '<p>'.date(get_option('date_format'),$mb->get_the_value()).' '.date(get_option('time_format'),$mb->get_the_value());?></p>
                    <input type="text"
                           class="text-small"
                           name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id="<?php $mb->the_name(); ?>"
                           maxlength="10" />
                </td>
            </tr>
            <tr>
                <?php $mb->the_field('listing_duration'); ?>
                <th scope="row"><label for="<?php $mb->the_name(); ?>"><?php _e('Listing Duration','voozodealer');?>: (listing_duration)</label></th>
                <td>
                    <select name="<?php $mb->the_name(); ?>" id="<?php $mb->the_name(); ?>" class="form-control">
                        <option value="">Listing Duration...</option>
                        <option value="1" <?php selected($mb->get_the_value(),'1');?>>1 week</option>
                        <option value="2" <?php selected($mb->get_the_value(),'2');?>>2 weeks</option>
                        <option value="3" <?php selected($mb->get_the_value(),'3');?>>3 weeks</option>
                        <option value="4" <?php selected($mb->get_the_value(),'4');?>>1 month (max)</option>
                    </select>
                </td>
            </tr>
            <tr>
                <?php $mb->the_field('autorelist'); ?>
                <th scope="row"><label for="<?php $mb->the_name(); ?>X"><?php _e('Auto Re-list Listing','voozodealer');?>: (autorelist)</label></th>
                <td>
                    <?php
                        printf('<label for="%1$s"><input type="checkbox" name="%s" value="%s" id="%1$s" %s/> %s </label>', $mb->get_the_name(),'1', $mb->get_the_checkbox_state('1'),"Auto Re-list");
                    ?>
                </td>
            </tr>

            <?php
            $taxonomy = 'engine';
            $your_taxonomy = get_taxonomy($taxonomy);
            if($your_taxonomy!==false){ ?>
            <tr>
                <?php
                $args = array(	'hide_empty' => 0,);
                $yourtaxonomies = get_terms($taxonomy,$args);
                ?>
                <th scope="row"><label for="<?php echo get_post_type().'_'.$taxonomy;?>"><?php _e($your_taxonomy->labels->singular_name,'mathsiscool');?>:</label></th>
                <td>

                    <select name="<?php echo get_post_type().'_'.$taxonomy;?>" id="<?php echo get_post_type().'_'.$taxonomy;?>">
                        <?php
                        $names = wp_get_object_terms($post->ID, $taxonomy);
                        ?>
                        <option class="<?php echo $taxonomy;?>-option" value="" <?php if (!count($names)) echo "selected";?>><?php echo __('None', 'comsite') ?></option>
                        <?php
                        foreach ($yourtaxonomies as $yourtaxonomy) {
                            if ($post->post_type == 'listing') //Add check because this function will also get called on post revision objects, and will result in double counting the newly added taxonomy relationship.
                                echo '<option class="'.$taxonomy.'-option" value="' . $yourtaxonomy->slug . '" '. ( !is_wp_error($names) && !empty($names) && !strcmp($yourtaxonomy->slug, $names[0]->slug)?'selected':'') .'>' . $yourtaxonomy->name . '</option>\n';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <?php } ?>
            <?php /* ?><tr>
                <th scope="row"> <label>Luxury:</label></th>
                <td><?php $mb->the_field('luxury'); ?>
                    <label for="<?php $mb->the_name(); ?>"><input type="checkbox"id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> Tick to make the listing 'luxury'</label>
                    <p class="description">Tick to make the listing luxury. It will show up on if you use the filter.</p></td>
            </tr><?php */ ?>
            <?php
            $taxonomy = 'style';
            $your_taxonomy = get_taxonomy($taxonomy);
            if($your_taxonomy!==false){ ?>
            <tr>
                <?php
                $args = array(	'hide_empty' => 0,);
                $yourtaxonomies = get_terms($taxonomy,$args);
                ?>
                <th scope="row"><label for="<?php echo get_post_type().'_'.$taxonomy;?>"><?php _e($your_taxonomy->labels->singular_name,'mathsiscool');?>:</label></th>
                <td>
                    <?php
                    $names = wp_get_object_terms($post->ID, $taxonomy,array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'slugs'));
                    ?>
                    <select name="<?php echo get_post_type().'_'.$taxonomy;?>" id="<?php echo get_post_type().'_'.$taxonomy;?>">
                        <option class="<?php echo $taxonomy;?>-option" value="" <?php if (!count($names)) echo "selected";?>><?php echo __('None', 'comsite') ?></option>
                        <?php
                        foreach ($yourtaxonomies as $yourtaxonomy) {
                            if ($post->post_type == 'listing') //Add check because this function will also get called on post revision objects, and will result in double counting the newly added taxonomy relationship.
                                echo '<option class="'.$taxonomy.'-option" value="' . $yourtaxonomy->slug . '" '. ( !is_wp_error($names) && !empty($names) && in_array($yourtaxonomy->slug, $names)?'selected':'') .'>' . $yourtaxonomy->name . '</option>\n';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <?php } ?>
            <tr>
                <?php $mb->the_field('related_items'); ?>
                <?php
                $custom_post = 'listing';
                $custom_post_object = get_post_type_object( $custom_post );
                $custom_post_name = $custom_post_object->labels->name;
                ?>
                <th scope="row">
                    <label for=""><?php _e('Related '.$custom_post_object->labels->name,'mathsiscool');?>:</label>
                    <a href="#" id="checkall-<?php echo $custom_post;?>" title="Check or uncheck all <?php echo $custom_post_name;?>">Check/Uncheck All</a>
                    <script type="text/javascript">
                        jQuery( document ).ready(function($) {
                            $("#checkall-<?php echo $custom_post;?>").click(function() {
                                var checkBoxes = $('#autochecklist-<?php echo $custom_post;?> .autocheck');
                                checkBoxes.prop("checked", !checkBoxes.prop("checked"));
                                return false;
                            });
                            return false;
                        });
                    </script>
                </th>
                <td>
                    <?php
                    $original_post = $post;
                    $args = array(
                        'post_type' => $custom_post,
                        'posts_per_page' => -1,
                        'post__not_in' => array( get_the_ID()),
                        'orderby'   => 'menu_order title'
                    );
                    $item_posts = get_posts( $args );

                    if ( count($item_posts)>0 ) :
                        echo '<div id="autochecklist-'.$custom_post.'"><ul class="">';
                        foreach( $item_posts as $post_rel ) :
                            setup_postdata($post_rel);
                            echo sprintf('<li><label for="%s">', $post_rel->ID );
                            echo sprintf('<input class="autocheck" type="checkbox" name="%s[]" value="%s" id="%2$s" %s/> %s</label></li>', $mb->get_the_name(), $post_rel->ID, $mb->get_the_checkbox_state($post_rel->ID),get_the_title($post_rel->ID) );
                        endforeach;
                        echo '</ul></div>';
                    else:
                        echo '<p>'.$custom_post_object->labels->not_found.'. <a href="'.admin_url('post-new.php?post_type='.$custom_post).'" target="_blank">Add one?</a></p>';
                    endif;
                    setup_postdata( $original_post);
                    ?>
                </td>
            </tr>
            <?php /* ?><tr>
                <?php $mb->the_field('berth'); ?>
                <th scope="row"><label for="">berth</label></th>
                <td>
                    <input type="text" readonly="readonly" aria-readonly="true" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" />
                    <br /><span class="description">berth for sorting posts by post meta 'berth' which matches taxonomies. Don't touch, it will populate on save to match Berth above.</span>
                </td>
            </tr><?php */ ?>
        </tbody>
    </table>
    <hr>
    <table class="form-tablex" width="100%">
    <?php
    foreach($mb->meta as $key=> $value){
        if($key == 'listing_mileage'
        or $key == 'listing_price'
        or $key == 'listing_date_end'
        or $key == 'listing_duration'
        or $key == 'autorelist'
        )
            continue;
        echo '<tr>';
        echo '<td>'.$key.':</td><td><input readonly="readonly" type="text" value="'.$value.'" name="_listing_meta['.$key.']"></td>';
        echo '</tr>';
    }
    ?>
    </table>

</div>