<div class="slide_meta_control">
    <table class="form-table">
        <tbody>
        <tr>
            <?php $mb->the_field('title_line'); ?>
            <th scope="row"><label for="title_line">Title Line:</label></th>
            <td>
                <input type="text"
                       class="widefat"
                       name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id="title_line"
                       maxlength="150" />
                <p class="description">Title line of the slide, seen in the top row.</p>
            </td>
        </tr>
        <?php /* ?><tr>
            <?php $mb->the_field('tag_line'); ?>
            <th scope="row"><label for="tag_line">Tag Text:</label></th>
            <td>
                <input type="text"
                       class="widefat"
                       name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id="title_line"
                       maxlength="150" />
                <p class="description">Slide tag text, seen below the title text, in smaller font.</p>
            </td>
        </tr><?php */ ?>
        <tr>
            <?php $mb->the_field('tag_line'); ?>
            <th scope="row"><label for="tag_line">Description Text:</label></th>
            <td>
                <?php
                $content = html_entity_decode($mb->get_the_value(),ENT_QUOTES, 'UTF-8');
                $id = sanitize_key($mb->get_the_name());//Needs to not have [] in the id, only letters and underscores
                $settings = array(
                    'quicktags' => array(
                        'buttons' => 'em,strong',
                    ),
                    /*'quicktags' => true,*/
                    'tinymce' => true,
                    'media_buttons'	=> false,
                    'textarea_name'	=> $mb->get_the_name(),
                    'textarea_rows'	=> 3,
                    'teeny'			=> true,

                );
                wp_editor($content, $id, $settings);
                ?>
                <?php /* ?><textarea id="tag_line" class="widefat" name="<?php $mb->the_name(); ?>" rows="2"><?php $mb->the_value(); ?></textarea><?php */ ?>
                <p class="description">Slide description text, seen below the title text, in smaller font.</p>
            </td>
        </tr>
        <tr>
            <?php $mb->the_field('button_ghost_text'); ?>
            <th scope="row"><label for="button_text">Ghost Button Text:</label></th>
            <td>
                <input type="text"
                       class="normal"
                       name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id="button_text"
                       maxlength="150" />
                <p class="description">Text for the ghost button on the slide. Leave blank to not display the button.</p>
            </td>
        </tr>
        <tr>
            <?php $mb->the_field('link_button_ghost'); ?>
            <th scope="row"><label for="<?php $mb->the_id(); ?>">Ghost Button Link?:</label></th>
            <td>
                <input type="text"
                       class="widefat"
                       name="<?php $mb->the_name(); ?>" value="<?php echo esc_url($mb->get_the_value()); ?>" id="<?php $mb->the_id(); ?>"
                       maxlength="150" />
                <p class="description">Enter a full URL link to the box above to make the button act as a link.</p>
            </td>
        </tr>
        <tr>
            <?php $mb->the_field('button_text'); ?>
            <th scope="row"><label for="button_text">Button Text:</label></th>
            <td>
                <input type="text"
                       class="normal"
                       name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id="button_text"
                       maxlength="150" />
                <p class="description">Text for the button on the slide. Leave blank to not display the button.</p>
            </td>
        </tr>
        <tr>
            <?php $mb->the_field('link_button'); ?>
            <th scope="row"><label for="<?php $mb->the_id(); ?>">Button Link?:</label></th>
            <td>
                <input type="text"
                       class="widefat"
                       name="<?php $mb->the_name(); ?>" value="<?php echo esc_url($mb->get_the_value()); ?>" id="<?php $mb->the_id(); ?>"
                       maxlength="150" />
                <p class="description">Enter a full URL link to the box above to make the button act as a link.</p>
            </td>
        </tr>
<!--        <tr>-->
<!--            --><?php //$mb->the_field('link'); ?>
<!--            <th scope="row"><label for="--><?php //$mb->the_id(); ?><!--">Slide Link?:</label></th>-->
<!--            <td>-->
<!--                <input type="text"-->
<!--                       class="widefat"-->
<!--                       name="--><?php //$mb->the_name(); ?><!--" value="--><?php //echo esc_url($mb->get_the_value()); ?><!--" id="--><?php //$mb->the_id(); ?><!--"-->
<!--                       maxlength="150" />-->
<!--                <p class="description">Enter a full URL link to the box above to make the whole slide clickable.</p>-->
<!--            </td>-->
<!--        </tr>-->
        <tr>
            <?php $mb->the_field('image_id'); ?>
            <?php $max_image_num = 1;?>
            <th scope="row"><label for="<?php $mb->the_id(); ?>">Upload Image:</label></th>
            <td id="slide-upload-td">
                <input id="upload_slide_button" type="button" class="button alignleft" value="Upload Image" />
                <a title="Delete Image" class="flkn-delete-slide submitdelete alignright" href="#" >Remove Image</a>
                <br><p class="description">Upload custom images for use in the slide. Ideal size is 1900x700 pixels (and a nice dark image as the text is white!). You can choose <?php echo $max_image_num._n(' image',' images',$max_image_num);?>.
                    If you do not choose an image the default image will be used. See preview below.</p>
            </td>
        </tr>
        <?php /* ?><tr>
            <?php $mb->the_field('sap_blue'); ?>
            <th scope="row"><label for="sap_blue">Make slide 'SAP Blue':</label></th>
            <td><label for="sap_blue"><input type="checkbox" id="sap_blue" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> Make slide colours 'SAP Blue'.</label><br/>
                <p class="description">Tick to make the slide have 'SAP blue' colours instead of the default greens.</p></td>
        </tr><?php */ ?>
        <?php /* ?><tr>
            <?php $mb->the_field('box_side');
            if(!$mb->get_the_value() or is_null($mb->get_the_value()))
                $mb->meta[$mb->name] = 'left';
            ?>
            <th scope="row"><label for="">Slide Text Box Side?:</label></th>
            <td>
                <?php $feature_cats = array('left', 'right'); ?>
                <?php foreach ($feature_cats as $i => $feature_cat): ?>
                <label for="radio-<?php echo $feature_cat;?>"><input type="radio" name="<?php $mb->the_name(); ?>" id="radio-<?php echo $feature_cat;?>" value="<?php echo $feature_cat; ?>"<?php $mb->the_radio_state($feature_cat); ?>/> <?php echo $feature_cat; ?></label>
                <?php endforeach; ?>
                <p class="description">Move the slide text box either left or right depending on your image. Default is left.</p>
            </td>
        </tr><?php */ ?>
        </tbody>
    </table>

    <?php $mb->the_field('image_id'); ?>
    <?php
    $default_thumbnail_slide = get_template_directory_uri().'/images/default-slider.jpg';
    $img_src = '<img src="'.$default_thumbnail_slide.'" class="slideimg default-image">';
    $image_id_val = '';
    if($mb->get_the_value()!=null){
        $image_url = wp_get_attachment_image_src( $mb->get_the_value(), 'slide-image-full' );
        $img_src = '<img src="'.$image_url[0].'" class="slideimg">';
        $image_id_val = $mb->get_the_value();
    }
    echo '<input type="hidden" id="image_id" name="'.$mb->get_the_name().'" value="'.$mb->get_the_value().'"/>';
    ?>

    <div id="slide-preview" data-max_file_uploads="<?php echo $max_image_num;?>">
        <?php echo $img_src;?>
        <?php //if($mb->get_the_value('title_line')!='' or $mb->get_the_value('tag_line')!=''){ ?>
        <div class="slide-tag <?php echo 'tagbox-'.$mb->get_the_value('box_side');?><?php echo ($mb->get_the_value('sap_blue')=='1'?' sap-blue':'')?>">
            <div class="banner-title"><?php echo htmlspecialchars_decode($mb->get_the_value('title_line'));?></div>
            <div class="banner-text"><?php echo htmlspecialchars_decode($mb->get_the_value('tag_line'));?></div>
            <?php /* ?><div class="banner-desc"><?php echo htmlspecialchars_decode($mb->get_the_value('desc'));?></div><?php */ ?>
            <?php
            if($mb->get_the_value('button_text')!='' and $mb->get_the_value('link')!=''){
                echo '<div class="btn-slide" >'.htmlspecialchars_decode($mb->get_the_value('button_text')).'</div>';
            }
            ?>
        </div>
        <?php //} ?>
    </div>



</div>