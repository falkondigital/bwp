<div class="my_meta_control">
	<label>Linked In URL</label>

	<p>
    <span>Enter URL for Linked In profile</span>
		<input type="text" name="<?php $metabox->the_name('linkedin'); ?>" value="<?php $metabox->the_value('linkedin'); ?>" id="testinput" maxlength="255"/>

	</p>
	<?php /* ?><label>Job position</label>

	<p>
    <span>Enter job position/title, (e.g. Director, PA)</span>
		<input type="text" name="<?php $metabox->the_name('jobrole'); ?>" value="<?php $metabox->the_value('jobrole'); ?>" id="testinput" maxlength="50"/>

	</p>

    <label>Education</label>

	<p>
    <span>Enter degree etc, (e.g. BSC (HONS) MRICS)</span>
		<input type="text" name="<?php $metabox->the_name('jobedu'); ?>" value="<?php $metabox->the_value('jobedu'); ?>" id="testinput" maxlength="50"/>

	</p><?php */ ?>
    
    <label>Email Address</label><br/>

	<?php while($mb->have_fields_and_multi('email_group')): ?>
	<?php $mb->the_group_open(); ?>

		<?php $mb->the_field('jobemail'); ?>
		<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" class="short"/>
		<?php $mb->the_field('cb_jobemail'); ?>
		<input type="checkbox" name="<?php $mb->the_name(); ?>" value="yes"<?php $mb->the_checkbox_state('yes'); ?>/> visible on website?<br/>
		

	<?php $mb->the_group_close(); ?>
	<?php endwhile; ?>

	<p><a href="#" class="docopy-email_group button">Add another email</a></p>
    
    <label>Phone Number</label><br/>

	<?php while($mb->have_fields_and_multi('phone_group')): ?>
	<?php $mb->the_group_open(); ?>
		<?php $mb->the_field('s_jobphone'); ?>
        <select name="<?php $mb->the_name(); ?>">
        	<option value="">Select...</option>
            <option value="Tel"<?php $mb->the_select_state('Tel'); ?>>Tel</option>
            <option value="DDI"<?php $mb->the_select_state('DDI'); ?>>DDI</option>
            <option value="Mob"<?php $mb->the_select_state('Mob'); ?>>Mob</option>
        </select>
		<?php $mb->the_field('jobphone'); ?>
		<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" class="short"/>
        <?php $mb->the_field('cb_jobphone'); ?>
		<input type="checkbox" name="<?php $mb->the_name(); ?>" value="yes"<?php $mb->the_checkbox_state('yes'); ?>/> visible on website?<br/>

		

	<?php $mb->the_group_close(); ?>
	<?php endwhile; ?>

	<p><a href="#" class="docopy-phone_group button">Add another number</a></p>
    
    
    <?php /* ?>
    <label>Funny Tag Line</label>
 	<span>Enter a funny finishing line, (e.g. Chris is very tidy!!!!)</span>
	<p>
		<input type="text" name="<?php $metabox->the_name('jobtag'); ?>" value="<?php $metabox->the_value('jobtag'); ?>" id="testinput" maxlength="100"/>

	</p>

    <label>Likes</label><br/>

	<?php while($mb->have_fields_and_multi('like_group')): ?>
	<?php $mb->the_group_open(); ?>

		<?php $mb->the_field('like'); ?>
		<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" class="short"/>


	<?php $mb->the_group_close(); ?>
	<?php endwhile; ?>

	<p><a href="#" class="docopy-like_group button">Add another like</a></p>

    <label>Dislikes</label><br/>

	<?php while($mb->have_fields_and_multi('dislike_group')): ?>
	<?php $mb->the_group_open(); ?>

		<?php $mb->the_field('dislike'); ?>
		<input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" class="short"/>


	<?php $mb->the_group_close(); ?>
	<?php endwhile; ?>

	<p><a href="#" class="docopy-dislike_group button">Add another dislike</a></p><?php */ ?>
    
    
    <input type="submit" class="button-primary" name="save" value="Save (Same as Update)">

</div>