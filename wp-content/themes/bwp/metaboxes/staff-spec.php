<?php

$staff_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_staff_meta',
	'title' => 'Staff Information',
	'types' => array('staff'), // added only for pages and to custom post type "events"
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'template' => get_stylesheet_directory() . '/metaboxes/staff-meta.php'
));

/* eof */