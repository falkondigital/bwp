<?php

function save_filter_testimonial($meta, $post_id){
	//$footMax = $_POST['property_proptype'];
	$tempArray = $_POST['_property_meta'];
	//echo 'sdfds = '.$tempArray['footmax'];
	//print_r($meta);
	//echo '<br>';
	//echo $post_id;
	//echo '<br>';	
	
	
//	if(!$meta['footmax'] or $meta['footmax'] == '' or $meta['footmax'] == 0){
//		//echo $meta['footmin'];
//		$meta['footmax'] = $meta['footmin'];
//
//		//update_post_meta($post_id, '_com_footmax', $tempArray['footmin']);
//	}
//	if(!$meta['footmin'] or $meta['footmin'] == '' or $meta['footmin'] == 0){
//		//echo $meta['footmin'];
//		$meta['footmin'] = $meta['footmax'];
//
//		//update_post_meta($post_id, '_com_footmax', $tempArray['footmin']);
//	}
	//print_r($meta);
	//exit;
	return $meta;
}

function save_action_testimonial($meta, $post_id) {
  	// Check permissions
  	if ( 'testimonial' == $_POST['post_type'] && !current_user_can( 'edit_post', $post_id )) {
      		return $post_id;
  	}

    $args = array(
        'numberposts'       =>  -1,
        'posts_per_page'	=>	-1,
        'paged'             =>  false,
        'post_type'         =>  'testimonial',
        'post_status'	    =>  'publish',
        'orderby'		    =>  'menu_id date',
        'order'             =>  'ASC',
    );
    $the_query = new WP_Query( $args );

//    var_dump($the_query->found_posts); exit;
    update_option('canning_testimonials_num',$the_query->found_posts);
 
// 	$ourtaxonomy = $_POST['testimonial_proptype'];
//	wp_set_object_terms( $post_id, $ourtaxonomy, 'proptype' );
//
//	$ourtaxonomy = $_POST['testimonial_propsize'];
//	wp_set_object_terms( $post_id, $ourtaxonomy, 'propsize' );
//
//	$ourtaxonomy = $_POST['testimonial_propsell'];
//	wp_set_object_terms( $post_id, $ourtaxonomy, 'propsell' );
//
//
	//return $ourtaxonomy;
}

add_action( 'save_post', 'custom_post_type_title_testimonial' ); //if post title = 'notitle' set it to ''
function custom_post_type_title_testimonial ( $post_id ) {
	$post_type = get_post_type( $post_id );
	if ( $post_type == 'testimonial') {
		$post_title = get_the_title($post_id);
		//var_dump($post_title);
		//	var_dump($post);
		//exit;
		global $wpdb;
		global $post;
		if($post_title==''){

			$post_name = substr(md5(rand()), 0, 6);
			$post_title = '';
			$where = array( 'ID' => $post_id );
			remove_action('save_post', 'custom_post_type_title_testimonial');
			$wpdb->update( $wpdb->posts, array( 'post_title' => $post_name, 'post_name' => $post_name), $where ); //can set post name/url here based on other meta data?
			add_action( 'save_post', 'custom_post_type_title_testimonial' );
		}
		elseif ( strtolower($post_title) == 'auto draft' and $post!=NULL or strtolower($post_title) == 'auto-draft' and $post!=NULL) {

			$post_name = $post->post_name;

			//if (strlen(stristr($post_name,$post_title))>0) { //generate a post_name
			$post_name = sanitize_title($post_type.'-'.$post_id);
			//}

			$post_title = '';
			$where = array( 'ID' => $post_id );

			remove_action('save_post', 'custom_post_type_title_testimonial');
			$wpdb->update( $wpdb->posts, array( 'post_title' => $post_name, 'post_name' => $post_name), $where ); //can set post name/url here based on other meta data?
			add_action( 'save_post', 'custom_post_type_title_testimonial' );
		}
		elseif($post_title!=''){
			$post_name = get_the_title($post_id);
			// var_dump($post_name);

			//if (strlen(stristr($post_name,$post_title))>0) { //generate a post_name
			$post_name = sanitize_title($post_name);
			//}

			$post_title = '';
			$where = array( 'ID' => $post_id );

			remove_action('save_post', 'custom_post_type_title_testimonial');
			$wpdb->update( $wpdb->posts, array( 'post_title' => $post_name, 'post_name' => $post_name), $where ); //can set post name/url here based on other meta data?
			add_action( 'save_post', 'custom_post_type_title_testimonial' );

			$post_name = $post->post_name;
			//	 var_dump($post_name);
			// exit;
		}
	}
}

add_action ( 'trash_post', 'do_trash_post_testimonial' ); // Set status to trash when post is trashed
function do_trash_post_testimonial($post_id) {
	if ( get_post_type( $post_id ) == 'testimonial' and !get_the_title($post_id)) {
		global $wpdb;
		$post_status = 'trash';
		$where = array( 'ID' => $post_id );
		$wpdb->update( $wpdb->posts, array( 'post_status' => $post_status), $where );
	}
}

add_action ( 'untrash_post', 'do_trash_post_testimonial' ); // Set status to whatever it was before it was in the trash

$testimonial_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_testimonial_meta',
	'title' => 'Testimonial Information',
	'types' => array('testimonial'), // added only for pages and to custom post type "events"
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	//'init_action' => 'remove_default_proptypediv',
	'save_filter' => 'save_filter_testimonial', // defaults to NULL
	'save_action' => 'save_action_testimonial',
	'template' => get_stylesheet_directory() . '/metaboxes/testimonial-meta.php',
	'mode' => WPALCHEMY_MODE_EXTRACT,
	'prefix' => '_terstimonial_'
));

/* eof */