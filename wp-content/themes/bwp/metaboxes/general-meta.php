<div class="banner_meta_control">
	<?php //var_dump($post);?>
	<?php //global $page_mb; ?>
	<?php //var_dump($mb->the_meta());?>
	<table class="form-table">
		<tbody>
		<?php
		//        $ancestors   = get_post_ancestors( $post->ID );
		//        $root = count( $ancestors ) - 1;
		//        $oldest_parent = ($ancestors) ? $ancestors[$root]: $post->ID;

		//        var_dump($ancestors);
		//        var_dump($root);
		//        var_dump($oldest_parent);

		//        $args = array(
		//            'order' => 'asc',
		//            'orderby' => 'menu_order',
		//            'post_parent' => $post->ID,
		//            'post_type' => 'page',
		//            'post_status' => 'publish',
		//            'fields'        =>  'ids'
		//        );
		//        $portfolio_children = new WP_Query( $args);
		//        var_dump($portfolio_children->posts);
		?>
		<tr>
			<?php $mb->the_field('custom_title'); ?>
			<th scope="row"><label for="<?php $mb->the_name(); ?>">Custom Title:</label></th>
			<td>
				<input type="text"
					   class="widefat"
					   name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id="<?php $mb->the_name(); ?>"
					   maxlength="150" />
				<p class="description">The page will use the top title as the page H1, enter a custom title here to override this.</p>
			</td>
		</tr>
		<tr>
            <?php $mb->the_field('custom_title_tag_line'); ?>
            <th scope="row"><label for="<?php $mb->the_name(); ?>">Title Tag Line:</label></th>
            <td>
                <textarea id="<?php $mb->the_name(); ?>" class="widefat" name="<?php $mb->the_name(); ?>" rows="2"><?php $mb->the_value(); ?></textarea>
                <p class="description">Custom H2 tag line which will appear under the page content title if this is filled in.</p>
            </td>
        </tr>
		</tbody>
	</table>
	<hr>
	<h1>Custom Page Banner</h1>
	<table class="form-table">
		<tbody>
		<?php /* ?><tr>
			<?php $mb->the_field('banner_title'); ?>
			<th scope="row"><label for="<?php $mb->the_name(); ?>">Banner Title:</label></th>
			<td>
				<input type="text"
					   class="widefat"
					   name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id="banner_title"
					   maxlength="150" />
				<p class="description">Title line of the banner.</p>
			</td>
		</tr>
		<tr>
			<?php $mb->the_field('banner_tag'); ?>
			<th scope="row"><label for="<?php $mb->the_name(); ?>">Banner Tag Text:</label></th>
			<td>
				<textarea id="banner_tag" class="widefat" name="<?php $mb->the_name(); ?>" rows="3"><?php $mb->the_value(); ?></textarea>
				<p class="description">Banner tag text to accompany the title above, seen below the title text, in smaller font.</p>
			</td>
		</tr><?php */ ?>
		<tr>
			<?php $mb->the_field('image_id'); ?>
			<?php $max_image_num = 1;?>
			<th scope="row"><label for="<?php $mb->the_id(); ?>">Upload Banner Image:</label></th>
			<td id="banner-upload-td">
				<input id="upload_banner_button" type="button" class="button alignleft" value="Upload Image" />
				<a title="Delete Image" class="flkn-delete-banner submitdelete alignright" href="#" >Delete Banner</a>
				<br><p class="description">Upload custom images for use in the banner. You can choose <?php echo $max_image_num._n(' image',' images',$max_image_num);?>.
					If you do not choose an image no banner will be shown on the front end, and any text above will
					not be displayed. See banner preview below.</p>
			</td>
		</tr>
		</tbody>
	</table>
	<hr>

	<?php
	if ( function_exists( 'falkon_get_global_options' ) ) $falkon_option = falkon_get_global_options(); //var_dump($falkon_option);
	$page_colour = '';
	$page_ancestors = get_post_ancestors( $page->ID );
	$image_id_val = '';
	$img_src = '<img src="" class="bannerimg">';
	$banner_class = 'hidden';
	$tag_class = ' hidden';
	if($mb->get_the_value()!=null){
		$image_url = wp_get_attachment_image_src( $mb->get_the_value(), 'banner-image-full' );
		$img_src = '<img src="'.$image_url[0].'" class="bannerimg">';
		$image_id_val = $mb->get_the_value();
		$banner_class = '';
		?>

		<?php
	}

	if($mb->get_the_value('banner_title')!='' or $mb->get_the_value('banner_tag')!='') $tag_class = '';

	echo '<input type="hidden" id="image_id" name="'.$mb->get_the_name().'" value="'.$mb->get_the_value().'"/>';
	?>
	<div id="banner-preview" data-max_file_uploads="<?php echo $max_image_num;?>" class="<?php echo $banner_class.$page_colour;?>">
		<?php echo $img_src;?>
		<?php //if($mb->get_the_value('title_line')!='' or $mb->get_the_value('tag_line')!=''){ ?>
		<div class="banner-tag<?php echo $tag_class;?>"><div class="middle"><div class="inner">
					<div class="banner-title"><?php $mb->the_value('banner_title');?></div>
					<div class="banner-text"><?php $mb->the_value('banner_tag');?></div>
				</div></div></div>
		<?php //} ?>
	</div>
	<?php /* ?><table id="table-hide-fade" class="form-table <?php echo $banner_class;?>">
        <tbody>
            <tr>
                <th scope="row"></th>
                <td><?php $mb->the_field('hide_fade'); ?>
                    <label for="hide_fade"><input type="checkbox" id="hide_fade" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> Hide the white fadeout.</label><br/>
                    <p class="description">Tick to hide the white background of the text on the slide/banner.</p></td>
            </tr>
        </tbody>
    </table><?php */ ?>
</div>