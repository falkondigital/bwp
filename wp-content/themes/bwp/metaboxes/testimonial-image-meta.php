<div class="my_meta_control">
	<table class="form-table">
		<tbody>
		<tr>
			<?php $mb->the_field('image_id'); ?>
			<?php $max_image_num = 1;?>
			<th scope="row"><label for="<?php $mb->the_id(); ?>">Upload Image:</label></th>
			<td id="testimonial-upload-td">
				<input id="upload_testimonial_button" type="button" class="button alignleft upload_custom_image_button" value="Upload Image" />


			</td>
		</tr>
		<tr>
			<td colspan="2" class="image-desc"><p class="description">Upload a testimonial image. Image of the user, logo, etc.</p></td>
		</tr>
		</tbody>
	</table>

	<?php /* ?><div id="imggal" data-max_file_uploads="<?php echo $max_image_num;?>">
        <?php
        if($mb->get_the_value()!=null){
            foreach($mb->get_the_value() as $image_id){
                $image_url = wp_get_attachment_image_src( $image_id, 'thumbnail' );
                //var_dump($image_url);

                echo'<div class="flkn-image-bar">
                    <a title="Edit" class="flkn-edit-file" href="'.admin_url("post.php?post=$image_id&amp;action=edit").'" target="_blank">Edit</a> |
                    <a title="Delete" class="flkn-delete-file submitdelete" href="#">Delete</a>
                </div>
                </div>';
            }
        }
        ?>
    </div><?php */ ?>

	<?php
//	$default_thumbnail_testimonial = get_template_directory_uri().'/images/default-thumbnail.jpg';
//	$img_src = '<img src="'.$default_thumbnail_testimonial.'" class="testimonialimg default-image">';
	$img_src = '';
	$image_id_val = '';
	if($mb->get_the_value()!=null){
		$image_url = wp_get_attachment_image_src( $mb->get_the_value(), 'testimonial-image-full' );
		$img_src = '<img src="'.$image_url[0].'" class="testimonialimg">';
		$image_id_val = $mb->get_the_value();
	}
	echo '<input type="hidden" id="image_id" name="'.$mb->get_the_name().'" value="'.$mb->get_the_value().'"/>';
	?>

	<div id="testimonial-preview" data-max_file_uploads="<?php echo $max_image_num;?>" class="upload_custom_image_button">
		<?php echo $img_src;?>
	</div>
	<a title="Delete Image" class="flkn-delete-testimonial submitdelete alignright" href="#" >Delete Image</a>
</div>
