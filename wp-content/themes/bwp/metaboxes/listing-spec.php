<?php

function meta_listing_save_filter_func($meta, $post_id){
//	var_dump($meta);

//	if(!$meta['rating']) $meta['rating'] = 0;
//	if(!$meta['reviews']) $meta['reviews'] = 0;
//	if(!$meta['cost_from']) $meta['cost_from'] = (float)0.00;
//	$meta['cost_from'] = format_price($meta['cost_from']);

//    var_dump($meta); exit;
//    if($meta['births']) $meta['births'] = (int)$meta['births'];

//	if(isset($_POST['listing_berth'])) {
//		$meta['berth'] = $_POST['listing_berth'];
//	}
//	else
//		unset($meta['berth']);

	if(!isset($meta['reg_year']) or !isset($meta['reg_letter'])){
		$form_reg_year_and_letter_array = explode('_',$meta['reg_year_and_letter']);
		$meta['reg_year'] = $form_reg_year_and_letter_array[0];
		$meta['reg_letter'] = $form_reg_year_and_letter_array[1];
	}

	if(!$meta['listing_duration'] or $meta['listing_duration']=='') $meta['listing_duration'] = '4';
	//$meta['listing_duration'] = '4';	//TEMP, REMOVE

	$postDate = strtotime('U', get_post_time('U', false, $post_id));
	$postDate = get_post_time('U', true, $post_id);
	$postEnd = strtotime('+'.$meta['listing_duration'].' week', $postDate);
	if(!$meta['listing_date_end'] or $meta['listing_date_end']=='') $meta['listing_date_end'] = $postEnd;

	// Force registration plate to capital, keep it tidy
	if($meta['listing_reg_plate']) $meta['listing_reg_plate'] = strtoupper((string)$meta['listing_reg_plate']);

	//var_dump($meta['mileage']);
	if($meta['listing_mileage']){
		$pattern = '/[^0-9]*/';
		//var_dump($meta['mileage']);
		$meta['listing_mileage'] = preg_replace($pattern,'', $meta['listing_mileage']);
		//var_dump($meta['mileage']); exit;
	}

	if($meta['listing_location']){
		$tmpBikePostcode = formatBritishPostcode($meta['listing_location']);
		if(!$tmpBikePostcode) /*$tmpBikePostcode = 'CA16 6AL';//*/$tmpBikePostcode = '';
		$meta['listing_location'] = $tmpBikePostcode;

		//Create long/lat co-ords for bike location
		if($tmpBikePostcode!=''){
			$userLocation = postcode_to_coords($tmpBikePostcode);
			if($userLocation!==false) $meta['listing_latitude'] = $userLocation['latitude'];
			if($userLocation!==false) $meta['listing_longitude'] = $userLocation['longitude'];
			if($userLocation!==false) $meta['listing_coords'] = $userLocation['latitude'].','.$userLocation['longitude'];
		}
	}

	if(!isset($meta['autorelist'])){
		$meta['autorelist'] = '0';
	}


	return $meta;
}

function save_action_listing_meta($meta, $post_id) {
  	_log("WPALC - action called save_action_listing_meta");
	// Check permissions
  	if ( 'listing' == $_POST['post_type'] && !current_user_can( 'edit_listings', $post_id )) {
      	_log('leave?');
		return $post_id;
  	}
	_log("WPALC - good to run action");

//    var_dump($meta);

//	if($meta['berths']){
//		$listing_berths = get_option('listing_births');
//
//		if(!$listing_berths)
//			$listing_berths = array();
//
////		var_dump($listing_berths);
//
//		if(!in_array((int)$meta['berths'],$listing_berths)){
//			$listing_berths[] = (int)$meta['berths'];
//		}
////		var_dump($listing_berths);
//	}
//	sort($listing_berths);
//	$listing_berths = update_option('listing_births',$listing_berths);
//	var_dump($listing_berths);

//	exit;
//    if($meta['births']) $meta['births'] = (int)$meta['births'];
//    var_dump($meta);
//    exit;

	$auctionMake = $meta['listing_make'];
	$auctionModel = $meta['listing_model'];
	$auctionDerivative = $meta['listing_derivative_short'];//USE SHORT TO CHECK MODEL, AS NON-SHORT INCLUDES CUSTOM TERM META YEARS ETC
	$taxonomy = 'make';
	_log('$auctionMake:');
	_log($auctionMake);
	_log('$auctionModel:');
	_log($auctionModel);
	_log('$auctionDerivative:');
	_log($auctionDerivative);

	$parent = 0;
	$make_term = term_exists($auctionMake, $taxonomy, $parent);
	_log('$make_term: does term exist?');
	_log($make_term);
	if ($make_term == 0) {
		_log('MAKE DOES NOT EXIST, CREATING...');
		wp_insert_term($auctionMake, $taxonomy);
	}
	$add_make = wp_set_object_terms($post_id, $auctionMake, $taxonomy);
	_log('$add_make:');
	_log($add_make);
	if (isset($auctionModel) && $auctionModel != '') {
		$parent = $add_make[0];
		_log('MODEL $parent:');
		_log($parent);
		$model_exists = term_exists($auctionModel, $taxonomy, $parent);
		_log('$model_exists: does term exist?');
		_log($model_exists);
		if ($model_exists == 0) {
			_log('MODEL DOES NOT EXIST, CREATING...');
			wp_insert_term($auctionModel, $taxonomy, array('parent' => $parent));
		}
		$add_model = wp_set_object_terms($post_id, $auctionModel, $taxonomy);
		_log('$add_model:');
		_log($add_model);
		if (isset($auctionDerivative) && $auctionDerivative != '') {
			$parent = $add_model[0];
			_log('DERIVATIVE $parent:');
			_log($parent);
			$derivative_exists = term_exists($auctionDerivative, $taxonomy, $parent);
			_log('$derivative_exists: does term exist?');
			_log($derivative_exists);
			if ($derivative_exists == 0) {
				_log('DERIVATIVE DOES NOT EXIST, CREATING...');
				wp_insert_term($auctionDerivative, $taxonomy, array('parent' => $parent));
			}
			$add_derivative = wp_set_object_terms($post_id, $auctionDerivative, $taxonomy);
			_log('$add_derivative:');
			_log($add_derivative);
		}
	}



//	$add_make = wp_set_object_terms($post_id, $meta['listing_make'], 'make');
//	_log('$add_make:');
//	_log($add_make);
//
//		$add_model = wp_set_object_terms($post_id, $meta['listing_model'], 'make');
//		_log('$add_model:');
//		_log($add_model);
//	$add_derivative = wp_set_object_terms($post_id, $meta['listing_derivative'], 'make');
//	_log('$add_derivative:');
//	_log($add_derivative);


//
//		//Save Taxonomy to taxonomy metas
//		//transmission
//		$ourtaxonomy = $_POST['listing_transmission'];
//		wp_set_object_terms( $post_id, $ourtaxonomy, 'transmission' );
//
////	var_dump($_POST);
//		//berth
//		$ourtaxonomy = $_POST['listing_engine'];
////		var_dump($ourtaxonomy);
////	exit;
//		wp_set_object_terms( $post_id, $ourtaxonomy, 'engine' );
//		//class
//		$ourtaxonomy = $_POST['listing_style'];
//		wp_set_object_terms( $post_id, $ourtaxonomy, 'style' );
//


//	var_dump($meta['berth']);
//	exit;

	//return $ourtaxonomy;
}


if ( is_admin()) add_action( 'admin_enqueue_scripts', 'my_metabox_styles_listing' );

function my_metabox_styles_listing()
{
	wp_enqueue_script('jquery-ui-tabs');
	wp_enqueue_style( 'wpalchemy-metabox-listing', get_stylesheet_directory_uri() . '/metaboxes/meta-listing.css');
//    wp_enqueue_script( 'jquery-apps-listings', get_template_directory_uri() .'/js/jquery.apps.listings.js', array('jquery'),filemtime( get_stylesheet_directory().'/js/jquery.apps.listings.js'), true  );

//	wp_enqueue_style( 'meta-tabs', get_template_directory_uri() .'/metaboxes/meta-tabs.css' );
}


$listing_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_listing_meta',
	'title' => 'Listing Information',
	'types' => array('listing'), // added only for pages and to custom post type "events"
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'save_filter' => 'meta_listing_save_filter_func',
	'save_action'	=>	'save_action_listing_meta',
	'template' => get_stylesheet_directory() . '/metaboxes/listing-meta.php',
	'mode' => WPALCHEMY_MODE_EXTRACT,
	'prefix' => '_listing_'
));

/* eof */