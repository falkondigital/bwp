<?php

function meta_slide_save_filter_func($meta, $post_id){
    if($meta['link']){
        $meta['link'] = esc_url($meta['link']);
    }
    if(!$meta['box_side']){
        $meta['box_side'] = 'left';
    }
    return $meta;
}

add_action( 'save_post', 'custom_post_type_title_slide' ); //if post title = 'notitle' set it to ''
function custom_post_type_title_slide ( $post_id ) {
    $post_type = get_post_type( $post_id );
    if ( $post_type == 'slide') {
        $post_title = get_the_title($post_id);
        global $wpdb;
        global $post;
        if($post_title==''){
            $post_name = substr(md5(rand()), 0, 6);
            $post_title = '';
            $where = array( 'ID' => $post_id );
            remove_action('save_post', 'custom_post_type_title_slide');
            $wpdb->update( $wpdb->posts, array( 'post_title' => $post_name, 'post_name' => $post_name), $where ); //can set post name/url here based on other meta data?
            add_action( 'save_post', 'custom_post_type_title_slide' );
        }
        elseif ( $post_title == 'Auto Draft' and $post!=NULL) {

            $post_name = $post->post_name;

            //if (strlen(stristr($post_name,$post_title))>0) { //generate a post_name
            $post_name = sanitize_title($post_type.'-'.$post_id);
            //}

            $post_title = '';
            $where = array( 'ID' => $post_id );

            remove_action('save_post', 'custom_post_type_title_slide');
            $wpdb->update( $wpdb->posts, array( 'post_title' => $post_name, 'post_name' => $post_name), $where ); //can set post name/url here based on other meta data?
            add_action( 'save_post', 'custom_post_type_title_slide' );
        }
        elseif($post_title!=''){
            $post_name = get_the_title($post_id);

            //if (strlen(stristr($post_name,$post_title))>0) { //generate a post_name
            $post_name = sanitize_title($post_name);
            //}

            $post_title = '';
            $where = array( 'ID' => $post_id );

            remove_action('save_post', 'custom_post_type_title_slide');
            $wpdb->update( $wpdb->posts, array( 'post_title' => $post_name, 'post_name' => $post_name), $where ); //can set post name/url here based on other meta data?
            add_action( 'save_post', 'custom_post_type_title_slide' );

            $post_name = $post->post_name;
        }
    }
}

add_action ( 'trash_post', 'do_trash_post_slide' ); // Set status to trash when post is trashed
function do_trash_post_slide($post_id) {
    if ( get_post_type( $post_id ) == 'slide' and !get_the_title($post_id)) {
        global $wpdb;
        $post_status = 'trash';
        $where = array( 'ID' => $post_id );
        $wpdb->update( $wpdb->posts, array( 'post_status' => $post_status), $where );
    }
}

add_action ( 'untrash_post', 'do_trash_post_slide' ); // Set status to whatever it was before it was in the trash


if ( is_admin()) add_action( 'admin_enqueue_scripts', 'my_metabox_styles_slide' );
function my_metabox_styles_slide()
{
//    if ( isset($_POST['post_type']) and 'slide' == $_POST['post_type'] && !current_user_can( 'ams_edit_pages', $post_id )) {
//        return $post_id;
//    }
//    elseif( 'slide' == get_post_type( $post_id)) {
        wp_enqueue_style('wpalchemy-metabox-slide', get_stylesheet_directory_uri() . '/metaboxes/meta-slide.css');
        global $pagenow;
        if( 'post.php' == $pagenow or 'post-new.php' == $pagenow) {
            wp_enqueue_script('thickbox');
            wp_enqueue_style('thickbox');
            wp_enqueue_script('media-upload');
            wp_enqueue_script('wptuts-upload');
            wp_enqueue_media();
            wp_enqueue_script('jquery-slide-upload', get_template_directory_uri() . '/js/slide-upload.js', array('jquery', 'media-upload', 'thickbox'));
            wp_localize_script('jquery-slide-upload', 'jquery_slide_upload', array('thumbnail_url' => get_template_directory_uri() . '/images/default-slider.jpg'));
        }
//    }
}

$slide_mb = new WPAlchemy_MetaBox(array
(
    'id' => '_slide',
    'title' => 'Slide Information',
    'types' => array('slide'),
    'context' => 'normal',
    'priority' => 'high',
    'save_filter' => 'meta_slide_save_filter_func',
    'template' => get_stylesheet_directory() . '/metaboxes/slide-meta.php',
    'prefix' => '_slide_'
));

/* eof */