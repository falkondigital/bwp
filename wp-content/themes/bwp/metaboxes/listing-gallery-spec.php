<?php

function save_action_listing_gallery_meta($meta, $post_id) {
	_log("WPALC - action called save_action_listing_gallery_meta");
	// Check permissions
	if ( 'listing' == $_POST['post_type'] && !current_user_can( 'edit_listings', $post_id )) {
		_log('leave?');
		return $post_id;
	}
	_log("WPALC - good to run action");

    _log($meta);

	if($meta['gallery'] and count($meta['gallery'])>=1){

		$is_image = wp_attachment_is_image((int)$meta['gallery'][0]);
		if($is_image){
			update_post_meta($post_id,'_thumbnail_id',(int)$meta['gallery'][0]);
		}
	}

//	var_dump($meta['berth']);
//	exit;

	//return $ourtaxonomy;
}

if ( is_admin()) add_action( 'admin_enqueue_scripts', 'my_metabox_styles_listing_gallery' );

function my_metabox_styles_listing_gallery()
{
	wp_enqueue_style( 'wpalchemy-metabox-listing-gallery', get_stylesheet_directory_uri() . '/metaboxes/meta-listing-gallery.css');
	//wp_enqueue_script( 'jquery-ui-spinner',array( 'jquery' ) );
//    wp_enqueue_script('thickbox');
//    wp_enqueue_style('thickbox');

//    wp_enqueue_script('media-upload');
//    wp_enqueue_script('wptuts-upload');
    wp_enqueue_media();
    wp_enqueue_script( 'listing-upload', get_template_directory_uri() .'/js/listing-upload.js', array('jquery','media-upload','thickbox') );
}


//if ( is_admin()) add_action('init', 'load_jquery_ui');

$listing_gallery_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_listinggallery',
	'title' => 'Listing Gallery',
	'types' => array('listing'), // added only for pages and to custom post type "events"
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	//'init_action' => 'remove_default_divs_listing',
	//'save_filter' => 'meta_listing_save_filter_func',
	'save_action'	=>	'save_action_listing_gallery_meta',
	'template' => get_stylesheet_directory() . '/metaboxes/listing-gallery-meta.php',
	//'mode' => WPALCHEMY_MODE_EXTRACT,
//	'prefix' => '_listinggallery_'
));

/* eof */