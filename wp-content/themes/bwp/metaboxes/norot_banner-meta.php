<?php global $wpalchemy_media_access; ?>


<div class="banner_meta_control">


<br>
	<label for="<?php $mb->the_name(); ?>">I DO NOT WANT TO SHOW A BANNER</label>
	<?php $mb->the_field('hide_banner'); ?>

	<label for="show_pg_title"><input type="checkbox" id="hide_banner" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/></label>
	<p class="description">Only tick this box if you do NOT want to display a banner at the top of the page.</p>
	<br>
<hr>


	<p>
		Upload an image for the banner of this page, suggested image size: 1900px by 440px
	</p>
	<?php $wpalchemy_media_access->setGroupName('img-n'. $mb->get_the_index())->setInsertButtonLabel('Insert Image'); ?>
	<p>
		<?php $mb->the_field('imgurl'); ?>
		<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-url')); ?>
		<?php $mb->the_field('image_id'); ?>
		<?php echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-id')); ?>
		<?php echo $wpalchemy_media_access->getButton(array('label' => 'Upload Image')); ?>
		<?php $mb->the_field('imgurl'); ?>
		<?php
		?>
		<img src="<?php echo (is_null($mb->get_the_value())?'':$mb->get_the_value());?>" class="slideimg default-image">
	</p>


	<?php $mb->the_field('title_line'); ?>
	<label for="<?php $mb->the_name(); ?>">Banner title</label>
	<p>
		<input type="text" style="width:100%;" id="<?php $mb->the_name(); ?>" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" />
	</p>

	<?php $mb->the_field('ban_tag'); ?>
	<label for="<?php $mb->the_name(); ?>">Banner subtitle</label>
	<p>
		<textarea id="<?php $mb->the_name(); ?>" rows="2" name="<?php $mb->the_name(); ?>"><?php $mb->the_value(); ?></textarea>
	</p>

<!--
	<label>Mobile Alignment</label>
	<p>Selected some alignment rules for the banner on a mobile - if nothing is selected here, right alignment will be used as default</p>
	<br>
	<?php $selected = ' selected="selected"'; ?>
	<?php $metabox->the_field('mobile_alignment'); ?>
	<select id="imageSelector" name="<?php $metabox->the_name(); ?>">
		<option value=""></option>
		<option value="banner-nrt-left"<?php if ($metabox->get_the_value() == 'banner-nrt-left') echo $selected; ?>>Left</option>
		<option value="banner-nrt-mid"<?php if ($metabox->get_the_value() == 'banner-nrt-mid') echo $selected; ?>>Middle</option>
	</select>
-->

</div>