<?php
/**
 * Single post template for CPT 'case-study'
 */
?>
<?php
//	global $genpage_meta;
//	$general_meta = $genpage_meta->the_meta();
//	global $banner_mb;
//	$banner_meta = $banner_mb->the_meta();
	// Falkon Options
global $falkon_option;
get_header();
global $page_mb;
$page_meta = $page_mb->the_meta();
if(has_post_thumbnail()){
	$jumbo_img_src = wp_get_attachment_image_src(get_post_thumbnail_id(),$size = 'full');//get_template_directory_uri() . '/images/banner-blog.jpg';
	if(is_array($jumbo_img_src))
		$jumbo_img_src = $jumbo_img_src[0];
}
else{
//    $blog_page_id = get_option('page_for_posts');
//    global $page_mb;
//    $page_meta = $page_mb->the_meta();
//    $page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
//    $page_title_tag_line = $page_meta['custom_title_tag_line']? $page_meta['custom_title_tag_line']: false;
	$jumbo_img_src = get_template_directory_uri() . '/images/banner-blog.jpg';
}
$page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
$page_title_tag_line = $page_meta['custom_title_tag_line']? $page_meta['custom_title_tag_line']: false;
if((int)$page_meta['image_id']!=0){
	$jumbo_img_src = wp_get_attachment_image_src((int)$page_meta['image_id'],$size = 'full');//get_template_directory_uri() . '/images/banner-blog.jpg';
	if(is_array($jumbo_img_src))
		$jumbo_img_src = $jumbo_img_src[0];
}
//Case Study tag line
if($page_title_tag_line===false){
	$case_study_archive_id = $falkon_option['falkon_case_study_page_id'];
	$page_title_tag_line = '<a href="'.get_permalink($case_study_archive_id).'">'.get_the_title($case_study_archive_id).'</a>';
	$page_title_tag_line .= '<span> - <span>'.get_the_title().'</span></span>';
}
?>
<div id="jumbotron" class="jumbotron jumbotron-fluid text-white" style="background-image: url('<?php echo $jumbo_img_src;?>'); background-position-x: center;">
	<div class="shadow"></div>
	<div class="container text-center">
		<div class="row">
			<div class="col-12 col-sm-8 offset-sm-2">
				<p class="h1 text-white"><?php echo $page_title;?></p>
			</div>
			<?php
			if($page_title_tag_line!==false){
				echo '<div class="w-100"></div>';
				echo '<div class="col-12">';
				echo '<p class="h3 text-white font-weight-normal header-meta">'.$page_title_tag_line.'</p>';
				echo '</div>';
			}
			?>
		</div>
	</div>
</div>
<?php //var_dump($banner_meta);
//?>
<div class="bg-white">
<div class="container pt-6">
	<div id="ourwork-content">
		<?php get_template_part('includes/loops/content', 'owp'); ?>
	</div>
</div>
</div>
<div class="bg-light">
<?php /* ?><section id="related-projects"><?php */ ?>
<?php include(locate_template('includes/blocks/related-work.php'));?>
</div>
<?php include(locate_template('includes/blocks/final-cta.php'));?>
<?php /* ?></section><?php */ ?>
<?php get_footer(); ?>
