<?php global $falkon_option;?>
<footer class="pt-6 pt-sm-8 bg-darker" id="footer">
  <div class="container pb-6">
    <div class="row pb-4 menus-contacts-wrapper" itemscope="" itemtype="http://schema.org/LocalBusiness">
      <div class="col-12 col-sm-6 col-md-3 col-lg-3 text-center text-sm-left pb-6 pb-sm-0">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name','display' ).( get_bloginfo('description')!=''? ' - '.get_bloginfo('description'): '')); ?>" rel="nofollow home">
          <?php /* ?><img src="<?php echo get_template_directory_uri(); ?>/images/logo-bwp-footer.png"
               alt="<?php echo get_bloginfo('name').' '.get_bloginfo('description'); ?>"
               class="logo"
               data-retina /><?php */ ?>
          <img src="<?php echo get_template_directory_uri(); ?>/images/logo-bwp.svg"
               width="260px"
               height="71px"
               alt="<?php echo get_bloginfo('name').' '.get_bloginfo('description'); ?>"
               onerror="this.src='<?php echo get_template_directory_uri(); ?>/images/logo-bwp.png';this.onerror=null;"
               class="logo"
               data-retina/>
        </a>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2 offset-lg-2 text-center text-sm-left pb-6 pb-md-0">
        <?php
        $menu_name = 'footmenu';
        $menu_args = array(
            'theme_location'    => $menu_name,
            'depth'             => 1,
            'menu_class'        => 'footer-links'
        );
        echo yourtheme_nav_menu_args($menu_name,$menu_args);
        ?>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2 text-center text-sm-left">
          <?php
          $company_name = $falkon_option['falkon_company_name']!=''? $falkon_option['falkon_company_name']: get_bloginfo( 'name' );
          echo '<span itemprop="name" style="display:none;">'.$company_name.'</span>';
          ?>
          <?php if($falkon_option['falkon_contact_address']!='') echo '<p class="footer-address">'.nl2br($falkon_option['falkon_contact_address']).'</p>';?>
          <p><?php echo ($falkon_option['falkon_company_name']!=''?$falkon_option['falkon_company_name']:bloginfo('name'));?></p>
          <img itemprop="image" src="<?php bloginfo('template_url');?>/images/logo-bwp.png" style="display:none;"/>
          <?php
          if($falkon_option['falkon_schema_location_streetaddress1']!=''){
            echo '<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<p itemprop="streetAddress">'.$falkon_option['falkon_schema_location_streetaddress1'].'</p>';
            if($falkon_option['falkon_schema_location_streetaddress2']) echo '<p itemprop="streetAddress">'.$falkon_option['falkon_schema_location_streetaddress2'].'</p>';
            if($falkon_option['falkon_schema_location_addressLocality']) echo '<p itemprop="addressLocality">'.$falkon_option['falkon_schema_location_addressLocality'].'</p>';
            if($falkon_option['falkon_schema_location_addressRegion']) echo '<p itemprop="addressRegion">'.$falkon_option['falkon_schema_location_addressRegion'].'</p>';
            if($falkon_option['falkon_schema_location_postalCode']) echo '<p itemprop="postalCode">'.$falkon_option['falkon_schema_location_postalCode'].'</p>';
            if($falkon_option['falkon_schema_location_country']) echo '<p itemprop="addressCountry">'.$falkon_option['falkon_schema_location_country'].'</p>';
            echo '</div>';
          }
          if($falkon_option['falkon_schema_opening_hours']!=''){
            $date_time_human = $falkon_option['falkon_opening_hours']?$falkon_option['falkon_opening_hours']:'';
            echo '<time style="display: none;" itemprop="openingHours" datetime="'.$falkon_option['falkon_schema_opening_hours'].'">'.$date_time_human.'</time>';
          }
          if($falkon_option['falkon_schema_location_geo']!=''){
            $co_ords = explode(',',$falkon_option['falkon_schema_location_geo']);
            echo '<div itemscope itemtype="http://schema.org/Place">
						<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
							<meta itemprop="latitude" content="'.$co_ords[0].'" />
							<meta itemprop="longitude" content="'.$co_ords[1].'" />
						</div>
					</div>';
          }
          ?>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2 contacts-wrapper text-center text-sm-left text-nowrap">
        <?php if($falkon_option['falkon_contact_number']!='') echo '<p>'.do_shortcode("[company-contact-number link=true international=true]").'</p>';?>
        <?php if($falkon_option['falkon_contact_fax']!='') echo '<p>'.do_shortcode("[company-contact-fax link=true]").'</p>';?>
        <?php if($falkon_option['falkon_contact_email']!='') echo '<p>'.do_shortcode("[company-contact-email link=true]").'</p>';?>
        <?php
        $social_links_header = $falkon_option['falkon_social_multicheckbox_inputs'];
        if(count($social_links_header)>0){
          echo '<div class="footer-socials-wrapper">';
          foreach($social_links_header as $social_url => $value){
            if($value and $falkon_option[$social_url]!='') echo '<a href="'.esc_url($falkon_option[$social_url]).'" class="sociallink" rel="nofollow" target="_blank"><i class="fab fa-custom-'.$social_url.'"></i></a>';
          }
          echo '</div>';
        }
        ?>
        <div class="regulation-logos footer-logos text-center text-sm-left mt-3">
          <?php
          if(is_array($falkon_option['falkon_footer_regulation_logos'])){
            $result = '';
            foreach ($falkon_option['falkon_footer_regulation_logos'] as $image_id) {
              $image_attributes = wp_get_attachment_image_src($image_id, 'full');
              $img_meta       = wp_prepare_attachment_for_js($image_id);
              $image_title    = $img_meta['title'] == '' ? '' : $img_meta['title'];
              $image_alt      = $img_meta['alt'] == '' ? $image_title : $img_meta['alt'];

              $retina_image = '';
              $info = get_attached_file($image_id);
              $file_explode = explode('.',$info);
              if (file_exists($file_explode[0].'@2x.'.$file_explode[1]))
                $retina_image = " data-retina";
              $result .= '<img src="' . $image_attributes[0] . '" title="'.esc_attr($image_title).'" alt="'.esc_attr($image_alt).'" '.$retina_image.'>';
            }
            echo $result;
          }
          ?>
        </div>
      </div>
    </div><!--itemscpe-->
    <?php
    $menu_name = 'footlinkmenu';
    $menu_args = array(
        'theme_location'    => $menu_name,
        'container' => false,
        'fallback_cb' => false,
        'before'		=> ' | ',
        'echo'			=> false,
        'menu_id'		=> 'footlinkmenu',
        'items_wrap'		=> '<ul id="%1$s" class="menu-inline-links %2$s">%3$s</ul>'
    );
    $footlinks = yourtheme_nav_menu_args($menu_name,$menu_args);
    ?>
    <?php
    $footer_email = '';//$falkon_option['falkon_site_email']? ' | <a href="mailto:'.$falkon_option['falkon_site_email'].'" title="'.esc_attr($falkon_option['falkon_site_email']).'" rel="nofollow" class="footeremail">'.$falkon_option['falkon_site_email'].'</a>': '';
    $footer_blurb = $falkon_option['falkon_footer_blurb']? str_replace('{{year}}',date('Y'),$falkon_option['falkon_footer_blurb']):'&copy; '.date('Y').' <a href="'.home_url( '/' ).'" title="'. esc_attr( get_bloginfo( 'name', 'display' ).' - '.get_bloginfo('description', 'display') ).'" rel="home nofollow">'.get_bloginfo( 'name' ).'</a>';
    $footer_company_number = $falkon_option['falkon_company_reg_num']?' No. '.$falkon_option['falkon_company_reg_num']:'';
    ?>
    <div class="row no-gutters">
      <div class="col pt-4 border-top  border-dark footer-blurb-wrapper text-center text-sm-left">
        <span class="footer-blurb footerlinks"><?php echo $footer_blurb.$footer_company_number.' '.$footlinks;?></span>
      </div>
    </div>
    <?php if(is_active_sidebar('footer-widget-area')): ?>
    <div class="row border-bottom pt-5 pb-4" id="footer" role="navigation">
      <?php dynamic_sidebar('footer-widget-area'); ?>
    </div>
    <?php endif; ?>
  </div>
</footer>
<p id="back-top">
  <a href="#top" class=" " rel="nofollow"><i class="fas fa-chevron-up"></i></a>
</p>
<?php wp_footer(); ?>
<?php
if(FALKON_SHOW_SITE_TIMER){
  global $global_timer;
  echo '<div class="w-25 p-3 text-white bg-primary fixed-bottom mx-2 rounded-top rounded-lg text-nowrapX clearfix" style="opacity: 0.75;">';
  echo '<p class=""><small>TRANSIENTS = '. (int)FALKON_USE_TRANSIENTS.', ';
  echo 'TIMERS = '. (int)FALKON_SHOW_TIMERS.', ';
  echo 'SITE_TIMER = '. (int)FALKON_SHOW_SITE_TIMER.'</small></p>';
  echo '<p class="font-weight-bold mb-0">'.number_format( microtime(true) - $global_timer, 5 ).' SECONDS</p>';//TESTING
  echo '</div>';
}
?>
</body>
</html>
