<?php
/**
 * Template Name: Case Study - Parent/Archive
 *
 */
global $falkon_option;
get_header();
?>
<?php
global $page_mb;
$page_meta = $page_mb->the_meta();
if(has_post_thumbnail()){
	$jumbo_img_src = wp_get_attachment_image_src(get_post_thumbnail_id(),$size = 'full');//get_template_directory_uri() . '/images/banner-blog.jpg';
	if(is_array($jumbo_img_src))
		$jumbo_img_src = $jumbo_img_src[0];
}
else{
//    $blog_page_id = get_option('page_for_posts');
//    global $page_mb;
//    $page_meta = $page_mb->the_meta();
//    $page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
//    $page_title_tag_line = $page_meta['custom_title_tag_line']? $page_meta['custom_title_tag_line']: false;
	$jumbo_img_src = get_template_directory_uri() . '/images/banner-blog.jpg';
}
$page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
$page_title_tag_line = $page_meta['custom_title_tag_line']? $page_meta['custom_title_tag_line']: false;
if((int)$page_meta['image_id']!=0){
	$jumbo_img_src = wp_get_attachment_image_src((int)$page_meta['image_id'],$size = 'full');//get_template_directory_uri() . '/images/banner-blog.jpg';
	if(is_array($jumbo_img_src))
		$jumbo_img_src = $jumbo_img_src[0];
}
?>
<div id="jumbotron" class="jumbotron jumbotron-fluid text-white" style="background-image: url('<?php echo $jumbo_img_src;?>'); background-position-x: center;">
	<div class="shadow"></div>
	<div class="container text-center">
		<div class="row">
			<div class="col-12 col-sm-8 offset-sm-2">
				<p class="h1 text-white"><?php echo $page_title;?></p>
			</div>
			<?php
			if($page_title_tag_line!==false){
				echo '<div class="w-100"></div>';
				echo '<div class="col-12">';
				echo '<p class="h3 text-white font-weight-normal header-meta">'.$page_title_tag_line.'</p>';
				echo '</div>';
			}
			?>
		</div>
	</div>
</div>
<div id="what-we-do">
    <section id="wwd-content" class="padd-sixty">
      <div class="container">
	      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
	      <?php get_template_part('includes/loops/page', 'content-no-header'); ?>
		  </div>
      </div>
	</section>
<!--	<section id="ow-case-study-type-tag">-->
		<div class="container">
			<section id="work-tags" class="hidden-xs hidden-sm">
		<?php
		$case_study_page_id = (int)$falkon_option['falkon_case_study_page_id'];
		$taxonomy_work_type = get_terms(
			array(
			'taxonomy' => 'case-study-type',
			'hide_empty' => true,
			)
		);
//		var_dump($taxonomy_work_type);
		echo '<ul class="work-tags">';

		$act_cli = (isset($_GET['orderby']) and $_GET['orderby']=='client')?'active':'';
		$act_all = (!isset($_GET['orderby']) and !isset($_GET['work-type']))?'active':'';

//		echo '<li class="'.$act_cli.'"><a href="'.get_permalink($case_study_page_id).'?orderby=client">By Client</a></li>';
		echo '<li class="'.$act_all.'"><a href="'.get_permalink($case_study_page_id).'">All</a></li>';
		if(is_array($taxonomy_work_type)){	//get_the_terms will return an array if there are any, otherwise false or wp_error

			foreach($taxonomy_work_type as $term) :	?>
				<li <?php echo ((isset($_GET['work-type']) and $_GET['work-type']==$term->slug)?'class="active"':'');?>>
					<a href="<?php echo get_permalink($case_study_page_id); ?>?work-type=<?php echo $term->slug; ?>" title="<?php echo $term->name; ?>">
						<?php echo $term->name; ?>
					</a>
				</li>
				<?php
			endforeach;

		}
		echo '</ul>';
		?>
			</section>
</div>

			<!--	</section>-->
	<section id="ow-pgfeed" class="greycol padd-sixty">
		<div class="container"><div class="row">
			<?php
			$post_type = 'case-study';
			//query subpages
			$args = array(
			'post_type' => $post_type,
				'posts_per_page' => 12,
			'paged' => $paged
			);
			$subpages = new WP_query($args);
			// create output

			if ($subpages->have_posts()) :
				$xyz = 1;
				while ($subpages->have_posts()) : $subpages->the_post();
//					global $genpage_meta;
//					$general_meta = $genpage_meta->the_meta();
//					global $banner_mb;
//					$banner_meta = $banner_mb->the_meta();
					global $case_study_mb;
					$case_study_meta = $case_study_mb->the_meta();
//					var_dump($general_meta);

					if(isset($case_study_meta['content_short'])) {
						$ow_tagline = $case_study_meta['content_short'] ? $case_study_meta['content_short'] : wp_trim_words( get_the_content(), 10 );
					}
					else {
						$ow_tagline =  wp_trim_words( get_the_content(), 10 );
					}
					if((int)$case_study_meta['cs_primary_category']!=0){
						$category[] = get_term( (int)$case_study_meta['cs_primary_category'], 'case-study-type' );

					}
					else{
						$category = get_the_terms( $subpages->ID, 'case-study-type' );
					}
//					var_dump($category);
					$catname = '';
					if(is_array($category)){	//get_the_terms will return an array if there are any, otherwise false or wp_error
						$our_work_page_id = $falkon_option['falkon_case_study_page_id'];
						$catname =  '<a href="'.get_permalink($our_work_page_id).'?work-type='.$category[0]->slug.'" class="imgcattag" title="'.esc_attr("View all case studies with tag '".$category[0]->name."'").'">'.$category[0]->name.'</a>';
					}

					$output .= '<div class="col-xs-12 col-sm-6 col-md-4" style="margin-bottom:35px;"><div class="col-xs-12 col-sm-12 ourwk-text-bg np text-center">';
					if(has_post_thumbnail()){
						$output .= '<div class="ow-feat-img">
						<a href="'.get_permalink().'" rel="nofollow" title="'.esc_attr(get_the_title()).'">'. get_the_post_thumbnail('','full', array('class'=>'child-img')).'</a>';

						$output .= '<span class="hidden-xs">'.$catname.'</span>';
						$output .= '</div>';
					}
					$output .= '<a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">';
					$output .= '<h4>' . get_the_title() . '</h4>';
					$output .= '<p>'.$ow_tagline.'<br />';
					$output .= '</p></a></div></div>';
					if($xyz%2 == 0) $output .= '<div class="clearfix visible-xs-block"></div>';
					if($xyz++%3 == 0) $output .= '<div class="clearfix hidden-xs-block"></div>';

				endwhile;
			else :
				$post_object = get_post_type_object( $post_type );
				$output = wpautop($post_object->labels->not_found);
			endif;
			echo $output;

			if (function_exists('custom_pagination')) {
				$output .= custom_pagination ($subpages->max_num_pages,"",$paged);
			}
			// reset the query
			wp_reset_postdata();
			?>
			</div>
		</div>
	</section>
</div>
<?php get_footer(); ?>
