<?php
/**
 * Template Name: Contact us Page
 */
global $falkon_option;
?>
<?php get_header(); ?>
<?php
global $page_mb;
$page_meta = $page_mb->the_meta();
if(has_post_thumbnail()){
  $jumbo_img_src = wp_get_attachment_image_src(get_post_thumbnail_id(),$size = 'full');//get_template_directory_uri() . '/images/banner-blog.jpg';
  if(is_array($jumbo_img_src))
    $jumbo_img_src = $jumbo_img_src[0];
}
else{
//    $blog_page_id = get_option('page_for_posts');
//    global $page_mb;
//    $page_meta = $page_mb->the_meta();
//    $page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
//    $page_title_tag_line = $page_meta['custom_title_tag_line']? $page_meta['custom_title_tag_line']: false;
  $jumbo_img_src = get_template_directory_uri() . '/images/banner-blog.jpg';
}
$page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
$page_title_tag_line = $page_meta['custom_title_tag_line']? $page_meta['custom_title_tag_line']: false;
if((int)$page_meta['image_id']!=0){
  $jumbo_img_src = wp_get_attachment_image_src((int)$page_meta['image_id'],$size = 'full');//get_template_directory_uri() . '/images/banner-blog.jpg';
  if(is_array($jumbo_img_src))
    $jumbo_img_src = $jumbo_img_src[0];
}
?>
<div id="jumbotron" class="jumbotron jumbotron-fluid text-white" style="background-image: url('<?php echo $jumbo_img_src;?>'); background-position-x: center;">
  <div class="shadow"></div>
  <div class="container text-center">
    <div class="row">
      <div class="col-12 col-sm-8 offset-sm-2">
        <p class="h1 text-white"><?php echo $page_title;?></p>
      </div>
      <?php
      if($page_title_tag_line!==false){
        echo '<div class="w-100"></div>';
        echo '<div class="col-12">';
        echo '<p class="h3 text-white font-weight-normal header-meta">'.$page_title_tag_line.'</p>';
        echo '</div>';
      }
      ?>
    </div>
  </div>
</div>
<?php get_template_part('includes/loops/page-content-no-header'); ?>
<!--Contact bloc -->
<div id="contacts-info-wrapper" class="">
  <div class="bg-white">
  <div class="container ">
    <div class="row py-6">
      <div id="offices" class="col-xs-12 col-lg-12">
        <div class="row align-items-center">
          <div class="col-xs-12 col-sm-5 offset-sm-1 map-text">
            <h3>Manchester Office</h3>
              <p>BWP<br>
                6 Hewitt Street<br>
                Manchester<br>
                M15 4GB</p>
              <?php /* ?><p><strong>John Valentine</strong></p>
              <p><span class="blu">john.valentine@bwpconsultants.co.uk</span><br>
              <?php */ ?>
              <p><a href="tel:01612368133">0161 236 8133</a></p>
          </div>
          <div class="col-xs-12 col-sm-6" style="padding-left:0;padding-right:0;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2374.7028572575623!2d-2.252083734156764!3d53.47377038000596!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487bb1e891316ed1%3A0x17c43ef124ba3d58!2sHewitt+St%2C+Manchester+M15+4GB!5e0!3m2!1sen!2suk!4v1496327704429"  frameborder="0" style="border:0; width:100%; height:280px;" allowfullscreen></iframe>
          </div>
        </div>
</div>
    </div></div></div>
    <div class="bg-light">
      <div class="container ">
        <div class="row py-6 align-items-center">
          <div class="col-xs-12 col-sm-6" style="padding-left:0;padding-right:0;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2484.9742868017697!2d-0.09368823423115188!3d51.47698627963015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876037e60b4d775%3A0xf4112d8287785ec7!2sLomond+Grove%2C+Camberwell%2C+London+SE5+7HN!5e0!3m2!1sen!2suk!4v1496328590619"  frameborder="0" style="border:0; width:100%; height:280px;" allowfullscreen></iframe>
          </div>
          <div class="col-xs-12 col-sm-5 offset-sm-1 map-text">
            <h3>London Office</h3>
              <p>BWP<br>
                Camberwell Business Centre <br>
                99-103 Lomond Grove<br>
                Camberwell<br>
                SE5 7HN
              </p>
           <?php /* ?> <p><strong>Darren Bailey</strong></p>
            <p><span class="blu">darren.bailey@bwpconsultants.co.uk</span><br><?php */ ?>
            <p><a href="tel:02074594165">0207 459 4165</a></p>
          </div>
        </div>
      </div>
    </div>



        <?php /*<div class="col-xs-12 col-sm-12 col-md-4">

                    <div class="col-sm-12 ctc-bd">

                        <p><strong>Gary Newton</strong></p>

                        <p><i class="fa fa-envelope-o" aria-hidden="true"></i> <span class="blu">gary.newton@bwpconsultants.co.uk</span><br>

                            <a href="tel:07970 786 772"><i class="fa fa-phone" aria-hidden="true"></i> 07970 786 772</a></p>

                    </div>

                </div> */ ?>
</div>

<?php /* ?>
      <div class="col-12 col-sm-3 offset-sm-1 text-left ">
        <div id="contacts-info-details" class="py-4">
          <?php
          $company_name = $falkon_option['falkon_company_name']!=''? $falkon_option['falkon_company_name']: get_bloginfo( 'name' );
          //echo '<div itemprop="name">'.$company_name.'</div>';
          ?>
          <?php if($falkon_option['falkon_contact_address']!='') echo '<p><i class="fas fa-map-marker-alt text-blue" aria-hidden="true"></i> '.nl2br($falkon_option['falkon_contact_address']).'</p>';?>
          <?php if($falkon_option['falkon_contact_number']!='') echo '<p><i class="fas fa-phone-square text-blue"></i> '.do_shortcode("[company-contact-number link=true]").'</p>';?>
          <?php if($falkon_option['falkon_contact_fax']!='') echo '<p><i class="fas fa-fax text-blue" aria-hidden="true"></i> '.do_shortcode("[company-contact-fax link=true]").'</p>';?>
          <?php if($falkon_option['falkon_contact_email']!='') echo '<p><i class="fas fa-envelope-square text-blue"></i> '.do_shortcode("[company-contact-email link=true]").'</p>';?>
          <?php /* ?><p><?php echo ($falkon_option['falkon_company_name']!=''?$falkon_option['falkon_company_name']:bloginfo('name'));?></p><?php */ ?>
          <?php //if($falkon_option['falkon_opening_hours']!='') echo '<p>Opening hours are '.do_shortcode("[company-opening-hours]").'</p>';?>
<?php /* ?>        </div>
      </div>
      <div class="col-12 col-sm-6 offset-sm-1 text-center">
        <?php
        $map_location = false;
        if($falkon_option['falkon_contact_page_map_embed_url']){
          $map_location = $falkon_option['falkon_contact_page_map_embed_url'];
          //        $map_location = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2469.56045071524!2d-0.564262783920776!3d51.759360679677364!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48764350e4dd0e9b%3A0x7f6f4f63751af014!2sHigh+St%2C+Berkhamsted+HP4+2DJ!5e0!3m2!1sen!2suk!4v1532965641874";

        }
        if($map_location!==false){
          $maps = '<div class="map-container">
                <iframe data-equalizer-watch="map" src="'.esc_url($map_location).'" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>';
          echo $maps;
        }
        ?>
        <div class="clearfix w-100"></div>
      </div>
    </div>
  </div>
</div><?php */ ?>
<!-- Contact Form -->
<div id="contacts-form-wrapper" class="bg-white">
  <div class="container">
    <div class="row pt-8">
      <div class="col col-sm-10 offset-sm-1">
        <?php
        if($falkon_option['falkon_contact_form_text_line']){
          echo '<p class="h2 text-center">'.$falkon_option['falkon_contact_form_title_line'].'</p>';
        }
        if($falkon_option['falkon_contact_form_text_line']){
          echo '<p class="text-center">'.$falkon_option['falkon_contact_form_text_line'].'</p>';
        }
        if($falkon_option['falkon_contact_form_shortcode']){
          echo '<div class="bg-white p-6 mt-7">';
          echo do_shortcode(stripslashes($falkon_option['falkon_contact_form_shortcode']));
          echo '</div>';
        }
        ?>
      </div>
    </div>
  </div>
</div>
<?php //include(locate_template('includes/blocks/final-cta.php'));?>
<?php get_footer(); ?>
