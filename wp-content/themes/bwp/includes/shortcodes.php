<?php
/**
 *
 */


add_shortcode("mic-get-trial-days","return_mic_get_trial_days");
function return_mic_get_trial_days($atts, $content = null) {
    global $falkon_option;
    $trial_weeks = $falkon_option['falkon_trial_weeks'];
    $trial_in_days = $trial_weeks*7;
    $out = '<span class="trial-period trial-in-days">'.$trial_in_days.'</span>';
    return $out;
}

add_shortcode("mic-get-trial-weeks","return_mic_get_trial_weeks");
function return_mic_get_trial_weeks($atts, $content = null) {
    global $falkon_option;
    $trial_weeks = $falkon_option['falkon_trial_weeks'];
    $out = '<span class="trial-period trial-in-weeks">'.$trial_weeks.'</span>';
    return $out;
}

add_shortcode( 'mic-trial-button', 'build_mic_trial_button' );
function build_mic_trial_button($atts, $content = null) {
    extract(shortcode_atts(array(
        'xclass'	=> '',
    ), $atts));
    $xclass = ($xclass == '') ? '' : ' '.$xclass;
    global $falkon_option;
    $trial_register_id = (int)$falkon_option['falkon_register_school_page_id'];
    $out = '<a href="'.get_permalink($trial_register_id).'" class="btn'.$xclass.'" title="'.esc_attr(wp_strip_all_tags(do_shortcode($content))).'">'.do_shortcode($content).'</a>';
    return $out;
}


/**
 * @param $atts
 * @param null $content
 * @return string
 */
function falkon_themes_site_contact_email( $atts, $content = null ) {
    $falkon_option = falkon_get_global_options();
    $out = '';
    if($falkon_option['falkon_contact_email']!=NULL and $falkon_option['falkon_contact_email']!=''){
        extract(shortcode_atts(array(
            'link'	=> false,
        ), $atts));
        if($link) $out = '<a href="mailto:'.antispambot($falkon_option['falkon_contact_email'],1).'" title="'.antispambot($falkon_option['falkon_contact_email'],0).'" rel="nofollow">'.antispambot($falkon_option['falkon_contact_email'],0).'</a>';
        else $out = $falkon_option['falkon_contact_email'];
    }
    return $out;

}
add_shortcode('company-contact-email', 'falkon_themes_site_contact_email');









function falkon_themes_site_contact_phone( $atts, $content = null ) {
    $falkon_option = falkon_get_global_options();
    $out = '';
    if($falkon_option['falkon_contact_number']!=NULL and $falkon_option['falkon_contact_number']!=''){
        extract(shortcode_atts(array(
            'link'	=> false,
            'international'   =>  false,
        ), $atts));
        $number_is = $falkon_option['falkon_contact_number'];
        $number_link = preg_replace('/\s+/', '', $falkon_option['falkon_contact_number']);
        if($international){
            $number_is = substr($falkon_option['falkon_contact_number'], 1);
            $number_is = "+44 (0) ".$number_is;
            $number_link = preg_replace('/\s+/', '', $falkon_option['falkon_contact_number']);
            $number_link = "044".substr($number_link, 1);
        }
        if($link) $out = '<a href="tel:'.$number_link.'" title="'.$number_is.'" rel="nofollow" class="telephone-link"><span itemprop="telephone">'.$number_is.'</span></a>';
        else $out = $number_is;
    }
    return $out;
}
add_shortcode('company-contact-number', 'falkon_themes_site_contact_phone');

function falkon_themes_company_opening_hours( $atts, $content = null ) {
    $falkon_option = falkon_get_global_options();
    $out = '';
    if($falkon_option['falkon_opening_hours']!=NULL and $falkon_option['falkon_opening_hours']!=''){
        $opening_human = $falkon_option['falkon_opening_hours'];
        $opening_schema = $falkon_option['falkon_schema_opening_hours'];
        if($opening_schema!=''){
            $out = '<span class="opening-hours" itemprop="openingHours" datetime="'.$falkon_option['falkon_schema_opening_hours'].'">'.$opening_human.'</span>';
        }
        else{
            $out = '<span class="opening-hours">'.$opening_human.'</span>';
        }
    }
    return $out;
}
add_shortcode('company-opening-hours', 'falkon_themes_company_opening_hours');


function falkon_themes_site_contact_fax( $atts, $content = null ) {
    $falkon_option = falkon_get_global_options();
    $out = '';
    if($falkon_option['falkon_contact_fax']!=NULL and $falkon_option['falkon_contact_fax']!=''){
        extract(shortcode_atts(array(
            'link'	=> false,
            'international'   =>  false,
        ), $atts));
        $number_is = $falkon_option['falkon_contact_fax'];
        $number_link = preg_replace('/\s+/', '', $falkon_option['falkon_contact_fax']);
        if($international){
            $number_is = substr($falkon_option['falkon_contact_number'], 1);
            $number_is = "+44 (0) ".$number_is;
            $number_link = preg_replace('/\s+/', '', $falkon_option['falkon_contact_fax']);
            $number_link = "044".substr($number_link, 1);
        }
        if($link) $out = '<a href="tel:'.antispambot($number_link,1).'" title="'.$number_is.'" rel="nofollow" class="fax-link"><span itemprop="faxNumber">'.$number_is.'</span></a>';
        else $out = $number_is;
    }
    return $out;
}
add_shortcode('company-contact-fax', 'falkon_themes_site_contact_fax');

function falkon_themes_site_contact_address( $atts, $content = null ) {
    $falkon_option = falkon_get_global_options();
    $out = '';
    if($falkon_option['falkon_site_address']!=NULL and $falkon_option['falkon_site_address']!=''){
        $out = nl2br($falkon_option['falkon_site_address']);
    }
    return $out;
}
add_shortcode('company-contact-address', 'falkon_themes_site_contact_address');



function falkon_show_location_google_map( $atts, $content = null )
{
    extract(shortcode_atts(array(
        'url' => '',
    ), $atts));
    if($url=='')
        return;
//    if($title=='')
//        $title = __('Click To Expand Location','admedsol');


    $out = '';
    $out .= '<div class="google-maps">';
    $out .= '<iframe src="'.esc_url($url).'" width="850" height="370" frameborder="0" style="border:0"></iframe>';
//    $out .= '<p></p>';
//    $out .= '<a href="'.esc_url($url).'"'.($external?' rel="nofollow" target="_blank"':'').' title="'.esc_attr($title).'"><i class="fa fa-'.($external?'external-':'').'link"></i> '.$title.'</a>';
    $out .= '</div>';

    return $out;
}
add_shortcode('google-map-embed', 'falkon_show_location_google_map');


function wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

/*
* Callback function to filter the MCE settings
*/

function my_mce_before_init_insert_formats( $init_array ) {

// Define the style_formats array

    $style_formats = array(
        /*
        * Each array child is a format with it's own settings
        * Notice that each array has title, block, classes, and wrapper arguments
        * Title is the label which will be visible in Formats menu
        * Block defines whether it is a span, div, selector, or inline style
        * Classes allows you to define CSS classes
        * Wrapper whether or not to add a new block-level element around any selected elements
        */
        array(
            'title' => 'H1 Span',
            'block' => 'span',
            'classes' => 'h1',
            'wrapper' => true,

        ),
        array(
            'title' => 'H2 Span',
            'block' => 'span',
            'classes' => 'h2',
            'wrapper' => true,

        ),
        array(
            'title' => 'H3 Span',
            'block' => 'span',
            'classes' => 'h3',
            'wrapper' => true,

        ),
        array(
            'title' => 'H4 Span',
            'block' => 'span',
            'classes' => 'h4',
            'wrapper' => true,

        ),
        array(
            'title' => 'H5 Span',
            'block' => 'span',
            'classes' => 'h5',
            'wrapper' => true,

        ),
        array(
            'title' => 'H6 Span',
            'block' => 'span',
            'classes' => 'h6',
            'wrapper' => true,

        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );


function falkon_prioy_car_van_from( $atts, $content = null )
{
    $atts = shortcode_atts(
        array(
            'price' => '',
            'image'   => '',
            'title'     =>  '',
            'cols'     =>  '',
        ), $atts, 'falkon_prioy_car_van_from' );


    $image = '';
    if($atts['image']!='')
        if (getimagesize($atts['image'])!==false)
            $image = '<img src="'.esc_url($atts['image']).'" alt="'.($atts['title']==''?'':'image of '.$atts['title']).'" />';
        else
            return;

    $price = '';
    if($atts['price']!='')
        $price = '<span class="price-from from-circle">From <span><sup>£</sup>'.number_format($atts['price'], 0, '.', ',').'</span> Per Day</span>';


    ob_start();
    ?>
    <div class="<?php echo ($atts['cols']=='4'?'col-xs-4':'col-xs-6 col-sm-3');?> text-center priory-car-van-from padd-top padd-bottom">
        <?php
        if($atts['title']!='')
            echo '<span class="h3">'.$atts['title'].'</span>';
        if($image!=''){
            echo '<div class="image-price-from">'.$price.$image.'</div>';
        }
        ?>
        <?php echo do_shortcode($content);?>
    </div>

    <?php
    return ob_get_clean();
//    return $out;
}
add_shortcode('priory_car_van_from', 'falkon_prioy_car_van_from');

// Break
function tp_break( $atts, $content = null ) {
    return '<div class="clearfix"></div>';
}
add_shortcode('clear', 'tp_break');


// Location
function falkon_maps( $atts ) {
    extract( shortcode_atts(
            array(
                'title' => '',
                'subtitle' => '',
                'map' => '',
                'address' => '',
                'phone' => '',
                'flip' => '',
                'email'     =>  '',
            ), $atts )
    );

    $map_location = $map!=''?$map:'';

    if($map_location=='')
        $map_location='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9546.004995849298!2d-2.496406!3d53.262638!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487af8db5ca350b3%3A0x422c47b2f71f15e7!2sPriory+Rentals!5e0!3m2!1sen!2suk!4v1478620833556';
    $maps = '
    <div class="location-box match-my-cols padd-bottom" data-equalizer="map" data-equalize-on="medium">
        <div class="col-xs-12 col-sm-4 location-content'.($flip==''?'':' box-flip').'" data-equalizer-watch="map">
            <h4>' . $title . '</h4>
            <p><strong>Address:</strong></p>
            <p>'.html_entity_decode($address).'</p>
            <p><i class="fa fa-phone-square red-hl"></i> <a href="tel:'.$phone.'">' . $phone . '</a></p>
            <p><i class="fa fa-envelope-square red-hl"></i> <a itemprop="email" href="mailto:'.antispambot($email).'">' . antispambot($email) . '</a></p>
        </div>
        <div class="col-xs-12 col-sm-8 location-map'.($flip==''?'':' box-flip').'">
            <div class="map-container">
                <iframe data-equalizer-watch="map" src="'.esc_url($map_location).'" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div><div class="clearfix"></div>';


    return $maps;
}
add_shortcode( 'maps', 'falkon_maps' );

add_shortcode( 'callout-box', 'pr_callout_box' );
function pr_callout_box( $atts, $content ){
    extract(shortcode_atts(array(
        'color' => '%d',
    ), $atts));
    $output = '<div class="well info '.$color.'">';
    $output .= do_shortcode( $content );
    $output .= '</div>';
    return $output;
}





add_action('init', 'register_falkon_showhide_jquery');
add_action('wp_footer', 'print_falkon_showhide_jquery');

function register_falkon_showhide_jquery() {
    wp_register_script(
        'falkon-show-hide',
        get_template_directory_uri().'/js/falkon-show-hide.js',
        array('jquery'),
        '1.0',
        true
    );
}

function print_falkon_showhide_jquery() {
    global $add_falkon_showhide_jquery;
    if (!$add_falkon_showhide_jquery) return;
    wp_print_scripts('falkon-show-hide');
}



function seocreative_showhide( $atts, $content = null ) {
    global $add_falkon_showhide_jquery;
    $add_falkon_showhide_jquery = true;
    extract(shortcode_atts(array(
        'start'	=> '',
        'align' => '',
        'button_text'	=>	'',
    ), $atts));
    $align_button = ($align == '') ? ' aligncenter' : ' align'.$align;
    $button_text = $button_text==''? 'Read More': $button_text;

    $out = '<div class="more-text">'.do_shortcode($content).'</div>
	<div id="read-more-home" class="xbutton'.$align_button.'"><span title="Click to '.$button_text.'">'.$button_text.'</span></div>';
    return $out;
}
add_shortcode('showhide', 'seocreative_showhide');



if (!isset($shortcode_tags['list-staff-short'])) {
    add_shortcode('list-staff-short', 'bwp_list_staff');
}



function bwp_list_staff(){

    $output = '';

    $args = array(
        'order'          => 'ASC',
        'orderby'        => 'menu_order title',
        'post_type'      => 'staff',
        'post_status'    => 'publish',
        'posts_per_page' => - 1
    );

    // query the posts of your custom post types
    $staffQuery = get_posts( $args );
    $output .= '<div class="pt-7 pb-7">';
    $output .= '<div class="row">';
    $output .= '<div class="col-12 text-center text-lg-left clearfix"><p class="h2 pb-3">Meet the directors</p></div>';
    $output .= '</div>';
    $output .= '<div class="row">';
    foreach ( $staffQuery as $staffPost ) :

        setup_postdata( $staffPost );
        global $staff_mb;
        $staff_meta = $staff_mb->the_meta( $staffPost->ID );

        $output .= '<div class="col-12 col-lg-4 text-center text-lg-left pb-6 pb-lg-0">';
        $output .= '<div class="row">';
        $output .= '<div class="col-12">';
        if ( get_the_post_thumbnail( $staffPost->ID, 'com-staff-thumb' ) ) {
            $output .= get_the_post_thumbnail( $staffPost->ID, 'com-staff-thumb' );
        } else {
            $output .= '<img class="attachment-full wp-post-image" title="No Featured Image" alt="No Featured Image" src="' . get_bloginfo( 'template_url' ) . '/images/com-no-user150x150.gif" >';
        }

        $output .= '</div>';
        $output .= '<div class="col-12">';
        $output .= '<span class="h2 latesttit">' . get_the_title( $staffPost->ID ) . '</span>';

        $output .= wpautop(get_the_content());

//        $output .= '<p class="staffrole">' . $staff_meta['jobrole'] . '</p>';
//        $output .= '<p class="staffeducation">' . $staff_meta['jobedu'] . '</p>';

        $customEmail = $staff_meta['email_group'];

        if ( $customEmail ) {
            $break = FALSE;
            for ( $count = 0; $count < 1; $count ++ ) {

                $tempArray = $customEmail[ $count ];

                $tempo = 'cb_jobemail';

                if ( $tempArray['cb_jobemail'] ) {
                    $linkedin_link = $staff_meta['linkedin']!=''?'<a href="'.$staff_meta['linkedin'].'" target="_blank"><i class="fab fa-linkedin"></i></a> | ':'';
                    $output .= '<p class="featpropshort featproploc staffemail">'. $linkedin_link . '<a href="mailto:'.antispambot($tempArray['jobemail']).'">'.$tempArray['jobemail'] . '</a></p>';

                }
            }
        }
        $customPhone = $staff_meta['phone_group'];

        if ( $customPhone ) {

            for ( $count = 0; $count < 1; $count ++ ) {
                $tempArray = $customPhone[ $count ];
                $tempo     = 'cb_jobphone';
                if ( $tempArray['cb_jobphone'] ) {
                    $number_link = preg_replace('/\s+/', '', $tempArray['jobphone']);
                    $output .= '<p class="featpropshort featproploc staffphone"> <a href="tel:'.$number_link.'" class="link-telephone">' . $tempArray['jobphone'] . '</a></p>';
                }
            }

        }
//        $output .= '<p class="stafftag">"' . $staff_meta['jobtag'] . '"</p>';
//        $output .= '<a href="' . get_permalink( $staffPost->ID ) . '" title="' . get_the_title( $staffPost->ID ) . '">';

//        $output .= '<span title="Read More" rel="nofollow" class="findmore">Read More</span>';
//        $output .= '</a>';
        $output .= '</div>';
        $output .= '</div>';

        $output .= '</div>';



    endforeach;
    $output .= '</div>';

    return $output;
}


add_shortcode( 'wrapper', 'build_wrapper' );
function build_wrapper($atts, $content = null) {
    extract(shortcode_atts(array(
        'xclass'	=> '',
    ), $atts));
    $xclass = ($xclass == '') ? '' : ' '.$xclass;
    $out = '<div class="'.$xclass.' pt-7 pb-8">'.do_shortcode($content).'</div>';
    return $out;
}