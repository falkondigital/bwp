<?php
/**
 * // Register Custom Post Type 'Staff Member'
 */


add_action( 'init', 'create_post_type_staff',0 );
function create_post_type_staff() {
    $labels = array(
        'name' => _x( 'Staff Members', 'post type general name' ), // Tip: _x('') is used for localization
        'singular_name' => _x( 'Staff Member', 'post type singular name' ),
        'add_new' => _x( 'Add New Staff Member', 'staff member' ),
        'add_new_item' => __( 'Add New Staff Member' ),
        'edit_item' => __( 'Edit Staff Member' ),
        'new_item' => __( 'New Staff Member' ),
        'view_item' => __( 'View Staff Member' ),
        'search_items' => __( 'Search Staff Member' ),
        'not_found' =>  __( 'No staff members found' ),
        'not_found_in_trash' => __( 'No staff members found in Trash' ),
        'parent_item_colon' => '',
        'insert_into_item'  => 'Insert into staff member',
        'uploaded_to_this_item' => 'Uploaded to this staff member',
        'featured_image' => 'Staff Member Image/Photo',
        'set_featured_image'=> 'Set staff member image',
        'remove_featured_image'=> 'Remove staff member image',
        'use_featured_image'    =>  'Use as staff member image',
        'items_list' => 'Posts list',
        'item_published' => 'Staff member published.',
        'item_published_privately' => 'Staff member published privately.',
        'item_reverted_to_draft' => 'Staff member reverted to draft.',
        'item_scheduled' => 'Staff member scheduled.',
        'item_updated' => 'Staff member updated.'
    );
    // Create an array for the $args
    $args = array(
        'labels' => $labels, /* NOTICE: the $labels variable is used here... */
        'public' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        //'query_var' => false,
        //'rewrite' => array( 'slug' => 'team', 'with_front' => FALSE ),
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon'             =>'dashicons-id-alt',
        'supports' => array( 'title', 'editor', 'thumbnail' )
    );

    register_post_type( 'staff', $args );
}

/*
 * Add custom messages for the three custom post types
 */
function updated_messages_staff( $messages ) {
    global $post, $post_ID;
    $messages['staff'] = array(
        0 => '',	// Unused. Messages start at index 1.
        1 => __('Staff member updated.'),
        2 => __('Custom field updated.'),
        3 => __('Custom field deleted.'),
        4 => __('Staff member updated.'),
        5 => isset($_GET['revision']) ? sprintf( __('Staff member restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
        6 => __('Staff member published.'),
        7 => __('Staff member saved.'),
        8 => sprintf( __('Staff member submitted. <a target="_blank" href="%s">Preview car?</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
        9 => sprintf( __('Staff member scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview car?</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
        10 => sprintf( __('Staff member draft updated. <a target="_blank" href="%s">Preview car?</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    );
    return $messages;
}
add_filter( 'post_updated_messages', 'updated_messages_staff' );


function contextual_help_staff( $contextual_help, $screen_id, $screen ) {
    if ( 'staff' == $screen->id ) {

        $contextual_help = '<h2>Testimonials</h2>
		<p>Add in staff members to the site. You\'re on your own.</p>
		<p>Contact Falkon for further assistance.</p>';

    } elseif ( 'edit-staff' == $screen->id ) {

        $contextual_help = '<h2>Editing products</h2>
		<p>This page allows you to view/modify product details. Please make sure to fill out the available boxes with the appropriate details (product image, price, brand) and <strong>not</strong> add these details to the product description.</p>';

    }
    return $contextual_help;
}
add_action( 'contextual_help', 'contextual_help_staff', 10, 3 );


function myactivationfunction_staff() {
    //my_custom_post_product();
    flush_rewrite_rules();
}
add_action("after_switch_theme", "myactivationfunction_staff", 10 ,  2);
function mydeactivationfunction_staff() {
    flush_rewrite_rules();
}
add_action("switch_theme", "mydeactivationfunction_staff", 10 ,  2);


include_once ( trailingslashit(get_stylesheet_directory()).'/metaboxes/wpalchemy/metabox.php');
include_once ( trailingslashit(get_stylesheet_directory()).'/metaboxes/staff-spec.php');

//include_once get_template_directory() . '/metaboxes/staff-image-spec.php';



add_filter( 'manage_edit-staff_columns', 'staff_columns_filter', 10, 1 );

function staff_columns_filter( $columns ) {

    //$column_thumbnail = array( 'prop-thumb' => 'Image' );
    $column_thumbnail = array( 'staff-thumb' => 'Thumbnail' );

    $column_propstatus = array( 'staff-info' => 'Information' );
    $column_desc = array( 'staff-desc' => 'Description' );

//    $column_featured = array( 'featured' => 'Featured' );

    //$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
    $columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
    $columns = array_slice( $columns, 0, 3, true ) + $column_desc + array_slice( $columns, 3, NULL, true );
//    $columns = array_slice( $columns, 0, 4, true ) + $column_featured + array_slice( $columns, 4, NULL, true );

    $columns = array_slice( $columns, 0, 2, true ) + $column_propstatus + array_slice( $columns,2, NULL, true );

    //$columns = array_slice( $columns, 0, 8, true ) + $column_propfeat + array_slice( $columns, 8, NULL, true );
    unset($columns['title']);
    unset($columns['author']);
    unset($columns['date']);
    unset($columns['comments']);
    return $columns;

}

add_action( 'manage_posts_custom_column', 'my_column_action_staff', 10,2 );

function my_column_action_staff( $column,$post_id ) {

    global $post;

    switch ( $column ) {
        case 'staff-thumb':
//			global $staff_image_mb;
//			$staff_meta = $staff_image_mb->the_meta();
            //var_dump($staff_meta);
            $labels = get_post_type_object('staff')->labels;
//            var_dump($labels);
            if(has_post_thumbnail()){
                $image_attributes = get_the_post_thumbnail_url();// wp_get_attachment_image_src( $post, 'thumbnail');
                $img_url = get_the_post_thumbnail_url();
                $img_src = '<img src="'.$img_url.'" width="100" height="auto" class="staffimg" style="border-radius:0;">';
            }
            else{
                $img_url = '';
                $img_src = '<img src="'.$img_url.'" width="100" height="auto" class="staffimg">';

            }
            //echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'">'.$img_src.'</a>';
            echo '<a href="'.admin_url('post.php?post='.$post_id.'&action=edit').'" title="'.esc_attr($labels->edit_item).'">'
            ?>
            <div id="staff-preview" class="list-preview">
                <?php echo $img_src;?>
            </div>
            <?php
            echo '</a>';
            echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'" title="'.esc_attr($labels->edit_item).'">Edit</a> | ';
            echo '<a class="submitdelete" title="Move staff member to the Trash"
            href="'.get_delete_post_link(get_the_id()).'">Trash</a>';

            break;
        case 'staff-desc':
            global $staff_mb;
            $staff_meta = $staff_mb->the_meta();
            echo wp_trim_words(get_the_content(),20,'...');
            ?>
            <?php /* ?><div class="linebreak" ></div>

            <?php
            $edit_title = get_the_title( $post->ID );
            $arr        = explode( ' ', trim( $edit_title ) );
            $edit_title = $arr[0];


            $staff_like = $staff_meta['like_group'];
            if ( $staff_like ) : ?>
                <div class="staff-like">
                    <span class="likeh4 like"><?php echo $edit_title; ?> Likes:</span>
                    <p>
                        <?php
                        for ( $count = 0; $count < count( $staff_like ); $count ++ ) :

                            $tempArray = $staff_like[ $count ];
                            echo $tempArray['like'] . '<br />';
                        endfor;
                        ?>
                    </p>
                </div>
                <?php
            endif;


            $staff_like = $staff_meta['dislike_group'];
            if ( $staff_like ) : ?>
                <div class="staff-like">
                    <span class="likeh4 dislike"><?php echo $edit_title; ?> Dislikes:</span>
                    <p>
                        <?php
                        for ( $count = 0; $count < count( $staff_like ); $count ++ ) :

                            $tempArray = $staff_like[ $count ];
                            echo $tempArray['dislike'] . '<br />';
                        endfor;
                        ?>
                    </p>
                </div>
                <?php
            endif;

            $last_letter = substr( $edit_title, - 1 );
            $edit_title  = $last_letter == 's' ? $edit_title . '\'' : $edit_title . '\'s';
            ?>
            <?php echo $edit_title;?>
            Quote:
            <?php echo wpautop('"'.$staff_meta['jobtag'].'"');?>
            <div class="linebreak"></div><?php */ ?>
            <?php
            break;
        case 'staff-info':
            global $staff_mb;
            $staff_meta = $staff_mb->the_meta();
            echo '<strong>Name:</strong> '.get_the_title().'<br />';
//            echo '<strong>Position:</strong> '.$staff_meta['jobrole'].'<br />';
//            echo '<strong>Education:</strong> '.$staff_meta['jobedu'];
            ?>
            <?php
            $customPhone = $staff_meta['phone_group'];

            if ( $customPhone ) : ?>
                <p class="staffphone">
                    <?php
                    for ( $count = 0; $count < count( $customPhone ); $count ++ ) :

                        $tempArray = $customPhone[ $count ];
                        $tempo     = 'cb_jobphone';
                        $tempVar   = $tempArray['s_jobphone'];

                        if ( ! $tempVar ) $tempVar = 'Tel';

//                        if ( $tempArray['cb_jobphone'] ) :
                            echo '<span>' . $tempVar . ': </span><a href="tel:' . str_replace( ' ', '', $tempArray['jobphone'] ) . '">' . $tempArray['jobphone'] . '</a>'.($tempArray['cb_jobphone']?' visible':'').'<br />';
//                        endif;

                    endfor;
                    ?>
                </p>
                <?php
            endif;

            $customEmail = $staff_meta['email_group'];

            if ( $customEmail ) : ?>
                <p class="staffemail">
                    <?php
                    for ( $count = 0; $count < count( $customEmail ); $count ++ ) :
                        $tempArray = $customEmail[ $count ];
                        $tempo = 'cb_jobemail';
//                        if ( $tempArray['cb_jobemail'] ) :
                            echo '<a href="mailto:' . $tempArray['jobemail'] . '" >' . $tempArray['jobemail'] . '</a>'.($tempArray['cb_jobemail']?' visible':'').'<br />';
//                        endif;
                    endfor;
                    ?>
                </p>
                <?php
            endif;
            break;
    }
}


function render_staff_thumbnail_meta_box() {
    global $post_type; // lets call the post type

    // remove the old meta box
    remove_meta_box( 'postimagediv','staff','side' );

    // adding the new meta box.
    add_meta_box('postimagediv', __('Staff Member Image'), 'new_post_thumbnail_meta_box_staff', 'staff', 'side', 'low');
}
add_action('do_meta_boxes', 'render_staff_thumbnail_meta_box');

/*
*  Add description to Featured image box
*/

function new_post_thumbnail_meta_box_staff() {
    global $post;

    $thumbnail_id = get_post_meta( $post->ID, '_thumbnail_id', true ); // grabing the thumbnail id of the post
    echo _wp_post_thumbnail_html( $thumbnail_id ); // echoing the html markup for the thumbnail

//	echo '<p>Add a custom thumbnail image for the staff, such as a user profile image or company logo.</p>
//	<p>The \'round\' cutout will be done by the site, just upload a square image
//and the site will do the rest.</p>';

}

add_filter( 'admin_post_thumbnail_html', 'add_featured_image_instruction_staff', 10, 3 );
function add_featured_image_instruction_staff(  $content, $post_id, $thumbnail_id ) {
    $post_type = get_post_type($post_id);
    if($post_type!='staff')
        return $content;

//    $content = str_replace('Set featured image','Set staff member image',$content);
//    $content = str_replace('Remove featured image','Remove staff member image',$content);
    return $content .= '<p>Add a custom thumbnail image for the member of staff, such as a user profile image.</p>
	<p>A square image, 500x500 pixels would be ideal.</p>';
}