<?php
// Register Custom Post Type 'Testimonial'
function reg_post_testimonial() {
	$labels = array(
		'name'                => _x( 'Testimonials', 'Post Type General Name', 'falkondigital' ),
		'singular_name'       => _x( 'Our Testimonials', 'Post Type Singular Name', 'falkondigital' ),
		'menu_name'           => __( 'Testimonials', 'falkondigital' ),
		'parent_item_colon'   => __( 'Parent Testimonial', 'falkondigital' ),
		'all_items'           => __( 'All Testimonials', 'falkondigital' ),
		'view_item'           => __( 'View Testimonial', 'falkondigital' ),
		'add_new_item'        => __( 'Add New Testimonial', 'falkondigital' ),
		'add_new'             => __( 'New Testimonial', 'falkondigital' ),
		'edit_item'           => __( 'Edit Testimonial', 'falkondigital' ),
		'update_item'         => __( 'Update Testimonial', 'falkondigital' ),
		'search_items'        => __( 'Search testimonials', 'falkondigital' ),
		'not_found'           => __( 'No testimonials found', 'falkondigital' ),
		'not_found_in_trash'  => __( 'No testimonials found in Trash', 'falkondigital' ),
	);

	$args = array(
		'label'               => __( 'testimonial', 'falkondigital' ),
		'description'         => __( 'Testimonials', 'falkondigital' ),
		'labels'              => $labels,
		'supports'            => array('thumbnail' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 28,
		'menu_icon'             =>'dashicons-format-quote',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'page',
		'rewrite'            => array( 'slug' => 'testimonial','with_front' => false ),
	);
	register_post_type( 'testimonial', $args );
}

// Hook into the 'init' action
add_action( 'init', 'reg_post_testimonial', 0 );


/*
 * Add custom messages for the three custom post types
 */
function updated_messages_testimonial( $messages ) {
	global $post, $post_ID;
	$messages['testimonial'] = array(
		0 => '',	// Unused. Messages start at index 1.
		1 => __('Testimonial updated.'),
		2 => __('Custom field updated.'),
		3 => __('Custom field deleted.'),
		4 => __('Testimonial updated.'),
		5 => isset($_GET['revision']) ? sprintf( __('Testimonial restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => __('Testimonial published.'),
		7 => __('Testimonial saved.'),
		8 => sprintf( __('Testimonial submitted. <a target="_blank" href="%s">Preview car?</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('Testimonial scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview car?</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('Testimonial draft updated. <a target="_blank" href="%s">Preview car?</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}
add_filter( 'post_updated_messages', 'updated_messages_testimonial' );


function contextual_help_testimonial( $contextual_help, $screen_id, $screen ) {
	if ( 'testimonial' == $screen->id ) {

		$contextual_help = '<h2>Testimonials</h2>
		<p>Add in testimonials to the site. You\'re on your own.</p>
		<p>Contact Falkon for further assistance.</p>';

	} elseif ( 'edit-testimonial' == $screen->id ) {

		$contextual_help = '<h2>Editing products</h2>
		<p>This page allows you to view/modify product details. Please make sure to fill out the available boxes with the appropriate details (product image, price, brand) and <strong>not</strong> add these details to the product description.</p>';

	}
	return $contextual_help;
}
add_action( 'contextual_help', 'contextual_help_testimonial', 10, 3 );


function myactivationfunction_testimonial() {
	//my_custom_post_product();
 	flush_rewrite_rules();
}
add_action("after_switch_theme", "myactivationfunction_testimonial", 10 ,  2);
function mydeactivationfunction_testimonial() {
 	flush_rewrite_rules();
}
add_action("switch_theme", "mydeactivationfunction_testimonial", 10 ,  2);


include_once ( trailingslashit(get_stylesheet_directory()).'/metaboxes/wpalchemy/metabox.php');
include_once ( trailingslashit(get_stylesheet_directory()).'/metaboxes/testimonial-spec.php');

//include_once get_template_directory() . '/metaboxes/testimonial-image-spec.php';



add_filter( 'manage_edit-testimonial_columns', 'testimonial_columns_filter', 10, 1 );

function testimonial_columns_filter( $columns ) {

 	//$column_thumbnail = array( 'prop-thumb' => 'Image' );
	$column_thumbnail = array( 'testimonial-thumb' => 'Thumbnail' );

	$column_propstatus = array( 'testimonial-info' => 'Information' );
	$column_desc = array( 'testimonial-desc' => 'Description' );

	$column_featured = array( 'featured' => 'Featured' );

	//$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
	$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
	$columns = array_slice( $columns, 0, 3, true ) + $column_desc + array_slice( $columns, 3, NULL, true );
	$columns = array_slice( $columns, 0, 4, true ) + $column_featured + array_slice( $columns, 4, NULL, true );

	$columns = array_slice( $columns, 0, 2, true ) + $column_propstatus + array_slice( $columns,2, NULL, true );

	//$columns = array_slice( $columns, 0, 8, true ) + $column_propfeat + array_slice( $columns, 8, NULL, true );
	unset($columns['title']);
	unset($columns['author']);
	unset($columns['date']);
	unset($columns['comments']);
	return $columns;

}

add_action( 'manage_posts_custom_column', 'my_column_action_testimonial', 10, 1 );

function my_column_action_testimonial( $column ) {

	global $post;

	switch ( $column ) {
		case 'testimonial-thumb':
//			global $testimonial_image_mb;
//			$testimonial_meta = $testimonial_image_mb->the_meta();
			//var_dump($testimonial_meta);
			if(has_post_thumbnail()){

				$image_attributes = get_the_post_thumbnail_url();// wp_get_attachment_image_src( $post, 'thumbnail');
				$img_url = get_the_post_thumbnail_url();
				$img_src = '<img src="'.$img_url.'" width="100" height="auto" class="testimonialimg" style="border-radius:50px;">';
			}
			else{

				$img_url = '';
				$img_src = '<img src="'.$img_url.'" width="100" height="auto" class="testimonialimg">';

			}
			//echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'">'.$img_src.'</a>';
			echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'" title="Edit this testimonial">'
			?>
			<div id="testimonial-preview" class="list-preview">
				<?php echo $img_src;?>
				<?php if($testimonial_meta['title_line']!='' or $testimonial_meta['tag_line']!=''){ ?>
					<div class="banner-tag">
						<div class="banner-title"><?php echo $testimonial_meta['title_line'];?></div>
						<div class="banner-text"><?php echo $testimonial_meta['tag_line'];?></div>
					</div>
				<?php } ?>
			</div>
			<?php
			echo '</a>';
			echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'" title="Edit this testimonial">Edit</a> | ';
			echo '<a class="submitdelete" title="Move this testimonial to the Trash"
            href="'.get_delete_post_link(get_the_id()).'">Trash</a>';

			break;
		case 'testimonial-desc':
			global $testimonial_mb;
			$testimonial_meta = $testimonial_mb->the_meta();
			echo wp_trim_words($testimonial_meta['description'],20,'...');
			break;
		case 'testimonial-info':
			global $testimonial_mb;
			$testimonial_meta = $testimonial_mb->the_meta();
			echo '<strong>Name:</strong> '.$testimonial_meta['name'].'<br />';
			echo '<strong>Position:</strong> '.$testimonial_meta['position'].'<br />';
			echo '<strong>Company:</strong> '.$testimonial_meta['company'];
		break;
		case 'featured':
			global $testimonial_mb;
			$testimonial_meta = $testimonial_mb->the_meta();
//			var_dump($testimonial_meta);
			echo '<strong>Featured:</strong> '.( $testimonial_meta['feat'] ? 'YES!' : 'NO');
		break;
	}
}


/*Shortcode*/
function canning_add_testimonials( $atts, $content = null ) {
//	extract(shortcode_atts(array(
//		'num' => '',
//		'title'	=>	'',
//	), $atts));
	$bullet_num = ($num) ? $num:'.';
	$service_title = ($title) ? $title:'';
	$out = '';


	$args = array(
		'numberposts'       =>  -1,
        'posts_per_page'	=>	-1,
		'paged'             =>  false,
        'post_type'         =>  'testimonial',
		'post_status'	    =>  'publish',
	    'orderby'		    =>  'menu_id date',
		'order'             =>  'ASC',
	);
	$the_query = new WP_Query( $args );


    $out .= '<div itemscope itemtype="http://schema.org/LocalBusiness">
    <span itemprop="name" style="display: none;">'.get_bloginfo( 'name' ).' Ltd</span>
    <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        <span itemprop="itemReviewed" style="display: none;">'.get_bloginfo( 'name' ).'</span>
        <span itemprop="ratingValue" content="5.0" style="display: none;">5.0</span>
        <span itemprop="reviewCount" style="display: none;">'.(get_option('canning_testimonials_num')?get_option('canning_testimonials_num'):'1').'</span>
    </span>';
    $out .= '<ul class="testimonial-list">';

	//var_dump($the_query);
	//var_dump($the_query->found_posts);

	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) :
			$the_query->the_post();
			global $testimonial_mb;
			$testimonial_meta = $testimonial_mb->the_meta();
			global $testimonial_image_mb;
			$testimonial_image_meta = $testimonial_image_mb->the_meta();
			if(is_array($testimonial_image_meta)) $testimonial_meta = array_merge($testimonial_meta,$testimonial_image_meta);
//var_dump($testimonial_meta);
			$out .= '<li itemprop="review" itemscope itemtype="http://schema.org/Review">';
			$has_image = '';
			if($testimonial_meta['image_id']){
				$default_attr = array(
					'class'	=> "testimonial-thumb",
					'alt'   => '',
				);
				$out .= wp_get_attachment_image( $testimonial_meta['image_id'], 'medium',false,$default_attr );
				$has_image = ' has-image';
			}
			$out .= '<div class="testimonial-block'.$has_image.'">';
			$out .= '<span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><meta itemprop="ratingValue" content="5.0"/></span>';
			$out .= '<meta itemprop="datePublished" content="'.get_the_date('Y-m-d').'" />';
			$out .= '<div itemprop="description">';
			$out .= wpautop($testimonial_meta['description']);
			$out .= '</div>';
            $out .= '<span class="testimonial-meta'.$has_image.'">'
                .($testimonial_meta['name']?'<span itemprop="author">'.$testimonial_meta['name'].'</span>':'')
                .''.($testimonial_meta['position']?($testimonial_meta['name']?', ':'').$testimonial_meta['position']:'')
                .''.($testimonial_meta['company']?(($testimonial_meta['name'] or $testimonial_meta['position'])?', ':'').$testimonial_meta['company']:'').'</span>';
			$out .= '</div>';
            $out .= '</li>';
		endwhile;
		wp_reset_postdata();
	}

	//$out .= do_shortcode($content);
	$out .= '</ul>';
    $out .= '</div>';
	return $out;
}
add_shortcode('cann-testimonials', 'canning_add_testimonials');





function render_new_post_thumbnail_meta_box() {
	global $post_type; // lets call the post type

	// remove the old meta box
	remove_meta_box( 'postimagediv','testimonial','side' );

	// adding the new meta box.
	add_meta_box('postimagediv', __('Testimonial Image'), 'new_post_thumbnail_meta_box', 'testimonial', 'side', 'low');
}
add_action('do_meta_boxes', 'render_new_post_thumbnail_meta_box');

/*
*  Add description to Featured image box
*/

function new_post_thumbnail_meta_box() {
	global $post;

	$thumbnail_id = get_post_meta( $post->ID, '_thumbnail_id', true ); // grabing the thumbnail id of the post
	echo _wp_post_thumbnail_html( $thumbnail_id ); // echoing the html markup for the thumbnail

//	echo '<p>Add a custom thumbnail image for the testimonial, such as a user profile image or company logo.</p>
//	<p>The \'round\' cutout will be done by the site, just upload a square image
//and the site will do the rest.</p>';

}

add_filter( 'admin_post_thumbnail_html', 'add_featured_image_instruction', 10, 3 );
function add_featured_image_instruction(  $content, $post_id, $thumbnail_id ) {
//	var_dump($content);
	$post_type = get_post_type($post_id);
	if($post_type!='testimonial')
		return $content;

	$content = str_replace('Set featured image','Set testimonial image',$content);
	$content = str_replace('Remove featured image','Remove testimonial image',$content);
	return $content .= '<p>Add a custom thumbnail image for the testimonial, such as a user profile image or company logo.</p>
	<p>The \'round\' cutout will be done by the site, just upload a square image
and the site will do the rest.</p>';
}