<?php
/**
 * cp_case-study.php
 * File for registering and setting up custom post type 'case-study'
 */

// Register Custom Post Type case-study
function falkon_register_cp_case_study() {

    $labels = array(
        'name'                => _x( 'Case Study', 'Post Type General Name', 'text_domain' ),
        'singular_name'       => _x( 'Case Study', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'           => __( 'Case Studies', 'text_domain' ),
        'name_admin_bar'      => __( 'Case Study', 'text_domain' ),
        'parent_item_colon'   => __( 'Parent Case Study:', 'text_domain' ),
        'all_items'           => __( 'All Case Studies', 'text_domain' ),
        'add_new_item'        => __( 'Add New Case Study', 'text_domain' ),
        'add_new'             => __( 'Add New Case Study', 'text_domain' ),
        'new_item'            => __( 'New Case Study', 'text_domain' ),
        'edit_item'           => __( 'Edit Case Study', 'text_domain' ),
        'update_item'         => __( 'Update Case Study', 'text_domain' ),
        'view_item'           => __( 'View Case Study', 'text_domain' ),
        'search_items'        => __( 'Search Case Studies', 'text_domain' ),
        'not_found'           => __( 'No case studies found', 'text_domain' ),
        'not_found_in_trash'  => __( 'No case studies found in Trash', 'text_domain' ),
    );
    $args = array(
        'label'               => __( 'Case Study', 'text_domain' ),
        'description'         => __( 'Case Study projects setup for iSite', 'text_domain' ),
        'labels'              => $labels,
        'supports'            => array('title', 'author', 'thumbnail','editor'),
        'taxonomies'          => array( 'case-study-type','client' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
//        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-portfolio',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => false,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'rewrite'             => false,
        'capability_type'     => 'post',
//        'capability_type'     => 'case-study',//array('case-study','case-study'),
//        'capabilities'        => $capabilities,
//        'map_meta_cap' => true,
    );
    register_post_type( 'case-study', $args );
}
add_action( 'init', 'falkon_register_cp_case_study', 0 );


add_action('init', 'case_study_rewritex');
function case_study_rewritex(){
    if ( function_exists( 'falkon_get_global_options' ) ) $falkon_option = falkon_get_global_options(); //var_dump($falkon_option);
    global $wp_rewrite;

    $case_study_page_id = (int)$falkon_option['falkon_case_study_page_id'];
//    $case_study_bi_root_page_id = (int)$falkon_option['falkon_case_study_bi'];

    $case_study_page_slug = get_relative_permalink(get_permalink($case_study_page_id));
    $case_study_page_url = untrailingslashit($case_study_page_slug);
//    $case_study_bi_root_page_slug = get_relative_permalink(get_permalink($case_study_bi_root_page_id));
//    $case_study_bi_root_page_url = untrailingslashit($case_study_bi_root_page_slug);

    $wp_rewrite->add_rewrite_tag('%cpt_name_int%', '([^/]+)', 'case-study=');
    if($case_study_page_id!=0) $wp_rewrite->add_permastruct('case_study', $case_study_page_url.'/%cpt_name_int%', false);
//    if($case_study_bi_root_page_id!=0) $wp_rewrite->add_permastruct('case_study-bi', $case_study_bi_root_page_url.'/%cpt_name_int%', false);
}

add_filter('post_type_link', 'case_study_post_permalinkx', 1, 3);
function case_study_post_permalinkx($post_link, $id = 0, $leavename) {
    global $wp_rewrite;
    $post = &get_post($id);
    if ( is_wp_error( $post ) || empty($post_link) || in_array($post->post_status, array('draft', 'pending', 'auto-draft')))
        return $post_link;

    if($post->post_type=='case-study'){
        if ( function_exists( 'falkon_get_global_options' ) ) $falkon_option = falkon_get_global_options(); //var_dump($falkon_option);
        $post_id = $post->ID;
        $get_name = $post->post_name;
        $get_name = sanitize_title($get_name);

        $newlink = $wp_rewrite->get_extra_permastruct('case_study');
        $newlink = str_replace("%cpt_name_int%", $get_name, $newlink);
        $newlink = home_url(user_trailingslashit($newlink));
        return $newlink;
    }
    return $post_link;
}


/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function falkon_save_meta_box_data_case_study( $data , $postarr ) {

    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */

    // Check if our nonce is set.
//    if ( ! isset( $_POST['myplugin_meta_box_nonce'] ) ) {
//        return;
//    }

    // Verify that the nonce is valid.
//    if ( ! wp_verify_nonce( $_POST['myplugin_meta_box_nonce'], 'myplugin_save_meta_box_data' ) ) {
//        return;
//    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
//    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
//
//        if ( ! current_user_can( 'edit_page', $post_id ) ) {
//            return;
//        }
//
//    } else {
//
//        if ( ! current_user_can( 'edit_post', $post_id ) ) {
//            return;
//        }
//    }

    /* OK, it's safe for us to save the data now. */
    remove_action( 'wp_insert_post_data', 'set_private_categories' );
    add_settings_error(
        'missing-death-star-plans',
        'missing-death-star-plans',
        'You have not specified the location for the Death Star plans.',
        'error'
    );
    set_transient( 'settings_errors', get_settings_errors(), 30 );
    add_action( 'wp_insert_post_data', 'myplugin_save_meta_box_data' );
    return false;

    // Make sure that it is set.
    if ( ! isset( $_POST['myplugin_new_field'] ) ) {
        return;
    }

    // Sanitize user input.
    $my_data = sanitize_text_field( $_POST['myplugin_new_field'] );

    // Update the meta field in the database.
    update_post_meta( $post_id, '_my_meta_value_key', $my_data );
}
//add_action( 'wp_insert_post_data', 'myplugin_save_meta_box_data' );
//add_action( 'save_post', 'myplugin_save_meta_box_data' );




// Register Custom Taxonomy - case-study-type
function create_tax_case_study_case_study_type()  {
    $labels = array(
        'name'                       => _x( 'Case Study Types', 'Taxonomy General Name', 'prolixity' ),
        'singular_name'              => _x( 'Case Study Type', 'Taxonomy Singular Name', 'prolixity' ),
        'menu_name'                  => __( 'Case Study Type', 'prolixity' ),
        'all_items'                  => __( 'All Case Study Types', 'prolixity' ),
        'parent_item'                => __( 'Parent Case Study Type', 'prolixity' ),
        'parent_item_colon'          => __( 'Parent Case Study Type:', 'prolixity' ),
        'new_item_name'              => __( 'New Case Study Type', 'prolixity' ),
        'add_new_item'               => __( 'Add New Case Study Type', 'prolixity' ),
        'edit_item'                  => __( 'Edit Case Study Type', 'prolixity' ),
        'update_item'                => __( 'Update Case Study Type', 'prolixity' ),
        'separate_items_with_commas' => __( 'Separate Case Study Type with commas', 'prolixity' ),
        'search_items'               => __( 'Search case study types', 'prolixity' ),
        'add_or_remove_items'        => __( 'Add or remove case study types', 'prolixity' ),
        'choose_from_most_used'      => __( 'Choose from the most used case study types', 'prolixity' ),
    );
    $rewrite = array(
        'slug'                       => 'case-study-type',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => false,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
        'meta_box_cb'               =>  false,
        'query_var'                 => true,
        'rewrite'                    => false,
    );
    register_taxonomy( 'case-study-type',array('case-study'), $args );

//    if(taxonomy_exists('case-study-type')){
//        if(!term_exists('retail', 'case-study-type')){
//            wp_insert_term(	'Retail', // the term
//                'case-study-type', // the taxonomy
//                array(
//                    'slug' => 'retail'
//                )
//            );
//        }
//        if(!term_exists('commercial', 'case-study-type')){
//            wp_insert_term(	'Commercial', // the term
//                'case-study-type', // the taxonomy
//                array(
//                    'slug' => 'commercial'
//                )
//            );
//        }
//        if(!term_exists('health', 'case-study-type')){
//            wp_insert_term(	'Health', // the term
//                'case-study-type', // the taxonomy
//                array(
//                    'slug' => 'health'
//                )
//            );
//        }
//    }
}
add_action( 'init', 'create_tax_case_study_case_study_type', 0 );


// Register Custom Taxonomy - case-study-sector
function create_tax_case_study_case_study_sector()  {
    $labels = array(
        'name'                       => _x( 'Case Study Sectors', 'Taxonomy General Name', 'prolixity' ),
        'singular_name'              => _x( 'Case Study Sector', 'Taxonomy Singular Name', 'prolixity' ),
        'menu_name'                  => __( 'Case Study Sector', 'prolixity' ),
        'all_items'                  => __( 'All Case Study Sectors', 'prolixity' ),
        'parent_item'                => __( 'Parent Case Study Sector', 'prolixity' ),
        'parent_item_colon'          => __( 'Parent Case Study Sector:', 'prolixity' ),
        'new_item_name'              => __( 'New Case Study Sector', 'prolixity' ),
        'add_new_item'               => __( 'Add New Case Study Sector', 'prolixity' ),
        'edit_item'                  => __( 'Edit Case Study Sector', 'prolixity' ),
        'update_item'                => __( 'Update Case Study Sector', 'prolixity' ),
        'separate_items_with_commas' => __( 'Separate Case Study Sector with commas', 'prolixity' ),
        'search_items'               => __( 'Search case study sectors', 'prolixity' ),
        'add_or_remove_items'        => __( 'Add or remove case study sectors', 'prolixity' ),
        'choose_from_most_used'      => __( 'Choose from the most used case study sectors', 'prolixity' ),
    );
    $rewrite = array(
        'slug'                       => 'case-study-sector',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => false,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
        'meta_box_cb'               =>  false,
        'query_var'                 => true,
        'rewrite'                    => false,
    );
    register_taxonomy( 'case-study-sector',array('case-study'), $args );
}
add_action( 'init', 'create_tax_case_study_case_study_sector', 0 );


// Register Custom Taxonomy - client
function create_tax_case_study_client()  {
    $labels = array(
        'name'                       => _x( 'Clients', 'Taxonomy General Name', 'prolixity' ),
        'singular_name'              => _x( 'Client', 'Taxonomy Singular Name', 'prolixity' ),
        'menu_name'                  => __( 'Work Clients', 'prolixity' ),
        'all_items'                  => __( 'All Clients', 'prolixity' ),
        'parent_item'                => __( 'Parent Client', 'prolixity' ),
        'parent_item_colon'          => __( 'Parent Client:', 'prolixity' ),
        'new_item_name'              => __( 'New Client', 'prolixity' ),
        'add_new_item'               => __( 'Add New Client', 'prolixity' ),
        'edit_item'                  => __( 'Edit Client', 'prolixity' ),
        'update_item'                => __( 'Update Client', 'prolixity' ),
        'separate_items_with_commas' => __( 'Separate Client with commas', 'prolixity' ),
        'search_items'               => __( 'Search clients', 'prolixity' ),
        'add_or_remove_items'        => __( 'Add or remove clients', 'prolixity' ),
        'choose_from_most_used'      => __( 'Choose from the most used clients', 'prolixity' ),
    );
    $rewrite = array(
        'slug'                       => 'client',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => false,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => false,
        'show_tagcloud'              => false,
        'meta_box_cb'               =>  false,
        'rewrite'                    => $rewrite,
    );
    register_taxonomy( 'client',array('case-study'), $args );
}
//add_action( 'init', 'create_tax_case_study_client', 0 );


function change_default_title_case_study( $title ){
    $screen = get_current_screen();
    if  ( 'case-study' == $screen->post_type ) {
        $title = 'Enter case study name here';
    }
    return $title;
}
add_filter( 'enter_title_here', 'change_default_title_case_study' );

/**
 * Setup meta boxes using WPAlchemy for backend management and filter/save use on frontend.
 */
//if ( is_admin()) add_action( 'admin_enqueue_scripts', 'my_metabox_styles_school' );
function my_metabox_styles_case_study()
{
    wp_enqueue_style( 'wpalchemy-metabox-case_study', get_stylesheet_directory_uri() . '/metaboxes/meta-case_study.css');
}

$case_study_mb = new WPAlchemy_MetaBox(array
(
    'id' => '_case_study_meta',
    'title' => 'Case Study Meta Info',
    'types' => array('case-study'), // added only for pages and to custom post type "events"
    'context' => 'normal', // same as above, defaults to "normal"
    'priority' => 'high', // same as above, defaults to "high"
    'save_filter' => 'save_filter_case_study_data',
    'save_action' => 'save_action_case_study_meta',
    'template' => get_stylesheet_directory() . '/metaboxes/case-study-meta.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_case_study_',
));

function save_filter_case_study_data($meta, $post_id){
//    var_dump('dfsdf');
//exit;
//    if ( 'case-study' == $_POST['post_type'] && !current_user_can( 'edit_case-study', $post_id )) {
//        return $post_id;
//    }

//    var_dump($_POST);
//    exit;
//    if(isset($_POST['case-study_client'])) {
//        $meta['client_name'] = $_POST['case-study_client'];
//    }
//    else
//        unset($meta['client_name']);
//        add_settings_error(
//            'missing-case_study-meta-address_country',
//            'missing-case_study-meta-address_country',
//            'You must provide the case_study country.',
//            'error'
//        );
//    }

//    if(isset($meta['user_fname'])){
//        $meta['user_fname'] = 'bob';
//    }
//        add_settings_error(
//            'missing-case_study-meta-address_country',
//            'missing-case_study-meta-address_country',
//            'You must provide the case_study country.',
//            'error'
//        );
//    }
//    if(!isset($meta['address_country']) or $meta['address_country']==''){
//        add_settings_error(
//            'missing-case_study-meta-address_country',
//            'missing-case_study-meta-address_country',
//            'You must provide the case_study country.',
//            'error'
//        );
//    }
//    if(!isset($meta['client_id']) or $meta['client_id']==''){
//        add_settings_error(
//            'missing-case_study-meta-client',
//            'missing-case_study-meta-client',
//            'You must select a client from the drop down.',
//            'error'
//        );
//    }
//    var_dump($meta);
//    exit;
//    add_settings_error(
//        'missing-death-star-plans',
//        'missing-death-star-plans',
//        'You have not specified the location for the Death Star plans.',
//        'error'
//    );
//
//    add_settings_error(
//        'invalid-email',
//        '',
//        'You must define a value in the meta box.',
//        'error'
//    );

//    set_transient( 'settings_errors', get_settings_errors(), 30 );
//    return false;

//    var_dump($meta['equipment_required']);
//    exit;
    //Check for blank entries with qty = 1

//    var_dump($meta);


//exit;
//    if($meta['date_received']=='') unset($meta['date_received']);
//    if($meta['date_received']=='dd/mm/yyyy') unset($meta['date_received']);
//    if($meta['date_received']){
//        $meta['date_received'] = strtotime(str_replace('/','-',$meta['date_received']));
//    }

//    if($meta['date_required']=='') unset($meta['date_required']);
//    if($meta['date_required']=='dd/mm/yyyy') unset($meta['date_required']);
//    if($meta['date_required']){
//        $meta['date_required'] = strtotime(str_replace('/','-',$meta['date_required']));
//    }

//    if($meta['date_awarded']=='') unset($meta['date_awarded']);
//    if($meta['date_awarded']=='mm/yyyy') unset($meta['date_awarded']);
//    var_dump($meta['date_awarded']);
//    if($meta['date_awarded']){
//        $meta['date_awarded'] = strtotime("01-".str_replace('/','-',$meta['date_awarded']));
//    }
//    var_dump($meta['date_awarded']);
//exit;

//    if(isset($meta['equipment_required']) and is_array($meta['equipment_required'])){
//        foreach($meta['equipment_required'] as $key => $equipment){
//            if(count($equipment)==1 and isset($equipment['qty']))
//                unset($meta['equipment_required'][$key]);
//
//            if($equipment['equipment_id']=='' and $equipment['equipment_id_other']=='')
//                unset($meta['equipment_required'][$key]);
//            if($equipment['equipment_id'])
//            equipment_id_other
//        }
//    }
    //Re-index array
//    $meta['equipment_required'] = array_values($meta['equipment_required']);
//    var_dump($meta['equipment_required']);
//exit;

//    if(isset($meta['value']))
//        $meta['value'] = format_price($meta['value']);

//    var_dump($meta);
//exit;

    return $meta;
}


function save_action_case_study_meta($meta,$post_id){
    // Check permissions
    if ( 'case-study' == $_POST['post_type'] && !current_user_can( 'edit_posts', $post_id )) {
        return $post_id;
    }

//    var_dump($_POST);
//    exit;
    //Save Taxonomy to taxonomy metas
    //out-work-type
//    $ourtaxonomy = $_POST['case-study_case-study-type'];
//    wp_set_object_terms( $post_id, $ourtaxonomy, 'case-study-type' );
//
//    //client
//    $ourtaxonomy = $_POST['case-study_case-study-sector'];
//    wp_set_object_terms( $post_id, $ourtaxonomy, 'case-study-sector' );

    $ourtaxonomy = (array)$_POST['_case_study_meta']['cs_base_category'];
    $ourtaxonomy = array_map( 'intval', $ourtaxonomy );
    $ourtaxonomy = array_unique( $ourtaxonomy );

    if(empty($ourtaxonomy)) $ourtaxonomy = null;

    wp_set_object_terms( $post_id, $ourtaxonomy, 'case-study-type' );

    //client
    $ourtaxonomy = (array)$_POST['_case_study_meta']['cs_base_sector'];
    $ourtaxonomy = array_map( 'intval', $ourtaxonomy );
    $ourtaxonomy = array_unique( $ourtaxonomy );

    if(empty($ourtaxonomy)) $ourtaxonomy = null;

    wp_set_object_terms( $post_id, $ourtaxonomy, 'case-study-sector' );

}



add_filter( 'manage_case_study_posts_columns', 'case_study_columns_filter', 10, 1 );
function case_study_columns_filter( $columns ) {
//    $column_status = array( 'case_study-status' => 'Status' );
//    $column_average_score = array( 'case_study-average-score' => 'Av Score' );
//    $column_average_time = array( 'case_study-average-time' => 'Av Time' );
//    $column_attempts_review = array( 'case_study-attempts-r' => 'Attempts Review' );
//    $column_attempts_practice = array( 'case_study-attempts-p' => 'Attempts Practice' );
//    $column_downloads = array( 'case_study-downloads' => 'Downloads' );

//    $columns = array_slice( $columns, 0, 3, true ) + $column_status + array_slice( $columns, 3, NULL, true );
//    $columns = array_slice( $columns, 0, 4, true ) + $column_average_score + array_slice( $columns, 4, NULL, true );
//    $columns = array_slice( $columns, 0, 5, true ) + $column_average_time + array_slice( $columns, 5, NULL, true );
//    $columns = array_slice( $columns, 0, 6, true ) + $column_attempts_review + array_slice( $columns, 6, NULL, true );
//    $columns = array_slice( $columns, 0, 7, true ) + $column_attempts_practice + array_slice( $columns, 7, NULL, true );
//    $columns = array_slice( $columns, 0, 4, true ) + $column_downloads + array_slice( $columns, 4, NULL, true );

//    unset($columns['title']);
//    unset($columns['author']);
    unset($columns['date']);
    unset($columns['comments']);
    return $columns;
}
add_action( 'manage_case_study_posts_custom_column', 'case_study_column_action', 10, 1 );
function case_study_column_action( $column ) {
    global $post;
    switch ( $column ) {

    }
}


add_action('pre_get_posts','db_query_alter_query_case_study');
function db_query_alter_query_case_study($query){

    global $post;
    global $falkon_option;

    if (!is_admin()
        and !$query->is_main_query()
        and $post->ID == (int)$falkon_option['falkon_case_study_page_id']
        and $query->get('post_type')=='case-study') {

        //Do something to main query
        //echo '<pre>'; var_dump($_GET); echo '</pre>'; die;
        //echo '<pre>'; var_dump($query); echo '</pre>'; die;

        $orderby = 'menu_order title';//"&orderby=cost&order=DESC";
        $order = "ASC";

        //Set order by if set in URL
        if (isset($_GET['orderby'])) {
            switch ($_GET['orderby']) {
                case 'client':
                    $orderby = 'meta_value';//"&orderby=cost&order=DESC";
                    $order = "ASC";
                    $metaorder = "_case_study_client_name";
                    //$query->set('meta_key',$metaorder );
                    break;
            }
        }
        $query->set('meta_key', $metaorder);

        //Create the exclusion rule
        $new_tax_query = '';

        if(isset($_GET['work-type']) and $_GET['work-type']!=''){
            $makes = explode(",", $_GET['work-type']);
            $new_tax_query = array(
                'taxonomy' => 'case-study-type',
                'field'    => 'slug',
                'terms'    => $makes,
            );
        }

        //Create the exclusion rule
        $new_tax_query_sector = '';

        if(isset($_GET['work-sector']) and $_GET['work-sector']!=''){
            $makes = explode(",", $_GET['work-sector']);
            $new_tax_query_sector = array(
                'taxonomy' => 'case-study-sector',
                'field'    => 'slug',
                'terms'    => $makes,
            );
        }


        //If there is already a tax_query, 'AND' our new rule with the entire existing rule.
        $tax_query = $query->get('tax_query');
        if($tax_query=='') $tax_query = array();
        if(!empty($tax_query)) {
            $new_tax_query = array(
                'relation' => 'AND',
                $tax_query,
                $new_tax_query,
                $new_tax_query_sector,
            );
        }
        else{
            $new_tax_query = array(
                'relation' => 'AND',
                $new_tax_query,
                $new_tax_query_sector,
            );
        }
        $query->set('tax_query',$new_tax_query);

        //If there is already a meta_query, 'AND' our new rule with the entire existing rule.
        $meta_query = $query->get('meta_query');
        //var_dump($meta_query);
        if(!empty($meta_query)) {
            $new_meta_query = array(
                'relation' => 'AND',
                $meta_query,
                $new_meta_query,
            );
        }
        else{
            $new_meta_query = array(
                'relation' => 'AND',
                $new_meta_query,
            );
        }
        //var_dump($meta_query);
        $query->set('meta_query', $new_meta_query);

        //Finally set the order and orderby from the above
        $query->set('orderby',$orderby );
        $query->set('order',$order );
//        var_dump($query);
    }
}