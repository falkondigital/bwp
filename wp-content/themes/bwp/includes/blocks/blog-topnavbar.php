<?php
/*
 *
 */
?>
<div class="bg-white">
    <div class="container">
        <div class="row py-3">
            <?php
            global $wp_query;
            $page = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
            $ppp = get_query_var('posts_per_page');
            $total = $wp_query->found_posts;

            $end = ($total<=($ppp * $page))? $total : ($ppp * $page);
            $start = $page==1? 1: ($ppp * ($page-1)) + 1;
            if($start<0) $start = 1;
            //          echo "Showing ".($total=='1'?$total.' post':"$start-$end of $total posts");
            ?>
            <div class="col">
                <div id="archive-menu" class="row  align-items-center">
                    <div class="col">
                        <form class="form-inline">
                            <label class="h5 mr-sm-2" for="inlineFormCustomSelectPref">View posts by Blog Category</label>
                            <?php wp_dropdown_categories( 'show_option_none=All Categories' ); ?>
                        </form>
                        <script type="text/javascript">

                            var dropdown = document.getElementById("cat");
                            function onCatChange() {
                                if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
                                    location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
                                }
                            }
                            dropdown.onchange = onCatChange;
                        </script>
                    </div>
                    <div class="col search-lister text-right">
                        <?php get_search_form();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
