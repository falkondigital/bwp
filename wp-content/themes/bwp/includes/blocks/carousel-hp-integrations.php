<?php
// Falkon Options
global $falkon_option;

if(isset($falkon_option['falkon_homepage_integrations_images'])) {
    $result  = '<section id="integrations-logo-block" class="">';
    $result .= '<div class="container">';
    $result .= '<div class="row justify-content-center">';
    $thumb_array = $falkon_option['falkon_homepage_integrations_images'];
    foreach ($thumb_array as $image_id) {
        $image_attributes = wp_get_attachment_image_src($image_id, 'full-size');
        $img_meta       = wp_prepare_attachment_for_js($image_id);
        $image_title    = $img_meta['title'] == '' ? '' : $img_meta['title'];
        $image_alt      = $img_meta['alt'] == '' ? 'Featured integration logo '.$image_title : $img_meta['alt'];

        $img_src = wp_get_attachment_image_url( $image_id, 'medium' );
        $img_srcset = wp_get_attachment_image_srcset( $image_id,'full' );
        $img_arg_array = array(
            "title" =>  $image_title,
            "alt" =>  $image_alt,
            "class" => "img-responsive feedimg"
        );
        $result .= '<div class="col-4 col-sm-3 col-md-2 mb-3">';
        $result .= '<div class="item">';
        $result .= wp_get_attachment_image( $image_id, 'medium',"", $img_arg_array);
        $result .= '</div>';
        $result .= '</div>';
    }
    $result .= '</div>';
    $result .= '</div>';
    $result .= '</section>';
    echo $result;
}
?>