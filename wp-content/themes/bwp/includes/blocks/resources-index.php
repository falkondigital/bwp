<?php
/**
 *
 */
global $falkon_option;

$resource_page_id = (int)$falkon_option['falkon_resource_page_id'];

$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$args = array(
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'resource',
    'post_status' => 'publish',
//    'posts_per_page' => $hp_news_num,
    'ignore_sticky_posts'	=> 1,
    'post__not_in' => get_option( 'sticky_posts' ),
    'paged' => $paged
);

$recent_posts = new WP_Query( $args );
if ( $recent_posts->have_posts() ) :
    $xyz = 1;
    echo '<div class="row blog-list-vertical">';
    while ( $recent_posts->have_posts() ) : $recent_posts->the_post();
        include(locate_template('includes/loops/loop-resource-vertical.php'));
    endwhile;
    wp_reset_postdata();
    echo '</div><!--row-->';
endif;
//            echo '
//                          <div class="browse-mre text-center">
//                             <a href="'.$view_all_news_link.'" rel="nofollow" class="viewalllink hidden-xs" title="Browse all News Items">
//              		  	      	View All News Items <i class="fa fa-angle-right" aria-hidden="true"></i>
//                	        </a>
//                	      </div>
//                	       ';