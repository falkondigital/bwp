<?php
/**
 *
 */
global $falkon_option;

if($falkon_option['falkon_finalcta_title']){ ?>
    <!-- Final Call To Action -->
    <div id="cta-final-home-wrapper" class="bg-white">
        <div class="container">
            <div class="row py-8">
                <div class="col text-center">
                    <?php echo ($falkon_option['falkon_finalcta_title']?'<p class="h1 lato-black">'.$falkon_option['falkon_finalcta_title'].'</p>':'');?>
                    <?php echo ($falkon_option['falkon_finalcta_tagline']?'<p class="h3 font-weight-light">'.$falkon_option['falkon_finalcta_tagline'].'</p>':'');?>
                    <?php
                    if(is_array($falkon_option['falkon_finalcta_buttons'])){
                        $btn_num = 0;
                        foreach(array($falkon_option['falkon_finalcta_buttons']) as $buttons_array_temp){
                            foreach($buttons_array_temp as $key => $button_array_tmp) {
                                if ($button_array_tmp['url'] != '') $btn_num++;
                            }
                        }
                        echo '<div class="row">';
                        foreach(array($falkon_option['falkon_finalcta_buttons']) as $buttons_array){
                            foreach($buttons_array as $key => $button_array){
                                if($button_array['url']) echo '<div class="col-12 col-sm-'.(12/$btn_num).' '.($key==0?($btn_num==1?'':'text-sm-right'):($key==0?'text-sm-right':'text-sm-left')).' text-center">
                                <a href="'.esc_url($button_array['url']).'" title="'.esc_attr($button_array['title']).'"
                                class="btn btn-'.($key==0?'':'outline-').'primary btn-lg">'.$button_array['title'].'</a></div>';
                            }
                        }
                        echo '</div>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

