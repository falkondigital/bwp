<?php
/**
 *
 */
?>
<?php

global $case_study_mb;
$related_meta_items = $case_study_mb->the_meta();

$currentId = $post->ID;
$related_items = isset($related_meta_items['related_items'])?(array)$related_meta_items['related_items']:array();
$custom_post = 'case-study';
$custom_post_object = get_post_type_object( $custom_post );
$custom_post_name = $custom_post_object->labels->name;
$xyz=0;
$number_of_related_posts = 3;//$falkon_option['falkon_related_motorhome_num']? $falkon_option['falkon_related_motorhome_num'] : 3;

//var_dump($falkon_option);
//var_dump($number_of_related_posts);

$exisiting_posts = array();
$exisiting_posts[] = $currentId;
$exisiting_posts = array_merge($related_items,$exisiting_posts);

//var_dump($exisiting_posts);
if(count($related_items)<$number_of_related_posts){
    $args = array(
        'posts_per_page'    =>	($number_of_related_posts-count($related_items)),
        'post_type'         =>  $custom_post,
        'post_status'	    =>  'publish',
        'post__not_in'      =>  $exisiting_posts,
        'orderby'		    => 'rand',
        'fields'            =>  'ids'
    );
    $extra_posts = new WP_Query( $args );
//    var_dump($extra_posts);
    if($extra_posts->have_posts()) {
        $related_items = array_merge($related_items,$extra_posts->posts);
        wp_reset_postdata();
    }
}
if(count($related_items)>0){
    $args = array(
        'post_type'         =>  $custom_post,
        'post_status'	    =>  'publish',
        'post__in'          =>  $related_items,
        'orderby'		    => 'post__in',
        'posts_per_page'    =>	$number_of_related_posts,
    );
    $related_query = new WP_Query( $args );



    if($related_query->have_posts()){
        $output = '';
        echo '<section id="ow-pgfeed" class="pt-8 pb-7"><div class="container"><div class="row">';
        $related_posts_title = $falkon_option['falkon_related_title_text']? $falkon_option['falkon_related_title_text'] : 'Related Case Studies';
        echo '<div class="col-12  text-center text-sm-left"><span class="h2 pb-3">'.$related_posts_title.'</span></div>';
        while ( $related_query->have_posts() ) :
            $related_query->the_post();
            //THE RELATED LOOP, do your stuff here...
            global $genpage_meta;
//					$general_meta = $genpage_meta->the_meta();
//					global $banner_mb;
//					$banner_meta = $banner_mb->the_meta();
            global $case_study_mb;
            $case_study_meta = $case_study_mb->the_meta();
//					var_dump($general_meta);

            if(isset($case_study_meta['content_short'])) {
                $ow_tagline = $case_study_meta['content_short'] ? $case_study_meta['content_short'] : wp_trim_words( get_the_content(), 10 );
            }
            else {
                $ow_tagline =  wp_trim_words( get_the_content(), 10 );
            }
            if((int)$case_study_meta['cs_primary_category']!=0){
                $category[] = get_term( (int)$case_study_meta['cs_primary_category'], 'case-study-type' );

            }
            else{
                $category = get_the_terms( $subpages->ID, 'case-study-type' );
            }
//					var_dump($category);
            $catname = '';
            if(is_array($category)){	//get_the_terms will return an array if there are any, otherwise false or wp_error
                $our_work_page_id = $falkon_option['falkon_case_study_page_id'];

                $catname =  '<a href="'.get_permalink($our_work_page_id).'?work-type='.$category[0]->slug.'" class="imgcattag" title="'.esc_attr("View all case studies with tag '".$category[0]->name."'").'">'.$category[0]->name.'</a>';
            }

            $output .= '<div class="col-xs-12 col-sm-6 col-md-4" style="margin-bottom:35px;"><div class="col-xs-12 col-sm-12 ourwk-text-bg np text-center">';
            if(has_post_thumbnail()){
                $output .= '<div class="ow-feat-img">
						<a href="'.get_permalink().'" rel="nofollow" title="'.esc_attr(get_the_title()).'">'. get_the_post_thumbnail('','full', array('class'=>'child-img')).'</a>';

                $output .= '<span class="hidden-xs">'.$catname.'</span>';
                $output .= '</div>';
            }
            $output .= '<a href="'.get_permalink().'" title="'.esc_attr(get_the_title()).'">';
            $output .= '<h4>' . get_the_title() . '</h4>';
            $output .= '<p>'.$ow_tagline.'<br />';
            $output .= '</p></a></div></div>';
            if($xyz%2 == 0) $output .= '<div class="clearfix visible-xs-block"></div>';
            if($xyz++%3 == 0) $output .= '<div class="clearfix hidden-xs-block"></div>';


        endwhile;
        wp_reset_postdata();
        echo $output;
        echo '</div></div></section>';
    }
}

