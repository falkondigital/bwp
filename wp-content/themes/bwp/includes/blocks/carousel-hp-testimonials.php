<?php
$args = array(
'orderby' => 'menu_order post_date',
'order' => 'ASC',
'post_type' => 'testimonial',
'post_status' => 'publish',
'posts_per_page' => -1,
);
$recent_posts = new WP_Query( $args );
$slide_num = 0;
if ( $recent_posts->have_posts() ) :
    $slide_num = $recent_posts->post_count;
    ?>
    <?php
    // Falkon Options
    global $falkon_option;
//if(isset($falkon_option['falkon_homepage_client_images'])) {
    $result = '<section id="testimonials-carousel" class="mt-8">';
//        $result .= '<div class="container">';
    $result .= '<div class="row justify-content-center">';
    $result .= '<div class="col-sm-10">';

    $thumb_array = $falkon_option['falkon_homepage_client_images'];
    //    var_dump($thumb_array);
    $result .= '<div id="carousel-testimonials" class="owl-carousel owl-theme">';
    while ( $recent_posts->have_posts() ) : $recent_posts->the_post();

        global $testimonial_mb;
        $testimonial_meta = $testimonial_mb->the_meta();


//var_dump($testimonial_meta);
        $result .= '<div class="item row" itemprop="review" itemscope itemtype="http://schema.org/Review">';
        $has_image = '';
        if(has_post_thumbnail()){
            $default_attr = array(
                'class'	=> "testimonial-thumb",
                'alt'   => '',
            );
            $image_attributes = get_the_post_thumbnail_url();// wp_get_attachment_image_src( $post, 'thumbnail');
            $img_url = get_the_post_thumbnail_url();
            $image_id = get_post_thumbnail_id();
            $image_attributes = wp_get_attachment_image_src($image_id, 'full-size');
//                        var_dump($image_attributes);
            $img_meta       = wp_prepare_attachment_for_js($image_id);
            $image_title    = $testimonial_meta['name'] == '' ? $img_meta['title']:$testimonial_meta['name'];
            $image_alt      = $img_meta['alt'] == '' ? $image_title : $img_meta['alt'];


            $img_src = '<div class="col-6 offset-3 offset-md-0 col-md-4"><img src="'.$img_url.'" title="'.$image_title.'" alt="'.$image_alt.'" width="100" height="auto" class="testimonialimg" style="border-radius:147px;"></div>';


//            $out .= wp_get_attachment_image( $testimonial_meta['image_id'], 'medium',false,$default_attr );
            $result .= $img_src;
            $has_image = ' has-image';
        }
        $result .= '<div class="w-100 d-block d-md-none"></div>';
        $result .= '<div class="testimonial-block'.$has_image.' col-12 '.($has_image==''?'col-md-12':'col-md-8').' text-center align-self-center">';
        $result .= '<span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><meta itemprop="ratingValue" content="5.0"/></span>';
        $result .= '<span itemprop="itemReviewed" itemscope itemtype="http://schema.org/Service">';
        $result .= '<!-- Product properties -->';
        $result .= '</span>';
        $result .= '<meta itemprop="datePublished" content="'.get_the_date('Y-m-d').'" />';
        $result .= '<div itemprop="description">';
        $result .= '<blockquote>'.wpautop($testimonial_meta['description']).'</blockquote>';
        $result .= '</div>';
        $result .= '<span class="testimonial-meta'.$has_image.' h5 font-weight-bold">'
            .($testimonial_meta['name']?'<span itemprop="author" class="font-weight-normal">'.$testimonial_meta['name'].'</span>':'')
            .(isset($testimonial_meta['position'])?($testimonial_meta['name']?'<span class="font-weight-normal">,</span> ':'').'<span class="font-weight-normal">'.$testimonial_meta['position'].'</span>':'')
            .($testimonial_meta['company']?(($testimonial_meta['name'] or $testimonial_meta['position'])?'<br>':'').$testimonial_meta['company']:'').'</span>';
        $result .= '</div>';
        $result .= '</div>';


//        $result .= '<div class="item">';
//        $result .= '<img class="owl-lazy"
//                        data-src="' . $image_attributes[0] . '"
//                        '.$retina_image.'
//                        alt="'.$image_alt.'"
//                        >'; //src="' . $image_attributes[0] . '"
//        $result .= '</div>';
        ?>
        <?php
        endwhile;
    wp_reset_postdata();

    $result .= '</div>';
    $result .= '</div>';
//    $result .= '<div class="w-100"></div><div id="customNavContainer"></div>';

    $result .= '</div>';
//            $result .= '</div>';
    $result .= '</section>';
    echo $result;
    //}
    ?>
    <?php
    $client_num = $slide_num;
    $carousel_loop = $client_num>1?'true':'false';
    $mouse_drag = $client_num>1?'true':'false';
    $touch_drag = $client_num>1?'true':'false';
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var owlBrand = $('#carousel-testimonials');
            owlBrand.owlCarousel({
                items:1,
                center: true,
                margin:30,
    //                center:true,
//                nav:false,
//                dots:true,
                lazyLoad:true,
                lazyLoadEager:1,
    //                loop:true,
                loop: <?php echo $carousel_loop;?>,
                mouseDrag:<?php echo $mouse_drag;?>,
                touchDrag:<?php echo $touch_drag;?>,
                autoplay: <?php echo $carousel_loop;?>,
    //                autoplayTimeout:6000,
    //                autoplayTimeout: 7000,
                autoplayHoverPause: true,
                animateOut: 'fadeOut',
//                autoHeight:true
//                autoplaySpeed: 1500,
    //                dotsSpeed: 1500,
            });
        });
    </script>
    <?php
endif;
?>