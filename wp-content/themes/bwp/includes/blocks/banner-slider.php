<?php
$args = array(
    'orderby' => 'menu_order post_date',
    'order' => 'ASC',
    'post_type' => 'slide',
    'post_status' => 'publish',
    'posts_per_page' => -1,
);
$recent_posts = new WP_Query( $args );
$slide_num = 0;
if ( $recent_posts->have_posts() ) :
    $slide_num = $recent_posts->post_count;
    ?>
        <div id="home-page-slider" class="owl-carousel">
        <?php
        while ( $recent_posts->have_posts() ) : $recent_posts->the_post();
            global $slide_mb;
            $slide_meta = $slide_mb->the_meta();
            if($slide_meta['image_id']){

                $image_attributes = wp_get_attachment_image_src( $slide_meta['image_id'], 'slide-image-full');
                $img_url = $image_attributes[0];
                $img_src = '<img src="'.$img_url.'" class="slideimg">';
            }
            else{
                $img_url = get_template_directory_uri().'/images/default-slider.jpg';
                $img_src = '<img src="'.$img_url.'" class="slideimg default-image">';
            }
	        echo '<div>';
            echo '<div class="item banner-bg">';
            echo $img_src;
            if($slide_meta['title_line']!='' or $slide_meta['tag_line']!=''){ ?>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <div class="banner-tag <?php echo 'tagbox-'.$slide_meta['box_side'];?>">
                                <div class="middle">
                                    <div class="inner">
                                        <div class="banner-title"><?php echo $slide_meta['title_line'];?></div>
                                        <?php echo $slide_meta['tag_line']? '<div class="banner-text">'.$slide_meta['tag_line'].'</div>':'';?>
                                        <?php
                                        $btn_num = 0;
                                        if($slide_meta['button_ghost_text']!='') $btn_num++;
                                        if($slide_meta['button_text']!='') $btn_num++;
                                        if($btn_num>0){
                                        ?>
	                                    <div class="banner-btn row">
                                               <?php
                                                  if($slide_meta['button_ghost_text']!='' and $slide_meta['link_button_ghost']!=''){
                                                      $key = 0;
                                                    echo '<div class="col-12 col-sm-'.(12/$btn_num).' '.($btn_num==1?'':'text-sm-right').' text-center"><a href="'.esc_url($slide_meta['link_button_ghost']).'" class="btn btn-outline-white btn-lg btn-slider" >'.$slide_meta['button_ghost_text'].'</a></div>';
                                                      $key++;
                                                  }
                                               ?>
                                             <?php
                                               if($slide_meta['button_text']!='' and $slide_meta['link_button']!=''){
                                                 echo '<div class="col-12 col-sm-'.(12/$btn_num).' '.($btn_num==1?'':($key==0?'text-sm-right':'text-sm-left')).' text-center"><a href="'.esc_url($slide_meta['link_button']).'" class="btn btn-primary btn-lg btn-slider" >'.$slide_meta['button_text'].'</a></div>';
                                               }
                                             ?>
		                                </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
            echo '</div>';
	        echo '</div>';
            ?>
            <?php
        endwhile;
        wp_reset_postdata();
        ?>
    </div>
    <?php
    $carousel_loop = $slide_num>1?'true':'false';
    $mouse_drag = $slide_num>1?'true':'false';
    $touch_drag = $slide_num>1?'true':'false';
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var owlSlider = $('#home-page-slider');
            owlSlider.owlCarousel({
                items: 1,
                lazyLoad:true,
//                lazyLoadEager:1,
                loop: <?php echo $carousel_loop;?>,
                mouseDrag:<?php echo $mouse_drag;?>,
                touchDrag:<?php echo $touch_drag;?>,
                margin: 0,
                autoplay: false<?php //echo $carousel_loop;?>,
                autoplayTimeout:6000,
//                autoplayTimeout: 7000,
                autoplayHoverPause: true,
                autoplaySpeed: 1500,
                dotsSpeed: 1500
            });

            $('.play').on('click', function () {
                owlSlider.trigger('play.owl.autoplay', [1000])
            })

            $('.stop').on('click', function () {
                owlSlider.trigger('stop.owl.autoplay')
            })
        });
    </script>
    <?php
endif;
?>
<?php /* ?><script>
    jQuery(document).ready(function($) {
        $('.homepage-slider').bxSlider({
            auto:true,
            pause:7000,
            preloadImages:'all',
//            controls:true
            nextText:'',
            prevText:'',
            adaptiveHeight: true
        });

        var banner_height = $('.bx-wrapper').height();
        $('.banner-tag').css('height',banner_height+"px");
        var banner_page_height = $('.bannerimg').height();
        $('.banner-tag').css('height',banner_page_height+"px");
    });
</script><?php */ ?>