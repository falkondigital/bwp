<?php
/**
 *
 */
?>
<div class="bg-light">
    <div class="container">
        <div class="row align-items-center py-6">
            <!-- Pagination -->
            <?php /* ?><div class="col-3 text-left pagination-showing">
                <?php
                echo "<span class='h5'>Showing ".($total=='1'?$total.' post':"$start-$end of $total posts")."</span>";
                ?>
            </div><?php */ ?>
            <div class="col">
                <?php if ( function_exists('b4st_pagination') ) { b4st_pagination(); } else if ( is_paged() ) { ?>
                    <ul class="pagination">
                        <li class="older"><?php next_posts_link('<i class="glyphicon glyphicon-arrow-left"></i> ' . __('Previous', 'wbst')) ?></li>
                        <li class="newer"><?php previous_posts_link(__('Next', 'wbst') . ' <i class="glyphicon glyphicon-arrow-right"></i>') ?></li>
                    </ul>
                <?php } ?>
            </div>
            <?php /* ?><div class="col-xs-6 col-sm-3" id="sidebar" role="navigation">
              <?php include(locate_template('includes/sidebar.php')); ?>
          </div><?php */ ?>
        </div>
    </div><!-- /.container -->
</div>
