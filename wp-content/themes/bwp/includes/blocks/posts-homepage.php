<?php
/**
 *
 */
global $falkon_option;

$hp_news_num = $falkon_option['falkon_homepage_news_num']? $falkon_option['falkon_homepage_news_num'] : 3;

$args = array(
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => $hp_news_num,
    'ignore_sticky_posts'	=> 1,
    'post__not_in' => get_option( 'sticky_posts' ),
);

$recent_posts = new WP_Query( $args );
if ( $recent_posts->have_posts() ) :
    $xyz = 1;
    echo '<div class="row blog-list-vertical">';
    while ( $recent_posts->have_posts() ) : $recent_posts->the_post();
        include(locate_template('includes/loops/loop-post-vertical.php'));
    endwhile;
    wp_reset_postdata();
    echo '</div><!--row-->';
endif;
//            echo '
//                          <div class="browse-mre text-center">
//                             <a href="'.$view_all_news_link.'" rel="nofollow" class="viewalllink hidden-xs" title="Browse all News Items">
//              		  	      	View All News Items <i class="fa fa-angle-right" aria-hidden="true"></i>
//                	        </a>
//                	      </div>
//                	       ';