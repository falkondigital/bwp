<?php
/**
 *
 */
global $post;
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-'.((is_front_page() or is_single())?'6':'6').' post-vertical mb-3'); ?>>
    <div class="inner pb-6 h-100">
    <?php $block_excerpt = wp_trim_words(get_the_excerpt(), 30, '' ); ?>
    <?php if(has_post_thumbnail($post->ID)){
        $imgSRC = get_the_post_thumbnail( $post->ID,'thumbnail' );
        ?>
        <div class="newsimg"><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow" class="pb-3 d-block"><?php echo $imgSRC;?></a></div>
    <?php
    }
    else{
        $args = array(
            'numberposts' => 1,
            'order'=> 'ASC',
            'post_mime_type' => 'image',
            'post_parent' =>  $post->ID,
            'post_status' => 'inherit',
            'post_type' => 'attachment',
            'orderby' => 'menu_order ID'
        );
        $attachments = get_children( $args );
        if ($attachments) {
            foreach($attachments as $attachment) {
                $image_attributes = wp_get_attachment_image_src( $attachment->ID, 'thumbnail' );
                $imgSRC = '<img src="'.$image_attributes[0].'" width="'.$image_attributes[1].'" height="'.$image_attributes[2].'" class="current">';
            }
            ?>
            <div class="newsimg"><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow" class="pb-3 d-block"><?php echo $imgSRC;?></a></div>
        <?php
        }
        else{
            $imgSRC = '<img src="'.get_template_directory_uri().'/images/default-thumbnail-360x225.jpg" width="360" height="225" class="">';
            ?>
            <div class="newsimg"><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow" class="pb-3 d-block"><?php echo $imgSRC;?></a></div>
        <?php }
    }
    ?>
        <div class="inner-text">
            <p class="h4 blog-title font-weight-bold "><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="">
                    <?php the_title(); ?></a></p>
            <?php
//            if(function_exists('wp_pagenavi')) {
//                wp_pagenavi( array( 'type' => 'multipart' ) );
//            }
//            else{
//                wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) );
//            }
            ?>
        </div>
    </div>
</div><!-- #post-## -->

<?php //if($xyz%((is_front_page() or is_single())?'2':'2')==0) echo '<div class="w-100"></div>'; ?>
<?php if($xyz%(2)==0) echo '<div class="w-100"></div>'; ?>
<?php $xyz++;?>
 <?php //var_dump($xyz++%3);?>
<?php //comments_template( '', true ); ?>