<?php
/**!
 * The Page Content Loop
 */
?>
<?php
global $page_mb;
$page_meta = $page_mb->the_meta();
$page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
$page_title_tag_line = $page_meta['custom_title_tag_line']? $page_meta['custom_title_tag_line']: false;
?>
<h1><?php echo $page_title;?></h1>
<h2><?php echo $page_title_tag_line;?></h2>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
  <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
    <div>
      <?php the_content()?>
      <?php wp_link_pages(); ?>
    </div>
  </article>
<?php
  endwhile;
  else :
    get_template_part('loops/404');
  endif;
?>
