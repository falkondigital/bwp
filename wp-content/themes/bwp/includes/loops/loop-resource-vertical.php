<?php
/**
 *
 */
global $post, $resource_page_id;
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-'.((is_front_page() or is_single())?'4':'4').' post-vertical mb-6'); ?>>
    <div class="inner bg-white pb-6 h-100">
    <?php $block_excerpt = wp_trim_words(get_the_excerpt(), 30, '' ); ?>
        <?php
        $category = get_the_terms( $post->ID, 'resource-type' );
        //					var_dump($category);
        $catname = '';
        $video_icon = '';
        if(is_array($category)){	//get_the_terms will return an array if there are any, otherwise false or wp_error
            $catname =  '<a href="'.get_permalink($resource_page_id).'?resource-type='.$category[0]->slug.'" class="imgcattag" title="' . get_the_title() . '">'.$category[0]->name.'</a>';
        }
        if(strpos($catname,'video')){
            $video_icon = '<i class="far fa-play-circle"></i>';
        }
        ?>
    <?php if(has_post_thumbnail($post->ID)){
        $imgSRC = get_the_post_thumbnail( $post->ID,'thumbnail' );
        ?>
        <div class="resource-feat-img"><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow" class=""><?php echo $imgSRC.$video_icon;?></a></div>
    <?php
    }
    else{
        $args = array(
            'numberposts' => 1,
            'order'=> 'ASC',
            'post_mime_type' => 'image',
            'post_parent' =>  $post->ID,
            'post_status' => 'inherit',
            'post_type' => 'attachment',
            'orderby' => 'menu_order ID'
        );
        $attachments = get_children( $args );
        if ($attachments) {
            foreach($attachments as $attachment) {
                $image_attributes = wp_get_attachment_image_src( $attachment->ID, 'thumbnail' );
                $imgSRC = '<img src="'.$image_attributes[0].'" width="'.$image_attributes[1].'" height="'.$image_attributes[2].'" class="current">';
            }
            ?>
            <div class="resource-feat-img"><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow"><?php echo $imgSRC.$video_icon;?></a></div>
        <?php
        }
        else{
            $imgSRC = '<img src="'.get_template_directory_uri().'/images/default-thumbnail-360x225.jpg" width="360" height="225" class="">';
            ?>
            <div class="resource-feat-img"><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow"><?php echo $imgSRC.$video_icon;?></a></div>
        <?php }
    }
    ?>
        <div class="inner-text p-3 mb-6">
            <span class="h4 blog-title  "><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_title(); ?></a></span>
            <p class="catname text-uppercase text-muted"><?php echo $catname;?></p>
<!--            <div class="entry-meta">-->
<!--                --><?php //flkn_news_posted_on(); ?>
<!--            </div><!-- .entry-meta -->
            <p class="excerpt"><?php echo $block_excerpt; ?></p>
            <?php
//            if(function_exists('wp_pagenavi')) {
//                wp_pagenavi( array( 'type' => 'multipart' ) );
//            }
//            else{
//                wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) );
//            }
            ?>
        </div>
        <div class="read-more-button-wrapper text-center">
            <?php flkn_read_more();?>
        </div>
    </div>
</div><!-- #post-## -->

<?php if($xyz%((is_front_page() or is_single())?'4':'3')==0) echo '<div class="w-100"></div>'; ?>
<?php if($xyz%(3)==0) echo '<div class="w-100"></div>'; ?>
<?php $xyz++;?>
 <?php //var_dump($xyz++%3);?>
<?php //comments_template( '', true ); ?>