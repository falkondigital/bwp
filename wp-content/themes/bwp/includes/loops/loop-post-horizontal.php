<?php
/**
 *
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php $block_excerpt = wp_trim_words(get_the_excerpt(), 55, '' ); ?>
    <?php if(has_post_thumbnail($post->ID)){
        $imgSRC = get_the_post_thumbnail( $post->ID,'thumbnail' );
        ?>
        <div class="newsimg"><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow"><?php echo $imgSRC;?></a></div>
    <?php
    }
    else{
        $args = array(
            'numberposts' => 1,
            'order'=> 'ASC',
            'post_mime_type' => 'image',
            'post_parent' =>  $post->ID,
            'post_status' => 'inherit',
            'post_type' => 'attachment',
            'orderby' => 'menu_order ID'
        );
        $attachments = get_children( $args );
        if ($attachments) {
            foreach($attachments as $attachment) {
                $image_attributes = wp_get_attachment_image_src( $attachment->ID, 'thumbnail' );
                $imgSRC = '<img src="'.$image_attributes[0].'" width="'.$image_attributes[1].'" height="'.$image_attributes[2].'" class="current">';
            }
            ?>
            <div class="newsimg"><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow"><?php echo $imgSRC;?></a></div>
        <?php
        }
        else{
            $imgSRC = '<img src="'.get_template_directory_uri().'/images/default-thumbnail-200x200.jpg" width="178" height="178" class="">';
            ?>
            <div class="newsimg"><a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo esc_attr($post->post_title); ?>" rel="nofollow"><?php echo $imgSRC;?></a></div>
        <?php }
    }
    ?>
    <span class="likeh2 blog-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <?php the_title(); ?></a></span>
    <div class="entry-meta">
        <?php flkn_news_posted_on(); ?>
    </div><!-- .entry-meta -->
    <p class="excerpt"><?php echo $block_excerpt; ?></p>
    <?php
    if(function_exists('wp_pagenavi')) {
        wp_pagenavi( array( 'type' => 'multipart' ) );
    }
    else{
        wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) );
    }
    ?>
    <?php flkn_read_more();?>
</div><!-- #post-## -->

<?php //comments_template( '', true ); ?>