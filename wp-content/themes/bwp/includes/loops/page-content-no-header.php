<?php
/**!
 * The Page Content Loop
 */
?>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
  <?php
  global $post;
  $content = $post->post_content;
  if ( !empty( $content ) or trim($content)!='' ) :
    ?>
    <main class="container mt-5 bg-light mb-8">
      <div class="row">
        <div class="col-sm">
          <div id="content" role="main">
            <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
              <div>
                <?php the_content()?>
                <?php wp_link_pages(); ?>
              </div>
            </article>
          </div><!-- /#content -->
        </div>
      </div><!-- /.row -->
    </main><!-- /.container -->
    <?php
  endif;
  ?>
  <?php
endwhile;
else :
  get_template_part('loops/404');
endif;
?>