<?php
/**!
 * The Page Content Loop
 */
?>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
  <?php
  global $post;
  $content = $post->post_content;
  if ( !empty( $content ) or trim($content)!='' ) :
    ?>
          <div id="content" role="main">
            <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
                <?php the_content()?>
                <?php wp_link_pages(); ?>
            </article>
          </div><!-- /#content -->
    <?php
  endif;
  ?>
  <?php
endwhile;
else :
  get_template_part('loops/404');
endif;
?>