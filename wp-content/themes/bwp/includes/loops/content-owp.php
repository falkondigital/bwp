<?php
/*
The Page Loop
=============
*/
global $case_study_mb, $falkon_option;
$case_study_meta = $case_study_mb->the_meta();
?>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
    <div class="row py-7">
    <div class="col-12 col-sm-8">
    <div id="content">
    <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
        <div class="d-block d-md-none clearfix">
        <?php if($case_study_meta['client_name']) echo '<h2>'.$case_study_meta['client_name'].'</h2>';?>
        <?php echo '<h3>'.get_the_title().'</h3>';?>
        </div>
        <?php the_content()?>
        <?php wp_link_pages(); ?>
    </article>
    </div>
    </div>
    <div class="col-12 col-sm-4">
        <div class="d-none d-md-block clearfix">
        <?php if($case_study_meta['client_name']) echo '<h2>'.$case_study_meta['client_name'].'</h2>';?>
        <?php echo '<h3>'.get_the_title().'</h3>';?>
        </div>
        <?php
        if($case_study_meta['key_facts_value']
        or $case_study_meta['key_facts_duration']
        or $case_study_meta['key_facts_size']){
            echo '<div class="our-work-facts my-6"><p class="h5 font-weight-bold">Key Facts</p>';
            if($case_study_meta['key_facts_value']) echo '<p class="text-lightgray" >Project Value: <span>'.$case_study_meta['key_facts_value'].'</span></p>';
            if($case_study_meta['key_facts_duration']) echo '<p class="text-lightgray" >Project Duration: <span>'.$case_study_meta['key_facts_duration'].'</span></p>';
            if($case_study_meta['key_facts_size']) echo '<p class="text-lightgray" >Size: <span>'.$case_study_meta['key_facts_size'].'</span></p>';
            echo '</div>';
        }
        if($case_study_meta['project_brief']){
            echo '<p class="text-lightgray title-brief">Project Brief:</p>';
            echo wpautop($case_study_meta['project_brief']);
        }
        $category = get_the_terms( $post->ID, 'case-study-type' );
        if(count($category)>0){
            $our_work_page_id = $falkon_option['falkon_case_study_page_id'];

            echo '<p class="mt-6 h5 font-weight-bold">Service Lines Involved</p>';
            foreach($category as $cs_cat){
                echo '<a href="'.get_permalink($our_work_page_id).'?work-type='.$cs_cat->slug.'" class="imgcattag" title="'.esc_attr($cs_cat->name).'">'.$cs_cat->name.'</a><br>';
            }
        }
        $category = get_the_terms( $post->ID, 'case-study-sector' );
        if(count($category)>0){
            $our_work_page_id = $falkon_option['falkon_case_study_page_id'];

            echo '<p class="mt-6 h5 font-weight-bold">Sectors Involved</p>';
            foreach($category as $cs_cat){
                echo '<a href="'.get_permalink($our_work_page_id).'?work-sector='.$cs_cat->slug.'" class="imgcattag" title="'.esc_attr($cs_cat->name).'">'.$cs_cat->name.'</a><br>';
            }
        }

        ?>
    </div>
    </div>
<?php endwhile; ?>
<?php else: get_template_part('includes/loops/content', 'none'); ?>
<?php endif; ?>
