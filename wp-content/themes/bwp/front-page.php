<?php get_header(); ?>
<!-- Banner -->
<?php include(locate_template('includes/blocks/banner-slider.php'));?>
<?php //include(locate_template('includes/blocks/banner-slider-new.php'));?>

<?php /* ?><div id="established-home-wrapper" class="bg-white">
  <div class="container">
    <div class="row py-8 align-items-center">
      <div class="col-12 col-sm-6">
        <p class="h2">Established in <strong>2008</strong> with offices in Manchester and London, we operate throughout the UK.</p>
        <p>Our Director’s started their careers in large multi-disciplinary surveying businesses and each have a direct and personal involvement in all our instructions</p>
      </div>
      <div class="col-12 col-sm-6">
        <img src="<?php echo get_template_directory_uri(); ?>/images/tmp/homepage-buildings.png">
      </div>
    </div>
  </div>
</div>
<div id="sectors-home-wrapper" class="bg-bwp-blue-1 bg-computers text-white">
  <div class="container">
    <div class="row py-8 text-center">
      <div class="col-12 col-sm-10 offset-sm-1">
        <p class="h2 text-white">We are experts in our field and offer technically, commercially well rounded and concise advice</p>
        <p>Our team has significant expertise in both the public and private sectors including; retail, education, licensed trade, health, heritage, transport, residential, and commercial.</p>
        <a href="<?php echo get_permalink(20);?>" class="btn btn-outline-white btn-lg" >View all Sectors</a>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
<div id="clients-home-wrapper" class="bg-white">
  <div class="container">
    <div class="row py-8 align-items-center">
      <div class="col-12 col-sm-6 text-center text-sm-right order-last">
        <p class="h2">We listen to our clients and tailor each instruction to deliver their specific requirements.</p>
        <p>Clients receive the right support to allow them to make effective decisions with regard to their real estate.</p>
        <a href="<?php echo get_permalink(8);?>" class="btn btn-outline-primary btn-lg" >More About Us</a>
      </div>
      <div class="col-12 col-sm-6 order-first">
        <img src="<?php echo get_template_directory_uri(); ?>/images/tmp/homepage-clients.jpg">
      </div>
    </div>
  </div>
</div><?php */ ?>

<?php
if(1==2 and isset($falkon_option['falkon_cta_tagline']) and $falkon_option['falkon_cta_tagline']!=''){
?>
<!-- Block -->
<div id="block-home-wrapper" class="bg-white">
  <div class="container">
    <div class="row py-8 justify-content-center">
      <div class="col-10 text-center">
        <div class="h2"><?php echo wpautop(do_shortcode($falkon_option['falkon_cta_tagline']));?></div>
      </div>
    </div>
  </div>
</div>
  <?php
}
?>
<?php if(1==2 and isset($falkon_option['falkon_content_block_details']) and $falkon_option['falkon_content_block_details']!=''){ ?>
  <div id="cta-first-home-wrapper" class="bg-light">
    <div class="container">
      <div class="row py-8 justify-content-center">
        <?php
        echo do_shortcode($falkon_option['falkon_content_block_details']);
        ?>
      </div>
    </div>
  </div>
<?php } ?>
<!-- Client Logos -->
<?php if(1==2 and isset($falkon_option['falkon_homepage_integrations_vis']) and $falkon_option['falkon_homepage_integrations_vis']=='1' ): ?>
  <div id="integrations-home-wrapper" class="bg-white">
    <div class="container ">
      <div class="row py-8">
        <div class="col">
          <?php
          $hp_brands_title = $falkon_option['falkon_homepage_integrations_title']? $falkon_option['falkon_homepage_integrations_title'] : 'Featured Integrations';
          ?>
          <p class="h2 text-center font-weight-light"><?php echo $hp_brands_title;?></p>
          <?php
          $block_location = 'carousel-integrations';
          $carousel_block = falkon_get_transient($block_location);//get_transient( 'block-' . $block_location );
          echo $carousel_block;
          ?>
        </div>
      </div>
    </div>
  </div>
<?php endif;?>
<?php if(1==2 and isset($falkon_option['falkon_content_block_blue']) and $falkon_option['falkon_content_block_blue']!=''){ ?>
<div id="cta-first-home-wrapper" class="bg-blue text-white">
  <div class="container">
    <div class="row py-8">
      <?php
      echo do_shortcode($falkon_option['falkon_content_block_blue']);
      ?>
    </div>
  </div>
</div>
<?php } ?>
<!-- Client Logos -->
<?php if(1==2 and isset($falkon_option['falkon_homepage_client_vis']) and $falkon_option['falkon_homepage_client_vis']=='1' ): ?>
<div id="clients-home-wrapper" class="bg-white">
  <div class="container ">
    <div class="row py-8">
      <div class="col">
        <?php
        $hp_brands_title = $falkon_option['falkon_homepage_client_title']? $falkon_option['falkon_homepage_client_title'] : 'Who we work with';
        ?>
        <p class="h2 text-center font-weight-light"><?php echo $hp_brands_title;?></p>
        <?php
        $block_location = 'carousel-clients';
        $carousel_block = falkon_get_transient($block_location);//get_transient( 'block-' . $block_location );
        echo $carousel_block;
        ?>
      </div>
    </div>
  </div>
</div>
<?php endif;?>
<?php if(1==2 and isset($falkon_option['falkon_homepage_testimonial_vis']) and $falkon_option['falkon_homepage_testimonial_vis']=='1' ): ?>
<!-- testimonials -->
<div id="testimonials-home-wrapper" class="bg-light">
  <div class="container">
    <div class="row py-8">
      <div class="col">
        <?php
        $hp_testimonial_title = $falkon_option['falkon_homepage_testimonial_title']? $falkon_option['falkon_homepage_testimonial_title'] : 'What our clients say';
        ?>
        <p class="h2 text-center font-weight-light"><?php echo $hp_testimonial_title;?></p>
        <?php
        $block_location = 'carousel-testimonials';
        $carousel_testimonial_block = falkon_get_transient($block_location);//get_transient( 'block-' . $block_location );
        echo $carousel_testimonial_block;
        ?>
      </div>
    </div>
  </div>
</div>
<?php endif;?>
<?php get_template_part('includes/loops/page-content-no-header-no-container'); ?>
<?php include(locate_template('includes/blocks/final-cta.php'));?>
<!-- Blog -->
<div id="news-home-wrapper" class="bg-light">
  <div class="container">
    <div class="row py-8">
      <div class="col-sm-3">
        <?php
        $hp_news_title = $falkon_option['falkon_homepage_news_title']? $falkon_option['falkon_homepage_news_title'] : 'Latest from the blog';
        $hp_news_subtitle = $falkon_option['falkon_homepage_news_subtitle']? $falkon_option['falkon_homepage_news_subtitle'] : '';
        $view_all_news_link = get_permalink(get_option('page_for_posts'));
        ?>
        <p class="h2 text-center text-sm-left font-weight-light"><?php echo $hp_news_title;?></p>
        <?php if($hp_news_subtitle!='') echo '<p class="text-center text-sm-left">'.$hp_news_subtitle.'</p>' ?>
      </div>
      <div class="col-sm-1 hidden-xs text-center">
        <div class="vl"></div>
      </div>
      <div class="col-sm-8">

          <div class="blog-list">
            <?php
            $block_location = 'news-homepage';
            $news_block = falkon_get_transient($block_location);//get_transient( 'block-' . $block_location );
            echo $news_block;
            ?>
          </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
