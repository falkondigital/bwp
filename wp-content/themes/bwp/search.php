<?php
  global $falkon_option;
  get_header();?>
<?php
global $page_mb;
$blog_page_id = get_option('page_for_posts');
$page_meta = $page_mb->the_meta($blog_page_id);
$page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title($blog_page_id);
$page_title_tag_line = $page_meta['custom_title_tag_line']? $page_meta['custom_title_tag_line']: false;
?>
  <div class="jumbotron jumbotron-fluid text-white" style="background-image: url('<?php echo get_template_directory_uri() . '/images/banner-blog.jpg';?>');">
    <div class="shadow"></div>
    <div class="container text-center">
      <p class="h1 text-white"><?php _e('Search Results for', 'b4st'); ?> &ldquo;<?php the_search_query(); ?>&rdquo;</p>
      <?php
      if($page_title_tag_line!==false){
        echo '<p class="h3 text-white font-weight-normal">'.$page_title_tag_line.'</p>';
      }
      ?>
    </div>
  </div>
<?php include(locate_template('includes/blocks/blog-topnavbar.php')); ?>
  <div id="bwp-blog" class="bg-light pb-7 pt-8">
  <div class="container">
  <div class="blog-list">
  <div class="row blog-list-vertical">
<?php $xyz = 1;?>
<?php if(have_posts()): while(have_posts()): the_post();?>
  <?php //get_template_part('includes/loops/content', get_post_format()); ?>
  <?php include(locate_template('includes/loops/loop-post-vertical.php')); ?>
<?php endwhile; ?>
  </div>
  </div>
  </div><!-- /.container -->
  </div>
  <?php include(locate_template('includes/blocks/pagination-blog-bottom.php')); ?>
<?php else: wp_redirect(get_bloginfo('siteurl').'/404', 404); exit; endif; ?>
<?php get_footer();?>