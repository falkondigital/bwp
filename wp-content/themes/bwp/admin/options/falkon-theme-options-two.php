<?php
/**
 * Define page tabs
 * $tabs['tab-slug'] 	= __('Tab Name', 'wpShop');
 */
function falkon_options_two_page_tabs() {

	$tabs = array();
    $tabs['homepage'] 		= __('Homepage', 'falkon_textdomain');
    $tabs['general'] 	= __('General Settings', 'falkon_textdomain');
    $tabs['page-ids'] 		= __('Page IDs', 'falkon_textdomain');
    $tabs['social-media'] 		= __('Social Media', 'falkon_textdomain');
    $tabs['contacts'] 		= __('Contact Settings', 'falkon_textdomain');
    $tabs['smtp_email'] 		= __('Email Settings', 'falkon_textdomain');
//    $tabs['case_studies'] 		= __('Clients', 'falkon_textdomain');
    $tabs['schema-seo'] 		= __('SEO/Schema Markup', 'falkon_textdomain');

//	$tabs['our-work'] 		= __('Our Work', 'falkon_textdomain');
//    $tabs['select'] 		= __('Select', 'wpShop');
//	$tabs['checkboxes'] 	= __('Checkboxes', 'wpShop');

	return $tabs;
}


/**
 * Define our settings sections
 *
 * array key=$id, array value=$title in: add_settings_section( $id, $title, $callback, $page );
 * @return array
 */
function falkon_options_two_page_sections() {

	// get the current tab
	$tab = falkon_get_the_tab();

	// sections according to tab
	switch ($tab) {
        case 'schema-seo':
            $sections = array();
            $sections['schema_section']    =   __('Schema Markeup Details', 'falkon_textdomain');

            break;
		// Text Inputs
		case 'general':
			$sections = array();
			//$sections['hp_section']				= __('Homepage Settings', 'falkon_textdomain');
//			$sections['hp_section']			= __('', 'falkon_textdomain');
//            $sections['office_section']			= __('', 'falkon_textdomain');

//                $sections['header_content']				= __('Page header settings', 'falkon_textdomain');
//              $sections['footer_content']				= __('Page footer settings', 'falkon_textdomain');

//            $sections['blog_controls']				= __('Blog Options', 'falkon_textdomain');
//            $sections['company_section']				= __('Company Info Section', 'falkon_textdomain');
//            $sections['finance_section']				= __('Finance/Invoicing Section', 'falkon_textdomain');
//            $sections['general_section']			= __('Other Settings', 'falkon_textdomain');
            $sections['header_section']			= __('Header Settings', 'falkon_textdomain');
            $sections['footer_section']			= __('Footer Settings', 'falkon_textdomain');
            $sections['finalcta_section']			= __('Final Call To Action', 'falkon_textdomain');

            $sections['company_section']				= __('Company Info Section', 'falkon_textdomain');
//            $sections['finance_section']				= __('Finance/Invoicing Section', 'falkon_textdomain');
            $sections['general_section']			= __('Other Settings', 'falkon_textdomain');
		break;
        case 'homepage':
            $sections = array();
//            $sections['featured_woo_cat_section']				= __('Home Featured Categories', 'falkon_textdomain');
            $sections['hp_section']				= __('Homepage Settings', 'falkon_textdomain');
//            $sections['event_section']				= __('Event Section', 'falkon_textdomain');
//            $sections['feat_art_section']				= __('Featured Artiste Section', 'falkon_textdomain');
//            $sections['showreel_section']				= __('Showreel Section', 'falkon_textdomain');
//            $sections['integration_section']				= __('Integrations Section', 'falkon_textdomain');

//            $sections['client_section']				= __('Client Section', 'falkon_textdomain');
            //$sections['footer_section']			= __('Footer Settings', 'falkon_textdomain');
            $sections['general_section']			= __('General Settings', 'falkon_textdomain');
        break;
        case 'page-ids':
            $sections = array();
//            $sections['dealer_signup_ids_section'] 	= __('Dealer Signup Page ID Links', 'falkon_textdomain');
//            $sections['portal_ids_section'] 	= __('Portal Page ID Links', 'falkon_textdomain');
//            $sections['loginregister_ids_section'] 	= __('Login/Logout/Register etc', 'falkon_textdomain');
            $sections['general_ids_section'] 	= __('General Page ID Links', 'falkon_textdomain');
//            $sections['dashboard_ids_section'] 	= __('Dashboard Page ID Links', 'falkon_textdomain');
//            $sections['dashboard_teacher_ids_section'] 	= __('Dashboard teacher Page ID Links', 'falkon_textdomain');
//            $sections['dashboard_student_ids_section'] 	= __('Dashboard Student Page ID Links', 'falkon_textdomain');
//            $sections['dashboard_school_ids_section'] 	= __('Dashboard School Page ID Links', 'falkon_textdomain');
//            $sections['dashboard_group_ids_section'] 	= __('Dashboard Group Page ID Links', 'falkon_textdomain');
//            $sections['dashboard_topic_ids_section'] 	= __('Dashboard Topics ID Links', 'falkon_textdomain');
//            $sections['results_ids_section'] 	= __('Results Page ID Links', 'falkon_textdomain');
            break;
//        case 'case_studies':
//            $sections = array();
////            $sections['dealer_signup_ids_section'] 	= __('Dealer Signup Page ID Links', 'falkon_textdomain');
////            $sections['portal_ids_section'] 	= __('Portal Page ID Links', 'falkon_textdomain');
////            $sections['loginregister_ids_section'] 	= __('Login/Logout/Register etc', 'falkon_textdomain');
////            $sections['general_ids_section'] 	= __('General Page ID Links', 'falkon_textdomain');
////            $sections['dashboard_ids_section'] 	= __('Dashboard Page ID Links', 'falkon_textdomain');
//            $sections['client_images'] 	= __('Client Logos', 'falkon_textdomain');
////            $sections['dashboard_student_ids_section'] 	= __('Dashboard Student Page ID Links', 'falkon_textdomain');
////            $sections['dashboard_school_ids_section'] 	= __('Dashboard School Page ID Links', 'falkon_textdomain');
////            $sections['dashboard_group_ids_section'] 	= __('Dashboard Group Page ID Links', 'falkon_textdomain');
////            $sections['dashboard_topic_ids_section'] 	= __('Dashboard Topics ID Links', 'falkon_textdomain');
////            $sections['casestudiesparent'] 	= __('Case Study Page Settings', 'falkon_textdomain');
////            $sections['casestudies'] 	= __('Case Study Single Page Settings', 'falkon_textdomain');
//
//            break;
		case 'our-work':
			$sections = array();
			$sections['our_work'] 	= __('Our Work', 'falkon_textdomain');
			break;
        case 'social-media':
			$sections = array();
//            $sections['social_header_section'] 	= __('Social Media Header Links', 'falkon_textdomain');
            $sections['social_footer_section'] 	= __('Social Media Footer Links', 'falkon_textdomain');
		break;

		case 'contacts':
			$sections = array();
			$sections['contact_section']    =   __('Site Contact details', 'falkon_textdomain');


//            $sections['social_header_section'] 	= __('Social Media Header Links', 'falkon_textdomain');
//            $sections['social_footer_section'] 	= __('Social Media Footer Links', 'falkon_textdomain');
//            $sections['pre_footer_section']    =   __('Pre Footer details', 'falkon_textdomain');
//            $sections['footer_section']    =   __('Pre Footer details', 'falkon_textdomain');
		break;
        case 'smtp_email':
            $sections = array();
            $sections['email_section'] 	    =   __('Email Sending Options', 'falkon_textdomain');
            $sections['smtp_email_section'] 	    =   __('SMTP Email Sending Options', 'falkon_textdomain');
            break;


	}

	return $sections;
}

/**
 * Define our form fields (options)
 *
 * @return array
 */
function falkon_options_two_page_fields() {

	// get the current tab
	$tab = falkon_get_the_tab();

    $pagepost_list = get_pages();
    $page_array = array();
    array_push($page_array,'Please Select|');
    foreach ( $pagepost_list as $page ) {
        $page_ancestors = get_post_ancestors( $page->ID );
        $level_deep = count($page_ancestors);
        $delim = str_repeat("— ", ($level_deep));
        if($page->post_parent!=0) $page->post_title = $delim.' '.$page->post_title;
        array_push($page_array,$page->post_title.'|'.$page->ID);
    }



    $args = array(
        'taxonomy'  =>  'product_cat',
        'orderby' => 'menu_id title',
        'order'      => 'ASC',
        'hide_empty' => 0,
        'parent'        => 0,
    );
    $category_list = get_terms( $args );

    //var_dump($category_list);

    //below here
    $cat_array = array();
    array_push($cat_array,'Please Select|');
    foreach ( $category_list as $catt ) {
        $cat_ancestors = get_ancestors( $catt->term_id );
        $level_deep = count($cat_ancestors);
        $delim = str_repeat("— ", ($level_deep));
        if($catt->parent!=0) $catt->name = $delim.' '.$catt->name;
        array_push($cat_array,$catt->name.'|'.$catt->term_id);
    }







    // setting fields according to tab
	switch ($tab) {
        case 'schema-seo':
            $options[] = array(
                "section" => "schema_section",
                "id"      => FALKON_SHORTNAME . "_schema_location_streetaddress1",
                "title"   => __( 'Schema Business Address (streetAddress 1)', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "schema_section",
                "id"      => FALKON_SHORTNAME . "_schema_location_streetaddress2",
                "title"   => __( 'Schema Business Address (streetAddress 2)', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "schema_section",
                "id"      => FALKON_SHORTNAME . "_schema_location_addressLocality",
                "title"   => __( 'Schema Business Address (addressLocality)', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "schema_section",
                "id"      => FALKON_SHORTNAME . "_schema_location_addressRegion",
                "title"   => __( 'Schema Business Address (addressRegion)', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "schema_section",
                "id"      => FALKON_SHORTNAME . "_schema_location_postalCode",
                "title"   => __( 'Schema Business Address (postalCode)', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "schema_section",
                "id"      => FALKON_SHORTNAME . "_schema_location_country",
                "title"   => __( 'Schema Business Address (Country)', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "schema_section",
                "id"      => FALKON_SHORTNAME . "_schema_location_geo",
                "title"   => __( 'Schema Business geo co-ords', 'falkon_textdomain' ),
                "desc"    => __( 'give in full coordinates lat,long e.g. 53.2621564,-2.5674118', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "schema_section",
                "id"      => FALKON_SHORTNAME . "_schema_opening_hours",
                "title"   => __( 'Company Opening Hours:', 'falkon_textdomain' ),
                "desc"    => __( 'Enter your opening hours in schema format, Days are specified using the following two-letter combinations: Mo, Tu, We, Th, Fr, Sa, Su.
Times are specified using 24:00 time. For example, 3pm is specified as 15:00. Example - "Tu,Th 16:00-20:00". Don\'t forget to fill in the <a href="'.admin_url('themes.php?page=falkon-settings-page-one&tab=contacts').'">Contact</a> option too!.','falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "allowlinebreaks"
            );


            break;
        case 'homepage':
            $options[] = array(
                "section" => "featured_woo_cat_section",
                "id"      => FALKON_SHORTNAME . "_top_box_1",
                "title"   => __( 'Top Category Box', 'wptuts_textdomain' ),
                "desc"    => __( '', 'wptuts_textdomain' ),
                "type"    => "select2",	//variation of the std multi-checkbox, with a limit of allowed ticks
                "std"     => "",	//the limit of allowed ticks
                "choices" => $cat_array
            );
            $options[] = array(
                "section" => "featured_woo_cat_section",
                "id"      => FALKON_SHORTNAME . "_top_box_2",
                "title"   => __( 'Bottom Category Box', 'wptuts_textdomain' ),
                "desc"    => __( '', 'wptuts_textdomain' ),
                "type"    => "select2",	//variation of the std multi-checkbox, with a limit of allowed ticks
                "std"     => "",	//the limit of allowed ticks
                "choices" => $cat_array
            );
            $options[] = array(
                "section" => "featured_woo_cat_section",
                "id"      => FALKON_SHORTNAME . "_pop_categories",
                "title"   => __( 'List Popular Categories', 'wptuts_textdomain' ),
                "desc"    => __( 'Select 8 categories to display as popular on the front page. +Ctrl will allow you to select multiple or de-select.', 'wptuts_textdomain' ),
                "type"    => "multiselect2-limit",	//variation of the std multi-checkbox, with a limit of allowed ticks
                "std"     => 8,	//the limit of allowed ticks
                "choices" => $cat_array
            );
            $options[] = array(
                "section" => "dealer_signup_ids_section",
                "id"      => FALKON_SHORTNAME . "_dealer_signup_stage1_page_id",
                "title"   => __( 'Stage 1', 'falkon_textdomain' ),
                "desc"    => __( 'Contact page of site, used in the theme in the header \'View Our UK Locations\'. ', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
            );
            $options[] = array(
                "section" => "hp_section",
                "id"      => FALKON_SHORTNAME . "_homepage_news_title",
                "title"   => __( 'Homepage News Title', 'falkondigital' ),
                "desc"    => __( 'Title for homepage news articles, default is "Latest news from the blog".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Latest news from the blog','falkondigital'),
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "hp_section",
                "id"      => FALKON_SHORTNAME . "_homepage_news_subtitle",
                "title"   => __( 'Homepage News Subtitle', 'falkondigital' ),
                "desc"    => __( 'Subtitle for homepage news articles, default is "".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('','falkondigital'),
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "hp_section",
                "id"      => FALKON_SHORTNAME . "_homepage_news_num",
                "title"   => __( 'Homepage News Number', 'falkondigital' ),
                "desc"    => __( 'Number of latest news articles to show on homepage', 'falkon_textdomain' ),
                "type"    => "select",
                "std"    => "2",
                "choices" => array( "2", "4", "6")
            );
            $options[] = array(
                "section" => "event_section",
                "id"      => FALKON_SHORTNAME . "_homepage_event_vis",
                "title"   => __( 'Artiste Event Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "event_section",
                "id"      => FALKON_SHORTNAME . "_homepage_event_title",
                "title"   => __( 'Homepage Event Title', 'falkondigital' ),
                "desc"    => __( 'Title for homepage event block, default is "BROWSE BY EVENT TYPE".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Browse By Event Type','falkondigital'),
                "class"   => "nohtml"
            );

            $taxonomy     = 'stock_event';
            $orderby      = 'slug';
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no
            $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'order'         =>'ASC',
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'hide_empty' => 0,
                'parent' => 0
            );
            $yourtaxonomies = get_categories($args);
            $tax_data = get_taxonomy( $taxonomy );
            $tax_data = $tax_data->labels;
            $tax_array = array();
            array_push($tax_array,'Please Select|');
            if(is_object($tax_data)){
                foreach($yourtaxonomies as $category) {
                    $categories_sub = get_categories( array(
                            'parent'	=>	$category->term_id,
                            'taxonomy'     => $taxonomy,
                            'hide_empty'	=>	false,
                            'hierarchical'	=>	false,
                            'pad_counts'	=>	true,
                        )
                    );
                    array_push($tax_array,$category->name.'|'.$category->term_id);
                    if(!empty($categories_sub)){
                        foreach($categories_sub as $category_sub) {
                            $categories_sub_sub = get_categories( array(
                                    'parent'	=>	$category_sub->term_id,
                                    'taxonomy'     => $taxonomy,
                                    'orderby'      => $orderby,
                                    'hide_empty'	=>	false,
                                    'hierarchical'	=>	false,
                                    'pad_counts'	=>	true,
                                )
                            );
                            array_push($tax_array,'&mdash;&nbsp;&nbsp;'.$category_sub->name.'|'.$category_sub->term_id);
                            if(!empty($categories_sub_sub)){
                                foreach($categories_sub_sub as $category_sub_sub) {
                                    array_push($tax_array,'&nbsp;&nbsp;&nbsp;&nbsp;&ndash;&nbsp;&nbsp;'.$category_sub_sub->name.'|'.$category_sub_sub->term_id);
                                }
                            }
                        }
                    }
                }
            }

            $options[] = array(
                "section" => "event_section",
                "id"      => FALKON_SHORTNAME . "_homepage_event_blocks",
                "title"   => __( 'Event Blocks', 'falkon_textdomain' ),
                "type"    => "select3",
                "std"     => 6,
                "class"     =>  3,
                "choices" => $tax_array,
                "desc"      =>  __( 'The 6 event blocks on the homepage. Select what goes where.', 'falkondigital' )
            );

            //Feat Artiste Cats
            $options[] = array(
                "section" => "feat_art_section",
                "id"      => FALKON_SHORTNAME . "_homepage_feat_art_vis",
                "title"   => __( 'Featured Artiste Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "feat_art_section",
                "id"      => FALKON_SHORTNAME . "_homepage_feat_art_title",
                "title"   => __( 'Homepage Featured Artiste Title', 'falkondigital' ),
                "desc"    => __( 'Title for homepage featured stock block, default is "Featured Artiste".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Featured Artiste','falkondigital'),
                "class"   => "nohtml"
            );

            $taxonomy     = 'stock_genre';
            $orderby      = 'slug';
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no
            $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'order'         =>'ASC',
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'hide_empty' => 0,
                'parent' => 0
            );
            $yourtaxonomies = get_categories($args);
            $tax_data = get_taxonomy( $taxonomy );
            $tax_data = $tax_data->labels;
            $tax_array = array();
            array_push($tax_array,'Please Select|');
            if(is_object($tax_data)){
                foreach($yourtaxonomies as $category) {
                    $categories_sub = get_categories( array(
                            'parent'	=>	$category->term_id,
                            'taxonomy'     => $taxonomy,
                            'hide_empty'	=>	false,
                            'hierarchical'	=>	false,
                            'pad_counts'	=>	true,
                        )
                    );
                    array_push($tax_array,$category->name.'|'.$category->term_id);
                    if(!empty($categories_sub)){
                        foreach($categories_sub as $category_sub) {
                            $categories_sub_sub = get_categories( array(
                                    'parent'	=>	$category_sub->term_id,
                                    'taxonomy'     => $taxonomy,
                                    'orderby'      => $orderby,
                                    'hide_empty'	=>	false,
                                    'hierarchical'	=>	false,
                                    'pad_counts'	=>	true,
                                )
                            );
                            array_push($tax_array,'&mdash;&nbsp;&nbsp;'.$category_sub->name.'|'.$category_sub->term_id);
                            if(!empty($categories_sub_sub)){
                                foreach($categories_sub_sub as $category_sub_sub) {
                                    array_push($tax_array,'&nbsp;&nbsp;&nbsp;&nbsp;&ndash;&nbsp;&nbsp;'.$category_sub_sub->name.'|'.$category_sub_sub->term_id);
                                }
                            }
                        }
                    }
                }
            }

            $options[] = array(
                "section" => "feat_art_section",
                "id"      => FALKON_SHORTNAME . "_homepage_feat_art_blocks",
                "title"   => __( 'Event Blocks', 'falkon_textdomain' ),
                "type"    => "select3",
                "std"     => 6,
                "class"     =>  3,
                "choices" => $tax_array,
                "desc"      =>  __( 'The 6 featured stock blocks on the homepage. Select what goes where.', 'falkondigital' )
            );

            //Showreel
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_vis",
                "title"   => __( 'Showreel Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_title",
                "title"   => __( 'Homepage Showreel Title', 'falkondigital' ),
                "desc"    => __( 'Title for homepage showreel section, default is "OUR SHOWREEL".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('OUR SHOWREEL','falkondigital'),
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_title_sub",
                "title"   => __( 'Homepage Showreel Title', 'falkondigital' ),
                "desc"    => __( 'Sub Title for homepage showreel section, default is "Mike Constantia stock showreel".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Mike Constantia stock showreel','falkondigital'),
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_text",
                "title"   => __( 'Homepage Showreel Text', 'falkondigital' ),
                "desc"    => __( 'Text for homepage showreel section.', 'falkondigital' ),
                "type"    => "textarea",
                "std"     => __('This video includes clips from some of our most popular stocks including Flameoz, Adam Kitch, Allesandria, Urban Soul Orchestra and Wild BOYS to name just a few.','falkondigital'),
                "class"   => "nohtml widefat"
            );
            //Integrations Area
            $options[] = array(
                "section" => "integration_section",
                "id"      => FALKON_SHORTNAME . "_homepage_integrations_vis",
                "title"   => __( 'Integrations Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this Integrations section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "integration_section",
                "id"      => FALKON_SHORTNAME . "_homepage_integrations_title",
                "title"   => __( 'Integrations Title', 'falkondigital' ),
                "desc"    => __( 'Title for Integrations block, default is "Featured Integrations".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Featured Integrations','falkondigital'),
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "integration_section",
                "id"      => FALKON_SHORTNAME . "_homepage_integrations_images",
                "title"   => __( 'Integrations Images', 'falkondigital' ),
                "desc"    => __( '', 'falkondigital' ),
                "type"    => "image",
                "class"   => "",
                "maximages"  =>  20,
                "buttontxt"    =>  "Upload Logos",
            );
            //Feat Client Area
            $options[] = array(
                "section" => "client_section",
                "id"      => FALKON_SHORTNAME . "_homepage_client_vis",
                "title"   => __( 'Featured Client Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "client_section",
                "id"      => FALKON_SHORTNAME . "_homepage_client_title",
                "title"   => __( 'Featured Client Title', 'falkondigital' ),
                "desc"    => __( 'Title for Featured Client block, default is "Featured Clients".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Featured Clients','falkondigital'),
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "client_section",
                "id"      => FALKON_SHORTNAME . "_homepage_client_images",
                "title"   => __( 'Featured Client Images', 'falkondigital' ),
                "desc"    => __( 'Title for homepage event block, default is "BROWSE BY EVENT TYPE".', 'falkondigital' ),
                "type"    => "image",
                "class"   => "",
                "maximages"  =>  20,
                "buttontxt"    =>  "Upload Images",
            );
            //GENERAL
            $options[] = array(
                "section" => "XXXgeneral_section",
                "id"      => FALKON_SHORTNAME . "_cta_tagline",
                "title"   => __( 'CTA Tagline', 'falkondigital' ),
                "desc"    => __( 'CTA tagline text in top box below slider. Leave blank to hide this.', 'falkondigital' ),
                "type"    => "wp_editor",
                "std"     => __('','falkondigital'),
                "class"   => "widefat"
            );

            $options[] = array(
                "section" => "XXXgeneral_section",
                "id"      => FALKON_SHORTNAME . "_content_block_details",
                "title"   => __( 'Product Details', 'falkondigital' ),
                "desc"    => __( 'Content block for product details. Leave blank to hide this.', 'falkondigital' ),
                "type"    => "wp_editor",
                "std"     => __('','falkondigital'),
                "class"   => "images"
            );

            $options[] = array(
                "section" => "XXXgeneral_section",
                "id"      => FALKON_SHORTNAME . "_content_block_blue",
                "title"   => __( 'Blue Content Block', 'falkondigital' ),
                "desc"    => __( 'Content block for blue section. Leave blank to hide this.', 'falkondigital' ),
                "type"    => "wp_editor",
                "std"     => __('','falkondigital'),
                "class"   => "images"
            );

            $options[] = array(
                "section" => "XXXgeneral_section",
                "id"      => FALKON_SHORTNAME . "_homepage_testimonial_vis",
                "title"   => __( 'Show Testimonial Quote', 'falkondigital' ),
                "desc"    => __( 'Show or hide the quote section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "XXXgeneral_section",
                "id"      => FALKON_SHORTNAME . "_homepage_testimonial_title",
                "title"   => __( 'Featured Client Title', 'falkondigital' ),
                "desc"    => __( 'Title for Testimonials block, default is "What our clients say".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('What our clients say','falkondigital'),
                "class"   => "nohtml"
            );
            break;
        case 'general':
            $options[] = array(
                "section" => "general_section",
                "id"      => FALKON_SHORTNAME . "_case_study_page_id",
                "title"   => __( 'Case Study Main Parent Page', 'falkon_textdomain' ),
                "desc"    => __( 'Parent archive page of "Case Study". Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
//            $options[] = array(
//                "section" => "general_section",
//                "id"      => FALKON_SHORTNAME . "_google_analytics",
//                "title"   => __( 'Google Analytics ID', 'falkon_textdomain' ),
//                "desc"    => __( 'Property/Website ID for Google Analytics', 'falkon_textdomain' ),
//                "type"    => "text",
//                "class"   => "nohtml"
//            );
            $options[] = array(
                "section" => "Xgeneral_section",
                "id"      => FALKON_SHORTNAME . "_results_split",
                "title"   => __( 'Result Split', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "slider",
                "class"   => "",
                "choices"   =>  array(40,70)
            );
            $options[] = array(
                "section" => "Xgeneral_section",
                "id"      => FALKON_SHORTNAME . "_trial_weeks",
                "title"   => __( 'Trial Period', 'falkon_textdomain' ),
                "desc"    => __( 'New registration trial period - value in weeks.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "2",
                "choices" => array( "1 week|1","2 weeks|2","3 weeks|3","4 weeks|4")
            );
            //Company Info Section
//            $options[] = array(
//                "section" => "company_section",
//                "id"      => FALKON_SHORTNAME . "_company_name",
//                "title"   => 'Company Name',
//                "desc"    => 'Company name used for invoicing etc. If nothing is specified, the \'Site Title\' will be used from <a href="'.admin_url('options-general.php').'">General Settings</a>.',
//                "type"    => "text",
//                "std"     => '',
//                "class"   => "nohtml"
//            );
//            $options[] = array(
//                "section" => "company_section",
//                "id"      => FALKON_SHORTNAME . "_company_reg_name",
//                "title"   => 'Company Registration Name',
//                "desc"    => 'Company registration name used for invoicing etc. If nothing is specified, the \'Site Title\' will be used from <a href="'.admin_url('options-general.php').'">General Settings</a>.',
//                "type"    => "text",
//                "std"     => '',
//                "class"   => "nohtml"
//            );
            $options[] = array(
                "section" => "company_section",
                "id"      => FALKON_SHORTNAME . "_company_reg_num",
                "title"   => __( 'Company Number', 'falkon_textdomain' ),
                "desc"    => __( 'Company reg number which goes in the footer of the site. Can also be used throughout the site with shortcode "<strong>[company-reg-num]</strong>"', 'falkon_textdomain' ),
                "type"    => "text",
                "class"   => "nohtml"
            );

            //Finance
            $options[] = array(
                "section" => "finance_section",
                "id"      => FALKON_SHORTNAME . "_subscription_net",
                "title"   => __( 'Subscription Cost Net', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "text",
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "finance_section",
                "id"      => FALKON_SHORTNAME . "_tax_percent",
                "title"   => __( 'VAT Amount', 'falkon_textdomain' ),
                "desc"    => __( 'Tax amount to apply to net cost as a percentage (e,g, 20%).', 'falkon_textdomain' ),
                "type"    => "text",
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "finance_section",
                "id"      => FALKON_SHORTNAME . "_company_vat_num",
                "title"   => __( 'VAT Number', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "text",
                "class"   => "nohtml"
            );


            //Feat Client Area






//            $options[] = array(
//                "section" => "footer_section",
//                "id"      => FALKON_SHORTNAME . "_contact_visible",
//                "title"   => __( 'Contact details visible?', 'falkon_textdomain' ),
//                "desc"    => __( 'Make the contact block of text visible in the footer? To edit these details, click on the <a href="'.admin_url('themes.php?page=falkon-settings-page-one&tab=contacts').'">Contact Settings</a> tab above.', 'falkon_textdomain' ),
//                "type"    => "select2",
//                "std"    => "1",
//                "choices" => array( __('Visible','falkon_textdomain') . "|1", __('Not Visible','falkon_textdomain') . "|0")
//            );

            /*$options[] = array(
                "section" => "footer_section",
                "id"      => FALKON_SHORTNAME . "_copy_visible",
                "title"   => __( 'Copyright text visible?', 'falkon_textdomain' ),
                "desc"    => __( 'Make the copyright block of text visible in the footer?', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "1",
                "choices" => array( __('Visible','falkon_textdomain') . "|1", __('Not Visible','falkon_textdomain') . "|0")
            );*/
            $options[] = array(
                "section" => "header_section",
                "id"      => FALKON_SHORTNAME . "_theme_colour",
                "title"   => 'Theme/Company Colour',
                "desc"    => 'HEX colour with # symbol of the default theme/company colour.',
                "type"    => "colorpicker",
                "std"     => '',
                "class"   => ""
            );
            $options[] = array(
                "section" => "header_section",
                "id"      => FALKON_SHORTNAME . "_hook_before_wp_head",
                "title"   => 'HOOK: Before wp_head() call',
                "desc"    => 'Add code in the header tag of the website.',
                "type"    => "textarea-all",
                "std"     => '',
                "class"   => "code"
            );
            $options[] = array(
                "section" => "header_section",
                "id"      => FALKON_SHORTNAME . "_hook_after_body",
                "title"   => 'HOOK: After opening body tag',
                "desc"    => 'Add code or html to just after the first opening body tag.',
                "type"    => "textarea-all",
                "std"     => '',
                "class"   => "code"
            );
//            $options[] = array(
//                "section" => "header_section",
//                "id"      => FALKON_SHORTNAME . "_cta_blocks",
//                "title"   => 'Header CTAs/USPs',
//                "desc"    => 'Call to Action or Unique Selling Point blocks for header.',
//                "type"    => "ctablocks",
//                "std"     => '',
//                "class"   => "",
//                "maxnum"  =>  3,
//            );
            $options[] = array(
                "section" => "footer_section",
                "id"      => FALKON_SHORTNAME . "_footer_blurb",
                "title"   => 'Footer copyright blurb',
                "desc"    => 'Copyright text in the footer for website to the left. Include {{year}} to include the current year in the text.',
                "type"    => "text",
                "std"     => '&copy; {{year}}',
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "footer_section",
                "id"      => FALKON_SHORTNAME . "_footer_regulation_logos",
                "title"   => __( 'Footer Regulation Logo', 'falkondigital' ),
                "desc"    => __( '', 'falkondigital' ),
                "type"    => "image",
                "class"   => "",
                "maximages"  =>  1,
                "buttontxt"    =>  "Upload Logo",
            );
//            $options[] = array(
//                "section" => "footer_section",
//                "id"      => FALKON_SHORTNAME . "_footer_accreditation_logos",
//                "title"   => __( 'Footer Accreditation Logos', 'falkondigital' ),
//                "desc"    => __( '', 'falkondigital' ),
//                "type"    => "image",
//                "class"   => "",
//                "maximages"  =>  3,
//                "buttontxt"    =>  "Upload Images",
//            );
//            $options[] = array(
//                "section" => "footer_section",
//                "id"      => FALKON_SHORTNAME . "_footer_payment_logos",
//                "title"   => __( 'Footer Payment Logos', 'falkondigital' ),
//                "desc"    => __( '', 'falkondigital' ),
//                "type"    => "image",
//                "class"   => "",
//                "maximages"  =>  6,
//                "buttontxt"    =>  "Upload Logos",
//            );
            $options[] = array(
                "section" => "finalcta_section",
                "id"      => FALKON_SHORTNAME . "_finalcta_title",
                "title"   => __( 'CTA Title', 'falkondigital' ),
                "desc"    => 'CTA title text. can include html.',
                "type"    => "text",
                "std"     => '',
                "class"   => "html"
            );
            $options[] = array(
                "section" => "finalcta_section",
                "id"      => FALKON_SHORTNAME . "_finalcta_tagline",
                "title"   => __( 'CTA Tagline', 'falkondigital' ),
                "desc"    => __( 'CTA tagline text after title', 'falkondigital' ),
                "type"    => "wp_editor",
                "std"     => __('','falkondigital'),
                "class"   => "widefat"
            );
            $options[] = array(
                "section" => "finalcta_section",
                "id"      => FALKON_SHORTNAME . "_finalcta_buttons",
                "title"   => 'CTA Buttons',
                "desc"    => 'Two buttons in the CTA, first is left (outline), second it right (solid)',
                "type"    => "buttonblocks",
                "std"     => '',
                "class"   => "",
                "maxnum"  =>  2,
                "placeholder"  =>  "Title|URL",
            );



//            $options[] = array(
//                "section" => "hp_section",
//                "id"      => FALKON_SHORTNAME . "_homepage_arrow_vis",
//                "title"   => __( 'Homepage Work Arrow', 'falkondigital' ),
//                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
//                "type"    => "radio",
//                "std"       =>  "1",
//                'choices' => array(
//                    'Visible|1',
//                    'Not visible|0',
//                )
//            );

            $options[] = array(
                "section" => "header_content",
                "id"      => FALKON_SHORTNAME . "_topbar_button_text",
                "title"   => __( 'Menu button text', 'falkon_textdomain' ),
                "desc"    => __( 'Enter some button text for the top menu button', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "allowlinebreaks"
            );

            $options[] = array(
                "section" => "header_content",
                "id"      => FALKON_SHORTNAME . "_topbar_button_link",
                "title"   => __( 'Menu button link', 'falkon_textdomain' ),
                "desc"    => __( 'Enter a link for the top menu button', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "allowlinebreaks"
            );

//            $options[] = array(
//                "section" => "header_content",
//                "id"      => FALKON_SHORTNAME . "_header_post_textarea",
//                "title"   => __( 'More info header textarea', 'falkondigital' ),
//                "desc"    => __( 'Enter some content the blue bar that appears a', 'falkondigital' ),
//                "type"    => "textarea",
//                "std"     => __('','falkondigital'),
//                "class"   => "default"
//            );



                     $options[] = array(
                "section" => "footer_content",
                "id"      => FALKON_SHORTNAME . "_contact_us_title",
                "title"   => __( 'Contact form textarea', 'falkon_textdomain' ),
                "desc"    => __( 'Enter a title for the contact form', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "allowlinebreaks"
            );
            $options[] = array(
                "section" => "footer_content",
                "id"      => FALKON_SHORTNAME . "_contact_us_textarea",
                "title"   => __( 'Contact form textarea', 'falkondigital' ),
                "desc"    => __( 'Enter come content for the textarea contact form', 'falkondigital' ),
                "type"    => "textarea",
                "std"     => __('','falkondigital'),
                "class"   => "default"
            );
            $options[] = array(
                "section" => "footer_content",
                "id"      => FALKON_SHORTNAME . "_footer_falkon_image",
                "title"   => __( 'Styles and Wood group logo', 'falkondigital' ),
                "desc"    => __( 'Upload the group logo here', 'falkondigital' ),
                "type"    => "image",
                "class"   => "",
                "maximages"  =>  1,
                "buttontxt"    =>  "Upload Images",
            );


            $options[] = array(
                "section" => "blog_controls",
                "id"      => FALKON_SHORTNAME . "_video_cat_number",
                "title"   => __( 'Number of Vlog to show', 'falkon_textdomain' ),
                "desc"    => __( 'Select the number of vlogs to show on the blog page', 'falkon_textdomain' ),
                "type"    => "select",
                "std"    => "",
                "choices" => array( "3", "6", "9","12")
            );

//            $options[] = array(
//                "section" => "blog_controls",
//                "id"      => FALKON_SHORTNAME . "_cat_slugs",
//                "title"   => __( 'Category slugs', 'falkon_textdomain' ),
//                "desc"    => __( 'Select the number of vlogs to show on the blog page', 'falkon_textdomain' ),
//                "type"    => "text",
//                "std"     => "",
//                "class"   => "nohtml"
//            );




            $options[] = array(
                "section" => "event_section",
                "id"      => FALKON_SHORTNAME . "_homepage_event_vis",
                "title"   => __( 'Artiste Event Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "event_section",
                "id"      => FALKON_SHORTNAME . "_homepage_event_title",
                "title"   => __( 'Homepage Event Title', 'falkondigital' ),
                "desc"    => __( 'Title for homepage event block, default is "BROWSE BY EVENT TYPE".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Browse By Event Type','falkondigital'),
                "class"   => "nohtml"
            );

            $taxonomy     = 'stock_event';
            $orderby      = 'slug';
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no
            $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'order'         =>'ASC',
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'hide_empty' => 0,
                'parent' => 0
            );
            $yourtaxonomies = get_categories($args);
            $tax_data = get_taxonomy( $taxonomy );
            $tax_data = $tax_data->labels;
            $tax_array = array();
            array_push($tax_array,'Please Select|');
            if(is_object($tax_data)){
                foreach($yourtaxonomies as $category) {
                    $categories_sub = get_categories( array(
                            'parent'	=>	$category->term_id,
                            'taxonomy'     => $taxonomy,
                            'hide_empty'	=>	false,
                            'hierarchical'	=>	false,
                            'pad_counts'	=>	true,
                        )
                    );
                    array_push($tax_array,$category->name.'|'.$category->term_id);
                    if(!empty($categories_sub)){
                        foreach($categories_sub as $category_sub) {
                            $categories_sub_sub = get_categories( array(
                                    'parent'	=>	$category_sub->term_id,
                                    'taxonomy'     => $taxonomy,
                                    'orderby'      => $orderby,
                                    'hide_empty'	=>	false,
                                    'hierarchical'	=>	false,
                                    'pad_counts'	=>	true,
                                )
                            );
                            array_push($tax_array,'&mdash;&nbsp;&nbsp;'.$category_sub->name.'|'.$category_sub->term_id);
                            if(!empty($categories_sub_sub)){
                                foreach($categories_sub_sub as $category_sub_sub) {
                                    array_push($tax_array,'&nbsp;&nbsp;&nbsp;&nbsp;&ndash;&nbsp;&nbsp;'.$category_sub_sub->name.'|'.$category_sub_sub->term_id);
                                }
                            }
                        }
                    }
                }
            }

            $options[] = array(
                "section" => "event_section",
                "id"      => FALKON_SHORTNAME . "_homepage_event_blocks",
                "title"   => __( 'Event Blocks', 'falkon_textdomain' ),
                "type"    => "select3",
                "std"     => 6,
                "class"     =>  3,
                "choices" => $tax_array,
                "desc"      =>  __( 'The 6 event blocks on the homepage. Select what goes where.', 'falkondigital' )
            );

            //Feat Artiste Cats
            $options[] = array(
                "section" => "feat_art_section",
                "id"      => FALKON_SHORTNAME . "_homepage_feat_art_vis",
                "title"   => __( 'Featured Artiste Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "feat_art_section",
                "id"      => FALKON_SHORTNAME . "_homepage_feat_art_title",
                "title"   => __( 'Homepage Featured Artiste Title', 'falkondigital' ),
                "desc"    => __( 'Title for homepage featured stock block, default is "Featured Artiste".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Featured Artiste','falkondigital'),
                "class"   => "nohtml"
            );

            $taxonomy     = 'stock_genre';
            $orderby      = 'slug';
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no
            $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'order'         =>'ASC',
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'hide_empty' => 0,
                'parent' => 0
            );
            $yourtaxonomies = get_categories($args);
            $tax_data = get_taxonomy( $taxonomy );
            $tax_data = $tax_data->labels;
            $tax_array = array();
            array_push($tax_array,'Please Select|');
            if(is_object($tax_data)){
                foreach($yourtaxonomies as $category) {
                    $categories_sub = get_categories( array(
                            'parent'	=>	$category->term_id,
                            'taxonomy'     => $taxonomy,
                            'hide_empty'	=>	false,
                            'hierarchical'	=>	false,
                            'pad_counts'	=>	true,
                        )
                    );
                    array_push($tax_array,$category->name.'|'.$category->term_id);
                    if(!empty($categories_sub)){
                        foreach($categories_sub as $category_sub) {
                            $categories_sub_sub = get_categories( array(
                                    'parent'	=>	$category_sub->term_id,
                                    'taxonomy'     => $taxonomy,
                                    'orderby'      => $orderby,
                                    'hide_empty'	=>	false,
                                    'hierarchical'	=>	false,
                                    'pad_counts'	=>	true,
                                )
                            );
                            array_push($tax_array,'&mdash;&nbsp;&nbsp;'.$category_sub->name.'|'.$category_sub->term_id);
                            if(!empty($categories_sub_sub)){
                                foreach($categories_sub_sub as $category_sub_sub) {
                                    array_push($tax_array,'&nbsp;&nbsp;&nbsp;&nbsp;&ndash;&nbsp;&nbsp;'.$category_sub_sub->name.'|'.$category_sub_sub->term_id);
                                }
                            }
                        }
                    }
                }
            }

            $options[] = array(
                "section" => "feat_art_section",
                "id"      => FALKON_SHORTNAME . "_homepage_feat_art_blocks",
                "title"   => __( 'Event Blocks', 'falkon_textdomain' ),
                "type"    => "select3",
                "std"     => 6,
                "class"     =>  3,
                "choices" => $tax_array,
                "desc"      =>  __( 'The 6 featured stock blocks on the homepage. Select what goes where.', 'falkondigital' )
            );

            //Showreel
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_vis",
                "title"   => __( 'Showreel Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_title",
                "title"   => __( 'Homepage Showreel Title', 'falkondigital' ),
                "desc"    => __( 'Title for homepage showreel section, default is "OUR SHOWREEL".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('OUR SHOWREEL','falkondigital'),
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_title_sub",
                "title"   => __( 'Homepage Showreel Title', 'falkondigital' ),
                "desc"    => __( 'Sub Title for homepage showreel section, default is "Mike Constantia stock showreel".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Mike Constantia stock showreel','falkondigital'),
                "class"   => "nohtml"
            );
            $options[] = array(
                "section" => "showreel_section",
                "id"      => FALKON_SHORTNAME . "_homepage_showreel_text",
                "title"   => __( 'Homepage Showreel Text', 'falkondigital' ),
                "desc"    => __( 'Text for homepage showreel section.', 'falkondigital' ),
                "type"    => "textarea",
                "std"     => __('This video includes clips from some of our most popular stocks including Flameoz, Adam Kitch, Allesandria, Urban Soul Orchestra and Wild BOYS to name just a few.','falkondigital'),
                "class"   => "nohtml widefat"
            );

            //Feat Client Area
            $options[] = array(
                "section" => "client_section",
                "id"      => FALKON_SHORTNAME . "_homepage_client_vis",
                "title"   => __( 'Featured Client Section', 'falkondigital' ),
                "desc"    => __( 'Show or hide this section on the homepage.', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Visible|1',
                    'Not visible|0',
                )
            );
            $options[] = array(
                "section" => "client_section",
                "id"      => FALKON_SHORTNAME . "_homepage_client_title",
                "title"   => __( 'Featured Client Title', 'falkondigital' ),
                "desc"    => __( 'Title for Featured Client block, default is "Featured Clients".', 'falkondigital' ),
                "type"    => "text",
                "std"     => __('Featured Clients','falkondigital'),
                "class"   => "nohtml"
            );
//            $options[] = array(
//                "section" => "client_section",
//                "id"      => FALKON_SHORTNAME . "_homepage_client_images",
//                "title"   => __( 'Featured Client Images', 'falkondigital' ),
//                "desc"    => __( 'Title for homepage event block, default is "BROWSE BY EVENT TYPE".', 'falkondigital' ),
//                "type"    => "image",
//                "class"   => "",
//                "maximages"  =>  10,
//                "buttontxt"    =>  "Upload Images",
//            );

        break;

        case 'case_studies':
            $options[] = array(
                "section" => "client_images",
                "id"      => FALKON_SHORTNAME . "_global_client_images",
                "title"   => __( 'Featured Client Images', 'falkondigital' ),
                "desc"    => __( 'Title for homepage event block, default is "BROWSE BY EVENT TYPE".', 'falkondigital' ),
                "type"    => "image",
                "class"   => "",
                "maximages"  =>  10,
                "buttontxt"    =>  "Upload Images",
            );

            $options[] = array(
                "section" => "casestudiesparent",
                "id"      => FALKON_SHORTNAME . "_case_banner_image",
                "title"   => __( 'Case Study Banner Image', 'falkondigital' ),
                "desc"    => __( 'Upload your case study page banner image.', 'falkondigital' ),
                "type"    => "image",
                "class"   => "",
                "maximages"  =>  1,
                "buttontxt"    =>  "Upload Image",
            );
            $options[] = array(
                "section" => "casestudiesparent",
                "id"      => FALKON_SHORTNAME . "_case_study_banner_title",
                "title"   => __( 'Case study banner title', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "allowlinebreaks"
            );
            $options[] = array(
                "section" => "casestudiesparent",
                "id"      => FALKON_SHORTNAME . "_case_study_banner_subtitle",
                "title"   => __( 'Case study banner sub-title', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "textarea",
                "std"     => "",
                "class"   => "default"
            );

            $options[] = array(
                "section" => "casestudiesparent",
                "id"      => FALKON_SHORTNAME . "_casestudies_parent_title",
                "title"   => __( 'Case Study Page title', 'falkondigital' ),
                "desc"    => __( '', 'falkondigital' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "nohtml"
            );

            $options[] = array(
                "section" => "casestudiesparent",
                "id"      => FALKON_SHORTNAME . "_casestudies_parent_content",
                "title"   => __( 'Case Study Page Content', 'falkondigital' ),
                "desc"    => __( 'Place the content here to be displayed on the case study main page', 'falkondigital' ),
                "type"    => "textarea",
                "std"     => __('','falkondigital'),
                "class"   => "default"
            );


            $options[] = array(
                "section" => "casestudies",
                "id"      => FALKON_SHORTNAME . "_related_studies_title",
                "title"   => __( 'Related work title', 'falkondigital' ),
                "desc"    => __( '', 'falkondigital' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "nohtml"
            );
              $options[] = array(
                "section" => "casestudies",
                "id"      => FALKON_SHORTNAME . "_related_studies_number",
                "title"   => __( 'Number of related case studies', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select",
                "std"    => "",
                "choices" => array('2','4','6'),
            );
        break;
        case 'page-ids':
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_contact_us_page_id",
                "title"   => __( 'Contact Us Page', 'falkon_textdomain' ),
                "desc"    => __( 'Main Contact Us page. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "XXXgeneral_ids_section",
                "id"      => FALKON_SHORTNAME . "_resource_page_id",
                "title"   => __( 'Resource Main Parent Page', 'falkon_textdomain' ),
                "desc"    => __( 'Parent archive page of "Resources". Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
        case 'page-ids':
            $options[] = array(
                "section" => "XXXgeneral_ids_section",
                "id"      => FALKON_SHORTNAME . "_free_trial_page_id",
                "title"   => __( 'Free Trial Page', 'falkon_textdomain' ),
                "desc"    => __( 'Parent archive page of "Resources". Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
        case 'page-ids':
            $options[] = array(
                "section" => "XXXgeneral_ids_section",
                "id"      => FALKON_SHORTNAME . "_pricing_page_id",
                "title"   => __( 'Pricing Page', 'falkon_textdomain' ),
                "desc"    => __( 'Parent archive page of "Resources". Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
        case 'page-ids':
            $options[] = array(
                "section" => "XXXgeneral_ids_section",
                "id"      => FALKON_SHORTNAME . "_features_page_id",
                "title"   => __( 'Features Page', 'falkon_textdomain' ),
                "desc"    => __( 'Parent archive page of "Resources". Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
        case 'page-ids':
            $options[] = array(
                "section" => "XXXgeneral_ids_section",
                "id"      => FALKON_SHORTNAME . "_login_page_url",
                "title"   => __( 'Login Button URL', 'falkon_textdomain' ),
                "desc"    => __( 'URL (off site) of customer login link.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"    => "",
                "class"   => ""
            );
            $options[] = array(
                "section" => "XXXgeneral_ids_section",
                "id"      => FALKON_SHORTNAME . "_free_trial_button_text",
                "title"   => __( 'Free Trial Button Text', 'falkon_textdomain' ),
                "desc"    => __( 'Custom text for button that links to trial sign up page above, default is Free Trial if not specified.',''),
                "type"    => "text",
                "std"    => "Free Trial",
                "class"   => ""
            );
            break;
        case 'page-idsX':
            $options[] = array(
                "section" => "dealer_signup_ids_section",
                "id"      => FALKON_SHORTNAME . "_dealer_signup_stage1_page_id",
                "title"   => __( 'Stage 1', 'falkon_textdomain' ),
                "desc"    => __( 'Contact page of site, used in the theme in the header \'View Our UK Locations\'. ', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
            );
            $options[] = array(
                "section" => "dealer_signup_ids_section",
                "id"      => FALKON_SHORTNAME . "_dealer_signup_stage2_page_id",
                "title"   => __( 'Stage 2', 'falkon_textdomain' ),
                "desc"    => __( 'Contact page of site, used in the theme in the header \'View Our UK Locations\'. ', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
            );
            $options[] = array(
                "section" => "dealer_signup_ids_section",
                "id"      => FALKON_SHORTNAME . "_dealer_signup_stage3_page_id",
                "title"   => __( 'Stage 3', 'falkon_textdomain' ),
                "desc"    => __( 'Contact page of site, used in the theme in the header \'View Our UK Locations\'. ', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
            );
            $options[] = array(
                "section" => "dealer_signup_ids_section",
                "id"      => FALKON_SHORTNAME . "_dealer_signup_stage5_page_id",
                "title"   => __( 'Stage 5 - After GoCardLess', 'falkon_textdomain' ),
                "desc"    => __( 'Contact page of site, used in the theme in the header \'View Our UK Locations\'. ', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
            );
            //Portal Pages
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_page_id",
                "title"   => __( 'Portal Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_page_id",
                "title"   => __( 'Stock Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_draft_page_id",
                "title"   => __( 'Draft Stock Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_sold_page_id",
                "title"   => __( 'Sold Stock Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_add_page_id",
                "title"   => __( 'Add Stock CAP Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_add_make_page_id",
                "title"   => __( 'Add Stock M&M Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_stock_upload_page_id",
                "title"   => __( 'Upload Stock CSV Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_lead_page_id",
                "title"   => __( 'Lead Management Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_my_cars_page_id",
                "title"   => __( 'My Cars Page (customer)', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "portal_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_site_users_page_id",
                "title"   => __( 'Site Users Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );

            //Promo IDs
            $options[] = array(
                "section" => "loginregister_ids_section",
                "id"      => FALKON_SHORTNAME . "_login_page_id",
                "title"   => __( '_login_page_id', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "loginregister_ids_section",
                "id"      => FALKON_SHORTNAME . "_logout_page_id",
                "title"   => __( '_logout_page_id', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "loginregister_ids_section",
                "id"      => FALKON_SHORTNAME . "_register_school_page_id",
                "title"   => __( '_register_school_page_id', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "loginregister_ids_section",
                "id"      => FALKON_SHORTNAME . "_lost_password_page_id",
                "title"   => __( '_lost_password_page_id', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
//            $options[] = array(
//                "section" => "loginregister_ids_section",
//                "id"      => FALKON_SHORTNAME . "_profile_page_id",
//                "title"   => __( '_profile_page_id', 'falkon_textdomain' ),
//                "desc"    => __( '', 'falkon_textdomain' ),
//                "type"    => "select2",
//                "std"    => "",
//                "choices" => $page_array,
//            );


            //Dashboard IDs
            $options[] = array(
                "section" => "dashboard_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_page_id",
                "title"   => __( 'Dashboard Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_profile_overview_page_id",
                "title"   => __( 'Profile Overview Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_ids_section",
                "id"      => FALKON_SHORTNAME . "_profile_page_id",
                "title"   => __( 'Profile Page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of user\'s profile page. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_ids_section",
                "id"      => FALKON_SHORTNAME . "_portal_profile_password_page_id",
                "title"   => __( 'Profile - Password Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );

            //Teacher IDs
            $options[] = array(
                "section" => "dashboard_teacher_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_teacher_overview_page_id",
                "title"   => __( 'Teacher Overview Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_teacher_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_add_teacher_page_id",
                "title"   => __( 'Dashboard Add Teacher Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_teacher_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_view_teacher_page_id",
                "title"   => __( 'Dashboard View Teacher Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );

            //Student IDs
            $options[] = array(
                "section" => "dashboard_student_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_student_overview_page_id",
                "title"   => __( 'Student Overview Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_student_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_add_student_page_id",
                "title"   => __( 'Dashboard Add Student Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_student_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_view_student_page_id",
                "title"   => __( 'Dashboard View Student Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            //Group IDs
            $options[] = array(
                "section" => "dashboard_group_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_group_overview_page_id",
                "title"   => __( 'group Overview Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_group_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_add_group_page_id",
                "title"   => __( 'Dashboard Add group Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_group_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_view_group_page_id",
                "title"   => __( 'Dashboard View group Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );

            //Topic IDs
            $options[] = array(
                "section" => "dashboard_topic_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_topic_overview_page_id",
                "title"   => __( 'Topic Overview Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_topic_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_topic_view_page_id",
                "title"   => __( 'Topic View Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_topic_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_topic_review_page_id",
                "title"   => __( 'Topic Review Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_topic_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_topic_practice_page_id",
                "title"   => __( 'Topic Practice Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );

            //School IDs
            $options[] = array(
                "section" => "dashboard_school_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_school_overview_page_id",
                "title"   => __( 'Dashboard School Overview Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "dashboard_school_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_school_subscription_page_id",
                "title"   => __( 'Dashboard School Subscription Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );


            //General IDs
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_our_work_page_id",
                "title"   => __( 'Our Work Main Parent Page', 'falkon_textdomain' ),
                "desc"    => __( 'Parent archive page of "Our Work". Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_enquiries_page_id",
                "title"   => __( 'View Enquiries page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_enquiry_page_id",
                "title"   => __( 'View Enquiry page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_new_enquiry_page_id",
                "title"   => __( 'New Enquiry page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_enquiries_deleted_page_id",
                "title"   => __( 'View Deleted Enquiries page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_enquiries_historic_page_id",
                "title"   => __( 'View Inactive/Historic Enquiries page', 'falkon_textdomain' ),
                "desc"    => __( 'View enquiries page for inactive & historic enquiries - won, lost and declined basically.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_system_settings_page_id",
                "title"   => __( 'System Settings page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of site System Settings. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );

            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_profile_password_page_id",
                "title"   => __( 'Profile - Password Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_sites_page_id",
                "title"   => __( 'View Sites Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_clients_page_id",
                "title"   => __( 'View Clients Page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_terms_page_id",
                "title"   => __( 'Terms & Conditions page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of site Terms & Conditions. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_terms_use_page_id",
                "title"   => __( 'Terms of Use page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of site Terms & Conditions. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_faqs_page_id",
                "title"   => __( 'FAQs page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of site Terms & Conditions. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_privacy_page_id",
                "title"   => __( 'Privacy Policy page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of site Terms & Conditions. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "general_ids_section",
                "id"      => FALKON_SHORTNAME . "_trailer_page_id",
                "title"   => __( 'Trailers page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of site Trailers page. Will be used in non-editable content which links to that page.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );


            //Results IDs
            $options[] = array(
                "section" => "results_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_results_overview_page_id",
                "title"   => __( 'Results Overview page', 'falkon_textdomain' ),
                "desc"    => __( 'Page of main Overview Results.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "results_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_results_topic_page_id",
                "title"   => __( 'Results by Topic page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            $options[] = array(
                "section" => "results_ids_section",
                "id"      => FALKON_SHORTNAME . "_dashboard_results_user_page_id",
                "title"   => __( 'Results by User page', 'falkon_textdomain' ),
                "desc"    => __( '', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "",
                "choices" => $page_array,
            );
            break;
		case 'our-work':
			$options[] = array(
				"section" => "our_work",
				"id"      => FALKON_SHORTNAME . "_related_title_text",
				"title"   => __( 'Related Motorhome Text', 'prioryrentals' ),
				"desc"    => __( 'Text to go above the related motorhome block for the single motorhome page.', 'prioryrentals' ),
				"type"    => "text",
				"std"     => __(''),
				"class"   => "large-text"
			);
			$options[] = array(
				"section" => "our_work",
				"id"      => FALKON_SHORTNAME . "_related_motorhome_num",
				"title"   => __( 'Related Motorhome Number', 'falkondigital' ),
				"desc"    => __( 'Number of related motorhomes to show on single motorhome page', 'falkon_textdomain' ),
				"type"    => "select",
				"std"    => "3",
				"choices" => array( "3", "6", "9")
			);
			break;
//		case 'social-media':
//            $options[] = array(
//                "section" => "social_header_section",
//                "id"      => FALKON_SHORTNAME . "_multicheckbox_inputs",
//                "title"   => __( 'Multi-Checkbox', 'wptuts_textdomain' ),
//                "desc"    => __( 'You can only choose <strong>9</strong> social links, choose wisely.', 'wptuts_textdomain' ),
//                "type"    => "multi-checkbox-limit",	//variation of the std multi-checkbox, with a limit of allowed ticks
//                "std"     => 9,	//the limit of allowed ticks
//                "choices" => array( __('Facebook','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_fb",
//                    __('Twitter','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_twitter",
//                    __('YouTube','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_youtube",
//                    __('LinkedIn','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_linkedin",
//                    __('Instagram','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_inst",
//                    __('Pinterest','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_pin",
//                    __('Google+','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_google_plus",
//                    __('Vimeo','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_vimeo",
//                    __('Tumblr','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_tumblr",
//                )
//            );
//			$options[] = array(
//				"section" => "social_footer_section",
//				"id"      => FALKON_SHORTNAME . "_url_fb",
//				"title"   => __( 'Facebook URL', 'falkon_textdomain' ),
//				"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
//				"type"    => "text",
//				//"std"     => "",
//				"class"   => "url"
//			);
//			$options[] = array(
//				"section" => "social_footer_section",
//				"id"      => FALKON_SHORTNAME . "_url_twitter",
//				"title"   => __( 'Twitter URL', 'falkon_textdomain' ),
//				"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
//				"type"    => "text",
//				//"std"     => "http://wp.tutsplus.com",
//				"class"   => "url"
//			);
//			$options[] = array(
//				"section" => "social_footer_section",
//				"id"      => FALKON_SHORTNAME . "_url_youtube",
//				"title"   => __( 'YouTube URL', 'falkon_textdomain' ),
//				"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
//				"type"    => "text",
//				//"std"     => "http://wp.tutsplus.com",
//				"class"   => "url"
//			);
//			$options[] = array(
//				"section" => "social_footer_section",
//				"id"      => FALKON_SHORTNAME . "_url_linkedin",
//				"title"   => __( 'LinkedIn URL', 'falkon_textdomain' ),
//				"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
//				"type"    => "text",
//				//"std"     => "http://wp.tutsplus.com",
//				"class"   => "url"
//			);
//			$options[] = array(
//				"section" => "social_footer_section",
//				"id"      => FALKON_SHORTNAME . "_url_inst",
//				"title"   => __( 'Instagram URL', 'falkon_textdomain' ),
//				"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
//				"type"    => "text",
//				//"std"     => "http://wp.tutsplus.com",
//				"class"   => "url"
//			);
//			$options[] = array(
//				"section" => "social_footer_section",
//				"id"      => FALKON_SHORTNAME . "_url_pin",
//				"title"   => __( 'Pinterest URL', 'falkon_textdomain' ),
//				"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
//				"type"    => "text",
//				//"std"     => "http://wp.tutsplus.com",
//				"class"   => "url"
//			);
//			$options[] = array(
//				"section" => "social_footer_section",
//				"id"      => FALKON_SHORTNAME . "_url_google_plus",
//				"title"   => __( 'Google+ URL', 'falkon_textdomain' ),
//				"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
//				"type"    => "text",
//				//"std"     => "http://wp.tutsplus.com",
//				"class"   => "url"
//			);
//            $options[] = array(
//                "section" => "social_footer_section",
//                "id"      => FALKON_SHORTNAME . "_url_vimeo",
//                "title"   => __( 'Vimeo URL', 'falkon_textdomain' ),
//                "desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
//                "type"    => "text",
//                //"std"     => "http://wp.tutsplus.com",
//                "class"   => "url"
//            );
//            $options[] = array(
//                "section" => "social_footer_section",
//                "id"      => FALKON_SHORTNAME . "_url_tumblr",
//                "title"   => __( 'Tumblr URL', 'falkon_textdomain' ),
//                "desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
//                "type"    => "text",
//                //"std"     => "http://wp.tutsplus.com",
//                "class"   => "url"
//            );
//		break;
        case 'social-media':
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_social_multicheckbox_inputs",
                "title"   => __( 'Social Multi-Checkbox', 'wptuts_textdomain' ),
                "desc"    => __( 'You can only show <strong>5</strong> social links in the footer, choose wisely.', 'wptuts_textdomain' ),
                "type"    => "multi-checkbox-limit",	//variation of the std multi-checkbox, with a limit of allowed ticks
                "std"     => 5,	//the limit of allowed ticks
                "choices" => array( __('Facebook','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_fb",
                    __('Twitter','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_twitter",
                    __('YouTube','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_youtube",
                    __('LinkedIn','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_linkedin",
                    __('Instagram','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_inst",
                    __('Pinterest','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_pin",
//                    __('Google+','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_google_plus",
                    __('Vimeo','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_vimeo",
                    __('Tumblr','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_tumblr",
                )
            );
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_fb",
                "title"   => __( 'Facebook URL', 'falkon_textdomain' ),
                //"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_twitter",
                "title"   => __( 'Twitter URL', 'falkon_textdomain' ),
                //"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_youtube",
                "title"   => __( 'YouTube URL', 'falkon_textdomain' ),
                //"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_linkedin",
                "title"   => __( 'LinkedIn URL', 'falkon_textdomain' ),
                //"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_inst",
                "title"   => __( 'Instagram URL', 'falkon_textdomain' ),
                //"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_pin",
                "title"   => __( 'Pinterest URL', 'falkon_textdomain' ),
                //"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_vimeo",
                "title"   => __( 'Vimeo URL', 'falkon_textdomain' ),
                //"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "social_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_tumblr",
                "title"   => __( 'Tumblr URL', 'falkon_textdomain' ),
                //"desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            break;
		case 'contacts':

            $options[] = array(
                "section" => "Xsocial_header_section",
                "id"      => FALKON_SHORTNAME . "_multicheckbox_inputs",
                "title"   => __( 'Multi-Checkbox', 'wptuts_textdomain' ),
                "desc"    => __( 'You can only choose <strong>9</strong> social links, choose wisely.', 'wptuts_textdomain' ),
                "type"    => "multi-checkbox-limit",	//variation of the std multi-checkbox, with a limit of allowed ticks
                "std"     => 9,	//the limit of allowed ticks
                "choices" => array( __('Facebook','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_fb",
                    __('Twitter','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_twitter",
                    __('YouTube','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_youtube",
                    __('LinkedIn','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_linkedin",
                    __('Instagram','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_inst",
                    __('Pinterest','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_pin",
                    __('Google+','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_google_plus",
                    __('Vimeo','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_vimeo",
                    __('Tumblr','wptuts_textdomain') . "|".FALKON_SHORTNAME . "_url_tumblr",
                )
            );
            $options[] = array(
                "section" => "Xsocial_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_fb",
                "title"   => __( 'Facebook URL', 'falkon_textdomain' ),
                "desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "Xsocial_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_twitter",
                "title"   => __( 'Twitter URL', 'falkon_textdomain' ),
                "desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "Xsocial_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_youtube",
                "title"   => __( 'YouTube URL', 'falkon_textdomain' ),
                "desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "Xsocial_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_linkedin",
                "title"   => __( 'LinkedIn URL', 'falkon_textdomain' ),
                "desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "Xsocial_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_inst",
                "title"   => __( 'Instagram URL', 'falkon_textdomain' ),
                "desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "Xsocial_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_pin",
                "title"   => __( 'Pinterest URL', 'falkon_textdomain' ),
                "desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "Xsocial_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_google_plus",
                "title"   => __( 'Google+ URL', 'falkon_textdomain' ),
                "desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "Xsocial_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_vimeo",
                "title"   => __( 'Vimeo URL', 'falkon_textdomain' ),
                "desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );
            $options[] = array(
                "section" => "Xsocial_footer_section",
                "id"      => FALKON_SHORTNAME . "_url_tumblr",
                "title"   => __( 'Tumblr URL', 'falkon_textdomain' ),
                "desc"    => __( 'A text input field which can be used for urls.', 'falkon_textdomain' ),
                "type"    => "text",
                //"std"     => "http://wp.tutsplus.com",
                "class"   => "url"
            );

            $pagepost_list = get_pages();
            $page_array = array();
            array_push($page_array,'Please Select|');
            foreach ( $pagepost_list as $page ) {
                $page_ancestors = get_post_ancestors( $page->ID );
                $level_deep = count($page_ancestors);
                $delim = str_repeat("— ", ($level_deep));
                if($page->post_parent!=0) $page->post_title = $delim.' '.$page->post_title;
                array_push($page_array,$page->post_title.'|'.$page->ID);
            }
//            $options[] = array(
//                "section" => "contact_section",
//                "id"      => FALKON_SHORTNAME . "_contact_number",
//                "title"   => __( 'Primary Contact Number:', 'falkon_textdomain' ),
//                "desc"    => __( 'Primary company contact number, it can be echoed in page content using shortcode \'[company-contact-number]\'.', 'falkon_textdomain' ),
//                "type"    => "text",
//                "std"     => "",
//                "class"   => "text"
//            );
//            $options[] = array(
//                "section" => "contact_section",
//                "id"      => FALKON_SHORTNAME . "_twitter_name",
//                "title"   => __( 'Twitter Name', 'falkon_textdomain' ),
//                "desc"    => __( 'This will display your twitter handle throughout the site', 'falkon_textdomain' ),
//                "type"    => "text",
//                "std"     => "",
//                "class"   => "text"
//            );
//			$options[] = array(
//				"section" => "contact_section",
//				"id"      => FALKON_SHORTNAME . "_sec_contact_number",
//				"title"   => __( 'Secondary Contact Number:', 'falkon_textdomain' ),
//				"desc"    => __( 'Secondary company contact number, shown in the footer with the primary number', 'falkon_textdomain' ),
//				"type"    => "text",
//				"std"     => "",
//				"class"   => "text"
//			);
//            $options[] = array(
//                "section" => "contact_section",
//                "id"      => FALKON_SHORTNAME . "_contact_fax",
//                "title"   => __( 'Website Fax Number:', 'falkon_textdomain' ),
//                "desc"    => __( 'Company fax number - used in the contact us page, and can be echoed in page content using shortcode \'[company-contact-fax]\'.', 'falkon_textdomain' ),
//                "type"    => "text",
//                "std"     => "",
//                "class"   => "text"
//            );
            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_contact_page_map_embed_url",
                "title"   => __( 'Google Map EMBED URL:', 'falkon_textdomain' ),
                "desc"    => __( 'The EMBED URL for the Google map.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "url large-text"
            );
            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_contact_form_title_line",
                "title"   => __( 'Contact Us form Title:', 'falkon_textdomain' ),
                "desc"    => __( 'Title text before the contact form on the Contact Us page.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "Quick Contact Form",
                "class"   => "text large-text"
            );
            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_contact_form_text_line",
                "title"   => __( 'Contact Us form tagline:', 'falkon_textdomain' ),
                "desc"    => __( 'Tagline text before the contact form on the Contact Us page.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text large-text"
            );
            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_contact_form_shortcode",
                "title"   => __( 'Contact Us form shortcode:', 'falkon_textdomain' ),
                "desc"    => __( 'Enter the full Contact Form 7 shortcode to add in the Contact Us contact form.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text code"
            );
            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_contact_number",
                "title"   => __( 'Website Contact Number:', 'falkon_textdomain' ),
                "desc"    => __( 'Company contact number - used in the header, and can be echoed in page content using shortcode \'[company-contact-number]\'.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_contact_fax",
                "title"   => __( 'Website Fax Number:', 'falkon_textdomain' ),
                "desc"    => __( 'Company fax number - used in the contact us page, and can be echoed in page content using shortcode \'[company-contact-fax]\'.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_contact_email",
                "title"   => __( 'Website Contact Email:', 'falkon_textdomain' ),
                "desc"    => __( 'Company contact email - used in the header, and can be echoed in page content using shortcode \'[company-contact-email]\'.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "email"
            );
            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_contact_address",
                "title"   => __( 'Contact Us Address Text', 'falkon_textdomain' ),
                "desc"    => __( 'Add in contact address and details which appear in the footer and the \'Contact Us\' page.', 'falkon_textdomain' ),
                "type"    => "textarea",
                "std"     => "",
                "class"   => "allowlinebreaks"
            );
            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_contact_email",
                "title"   => __( 'Email address:', 'falkon_textdomain' ),
                "desc"    => __( 'Company contact email - used in throughtout the site', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "email"
            );
//			$options[] = array(
//				"section" => "contact_section",
//				"id"      => FALKON_SHORTNAME . "_contact_gmap",
//				"title"   => __( 'Website Google Map:', 'falkon_textdomain' ),
//				"desc"    => __( 'Company google map embed can be echoed in page content using shortcode \'[company-contact_gmap]\'.', 'falkon_textdomain' ),
//				"type"    => "text",
//				"std"     => "",
//				"class"   => "text"
//			);

//			$options[] = array(
//				"section" => "contact_section",
//				"id"      => FALKON_SHORTNAME . "_company_phone_no",
//				"title"   => __( 'Company Number:', 'falkon_textdomain' ),
//				"desc"    => __( 'Enter your company number', 'falkon_textdomain' ),
//				"type"    => "text",
//				"std"     => "",
//				"class"   => "allowlinebreaks"
//			);

            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_opening_hours",
                "title"   => __( 'Company Opening Hours:', 'falkon_textdomain' ),
                "desc"    => __( 'Enter your opening hours in human readable text, e.g. Mon - Fri 8am - 5pm. Don\'t forget to fill in the <a href="'.admin_url('themes.php?page=falkon-settings-page-one&tab=schema-seo').'">Schema</a> option too!.','falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "allowlinebreaks"
            );
//
//            $options[] = array(
//                "section" => "contact_section",
//                "id"      => FALKON_SHORTNAME . "_company_register_no",
//                "title"   => __( 'Company Registered Number:', 'falkon_textdomain' ),
//                "desc"    => __( 'Enter your registered company number', 'falkon_textdomain' ),
//                "type"    => "text",
//                "std"     => "",
//                "class"   => "allowlinebreaks"
//            );
//
//            $options[] = array(
//                "section" => "contact_section",
//                "id"      => FALKON_SHORTNAME . "_company_vat_no",
//                "title"   => __( 'Company VAT Number:', 'falkon_textdomain' ),
//                "desc"    => __( 'Enter your VAT number', 'falkon_textdomain' ),
//                "type"    => "text",
//                "std"     => "",
//                "class"   => "allowlinebreaks"
//            );

            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_company_name",
                "title"   => __( 'Company Name:', 'falkon_textdomain' ),
                "desc"    => __( 'Enter your company name. Will override Blog name in the theme where full name is needed.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "allowlinebreaks"
            );
            $options[] = array(
                "section" => "contact_section",
                "id"      => FALKON_SHORTNAME . "_contact_address",
                "title"   => __( 'Company Address', 'falkondigital' ),
                "desc"    => __( 'Enter the registered company address', 'falkondigital' ),
                "type"    => "textarea",
                "std"     => __('','falkondigital'),
                "class"   => "default"
            );
//            $options[] = array(
//                "section" => "Xcontact_section",
//                "id"      => FALKON_SHORTNAME . "_footer_federation_image",
//                "title"   => __( 'FSB Certification Image', 'falkondigital' ),
//                "desc"    => __( 'Upload your certification image here.', 'falkondigital' ),
//                "type"    => "image",
//                "class"   => "",
//                "maximages"  =>  1,
//                "buttontxt"    =>  "Upload Images",
//            );
//            $options[] = array(
//                "section" => "contact_section",
//                "id"      => FALKON_SHORTNAME . "_company_registrations_link",
//                "title"   => __( 'Company Registrations Link', 'falkon_textdomain' ),
//                "desc"    => __( 'Enter the website URL here(only www.) ', 'falkon_textdomain' ),
//                "type"    => "text",
//                "std"     => "",
//                "class"   => "allowlinebreaks"
//            );
//            $options[] = array(
//                "section" => "contact_section",
//                "id"      => FALKON_SHORTNAME . "_registered_address",
//                "title"   => __( 'Company Address', 'falkondigital' ),
//                "desc"    => __( 'Enter the company address', 'falkondigital' ),
//                "type"    => "textarea",
//                "std"     => __('','falkondigital'),
//                "class"   => "default"
//            );
//
//            $options[] = array(
//                "section" => "contact_section",
//                "id"      => FALKON_SHORTNAME . "_company_contactus_link",
//                "title"   => __( 'Company Contact Us Page', 'falkon_textdomain' ),
//                "desc"    => __( 'Select the page for the websites contact us page', 'falkon_textdomain' ),
//                "type"    => "select2",
//                "std"    => "",
//                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
//            );
//
//            $options[] = array(
//                "section" => "contact_section",
//                "id"      => FALKON_SHORTNAME . "_company_termspage_link",
//                "title"   => __( 'Company terms and conditions page', 'falkon_textdomain' ),
//                "desc"    => __( 'Select the page for the terms and conditions page', 'falkon_textdomain' ),
//                "type"    => "select2",
//                "std"    => "",
//                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
//            );
//			$options[] = array(
//				"section" => "contact_section",
//				"id"      => FALKON_SHORTNAME . "_company_vat_no",
//				"title"   => __( 'Company Vat Number', 'falkon_textdomain' ),
//				"desc"    => __( 'Enter the vat number for the footer', 'falkon_textdomain' ),
//				"type"    => "text",
//				"std"     => "",
//				"class"   => "allowlinebreaks"
//			);
//            $options[] = array(
//                "section" => "contact_section",
//                "id"      => FALKON_SHORTNAME . "_contact_page_id",
//                "title"   => __( 'Contact Us page', 'falkon_textdomain' ),
//                "desc"    => __( 'Contact page of site, used in the backend for now. ', 'falkon_textdomain' ),
//                "type"    => "select2",
//                "std"    => "",
//                "choices" => $page_array//array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
//            );
            $options[] = array(
                "section" => "Xcontact_section",
                "id"      => FALKON_SHORTNAME . "_contact_number",
                "title"   => __( 'Defaut Contact Number:', 'falkon_textdomain' ),
                "desc"    => __( 'Enter a contact number that will appear on invoices etc.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "Xcontact_section",
                "id"      => FALKON_SHORTNAME . "_contact_address",
                "title"   => __( 'Default contact address', 'falkondigital' ),
                "desc"    => __( '', 'falkondigital' ),
                "type"    => "textarea",
                "std"     => __('','falkondigital'),
                "class"   => "text"
            );

            $options[] = array(
                "section" => "pre_footer_section",
                "id"      => FALKON_SHORTNAME . "_company_prefooter_txt",
                "title"   => __( 'Company Pre Footer Text', 'falkon_textdomain' ),
                "desc"    => __( 'Add some text for the pre footer pink footer section', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );

            $options[] = array(
                "section" => "pre_footer_section",
                "id"      => FALKON_SHORTNAME . "_company_prefooter_btn",
                "title"   => __( 'Company Pre Footer Button Text', 'falkon_textdomain' ),
                "desc"    => __( 'Add some button text for the pre footer pink footer section', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );

            $options[] = array(
                "section" => "footer_section",
                "id"      => FALKON_SHORTNAME . "_company_footer_",
                "title"   => __( 'Company Footer Images', 'falkon_textdomain' ),
                "desc"    => __( 'Upload 2 images for the footer - accreditations usually look good!', 'falkon_textdomain' ),
                     "type"    => "image",
                     "class"   => "",
                     "maximages"  =>  2,
                     "buttontxt"    =>  "Upload Images",

            );

            break;


        case 'smtp_email':
            //Email sending settings
            $options[] = array(
                "section" => "email_section",
                "id"      => FALKON_SHORTNAME . "_email_from_name",
                "title"   => __( 'From Name:', 'falkon_textdomain' ),
                "desc"    => __( 'Enter a name that will appear in the "From" field in a users\' email client.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "email_section",
                "id"      => FALKON_SHORTNAME . "_email_from_address",
                "title"   => __( 'From Email Address:', 'falkon_textdomain' ),
                "desc"    => __( 'Enter a from email address that will appear in the "From" field in a users\' email client.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "email_section",
                "id"      => FALKON_SHORTNAME . "_email_default_address",
                "title"   => __( 'Default Email Address:', 'falkon_textdomain' ),
                "desc"    => __( 'Default email for site.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "email_section",
                "id"      => FALKON_SHORTNAME . "_email_sales_address",
                "title"   => __( 'Sales Email Address:', 'falkon_textdomain' ),
                "desc"    => __( 'Default email for sales.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "email_section",
                "id"      => FALKON_SHORTNAME . "_email_bcc_admin",
                "title"   => __( 'Admin Bcc Emails:', 'falkon_textdomain' ),
                "desc"    => __( 'Add a comma-separated list of admin emails addresses which will be added to all automated emails in the Bcc field. (Is not used in Contact From 7 - Use individual form settings)', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text widefat"
            );
            $options[] = array(
                "section" => "Xemail_section",
                "id"      => FALKON_SHORTNAME . "_email_domain",
                "title"   => __( 'Email domain for emails:', 'falkon_textdomain' ),
                "desc"    => __( 'Email domain for emails, used for creating student emails.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text widefat"
            );
            //SMTP Email settings
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_enable",
                "title"   => __( 'Enable SMTP:', 'falkondigital' ),
                "desc"    => __( 'Enable emails to be sent via SMTP?', 'falkon_textdomain' ),
                "type"    => "radio",
                "std"       =>  "1",
                'choices' => array(
                    'Enabled|1',
                    'Disabled|0',
                )
            );
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_host",
                "title"   => __( 'SMTP Host:', 'falkon_textdomain' ),
                "desc"    => __( 'Add the SMTP host IP or name here.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text regular-text code"
            );

            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_auth",
                "title"   => __( 'SMTP Authentication:', 'falkon_textdomain' ),
                "desc"    => __( 'Add the SMTP host IP or name here.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "false",
                "choices" => array( __('True','falkon_textdomain') . "|true", __('False','falkon_textdomain') . "|false")
            );
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_port",
                "title"   => __( 'SMTP Port Number:', 'falkon_textdomain' ),
                "desc"    => __( 'SMTP port number - likely to be 25, 465 or 587', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "25",
                "class"   => "numeric regular-text code"
            );
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_user",
                "title"   => __( 'SMTP username:', 'falkon_textdomain' ),
                "desc"    => __( 'Username to use for SMTP authentication.', 'falkon_textdomain' ),
                "type"    => "text",
                "std"     => "",
                "class"   => "text"
            );
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_pass",
                "title"   => __( 'SMTP password:', 'falkon_textdomain' ),
                "desc"    => __( 'Password to use for SMTP authentication.', 'falkon_textdomain' ),
                "type"    => "password",
                "std"     => "",
                "class"   => "password"
            );
            $options[] = array(
                "section" => "smtp_email_section",
                "id"      => FALKON_SHORTNAME . "_email_smtp_secure",
                "title"   => __( 'SMTP Security Used:', 'falkon_textdomain' ),
                "desc"    => __( 'The encryption system to use - nothing, ssl (deprecated) or tls.', 'falkon_textdomain' ),
                "type"    => "select2",
                "std"    => "tls",
                "choices" => array( __('Nothing','falkon_textdomain') . "|", __('SSL','falkon_textdomain') . "|ssl", __('TLS','falkon_textdomain') . "|tls")
            );
            break;

        case 'homepage-blocks':

            break;
		// Textareas
		case 'homepage-blocks':
			$options[] = array(
				"section" => "xhomepageblocks_section",
				"id"      => FALKON_SHORTNAME . "_block_visible",
				"title"   => __( 'Homepage Blocks Visible?', 'falkon_textdomain' ),
				"desc"    => __( 'Make the block of three homepage items visible on the front end?', 'falkon_textdomain' ),
				"type"    => "select2",
				"std"    => "true",
				"choices" => array( __('Visble','falkon_textdomain') . "|true", __('Not Visible','falkon_textdomain') . "|false")
			);

		break;



	}

	return $options;
}

/**
 * Contextual Help
 */
function falkon_options_two_page_contextual_help() {

	// get the current tab
	$tab = falkon_get_the_tab();

	$text 	= "<h3>" . __('Falkon Digital Theme - Contextual Help','falkon_textdomain') . "</h3>";

	// contextual help according to tab
	switch ($tab) {
		// Text Inputs
		case 'general':
			$text 	.= "<p>" . __('General Settings Page help.','falkon_textdomain') . "</p>";
		break;
        case 'hoemage':
            $text 	.= "<p>" . __('Homepage Settings Page help.','falkon_textdomain') . "</p>";
            break;
		// Textareas
		case 'textareas':
			$text 	.= "<p>" . __('Contextual help for the "Textarea" settings fields goes here.','falkon_textdomain') . "</p>";
		break;

		// Select
		case 'select':
			$text 	.= "<p>" . __('Contextual help for the "Select" settings fields goes here.','falkon_textdomain') . "</p>";
		break;

		// Checkboxes
		case 'checkboxes':
			$text 	.= "<p>" . __('Contextual help for the "Checkboxes" settings fields goes here.','falkon_textdomain') . "</p>";
		break;
	}

	// must return text! NOT echo
	return $text;
} ?>