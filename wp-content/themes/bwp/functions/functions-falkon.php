<?php
/**
 *
 */

//Theme Options Settings
if(is_admin() and ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX )) require_once( trailingslashit(get_stylesheet_directory()).'admin/options/falkon-theme-settings-advanced.php');
if(is_admin() and ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX )) require_once( trailingslashit(get_stylesheet_directory()).'admin/admin.php');

/**
 * Collects falkon theme options
 *
 * @return array
 */
function falkon_get_global_options(){
    $falkon_option = array();
    // collect option names as declared in wptuts_get_settings()
    $falkon_option_names = array (
        'falkon_options_general',
        'falkon_options_homepage',
        'falkon_options_hp_blocks',
        'falkon_options_contacts',
        'falkon_options_login_section',
        'falkon_options_page_ids',
        'falkon_options_case_studies',
        'falkon_options_our_work',
        'falkon_options_social_media',
        'falkon_options_gocardless',
        'falkon_options_smtp_email',
        'falkon_options_schema_seo',

        // 'wptuts_options_two_select',
        // 'wptuts_options_two_checkboxes'
    );
    // loop for get_option
    foreach ($falkon_option_names as $falkon_option_name) {
        if (get_option($falkon_option_name)!= FALSE) {
            $option     = get_option($falkon_option_name);
            // now merge in main $wptuts_option array!
            $falkon_option = array_merge($falkon_option, $option);
        }
    }
    return $falkon_option;
}
/**
 * Call the function and collect in variable
 *
 * Should be used in template files like this:
 * <?php echo $falkon_option['falkon_txt_input']; ?>
 *
 * Note: Should you notice that the variable ($falkon_option) is empty when used in certain templates such as header.php, sidebar.php and footer.php
 * you will need to call the function (copy the line below and paste it) at the top of those documents (within php tags)!
 */
global $falkon_option;
$falkon_option = falkon_get_global_options();


global $falkon_themes_socials_array;
$falkon_themes_socials_array = array(
    'fb'            => 'Facebook',
    'twitter'       => 'Twitter',
    'youtube'       => 'YouTube',
    'linkedin'      => 'LinkedIn',
    'inst'          => 'Instagram',
    'pin'           => 'Pinterest',
    'google_plus'   => 'Google+',
    'vimeo'         => 'Vimeo',
    'tumblr'        => 'Tumblr',
);


/**
 * Filter caption shortcode to remove the extra 10px right width that is added in core function 'img_caption_shortcode'
 * @param $attrs
 * @return mixed
 */
add_filter('shortcode_atts_caption', 'fixExtraCaptionPadding');
function fixExtraCaptionPadding($attrs)
{
    if (!empty($attrs['width'])) {
        $attrs['width'] -= 10;
    }
    return $attrs;
}

add_filter( 'login_headerurl', 'namespace_login_headerurl' );

/**
 * Replaces the login header logo URL
 *
 * @param $url
 */
function namespace_login_headerurl( $url ) {
    $url = home_url( '/' );
    return $url;
}
add_filter( 'login_headertitle', 'namespace_login_headertitle' );
/**
 * Replaces the login header logo title
 *
 * @param $title
 */
function namespace_login_headertitle( $title ) {
    $title = get_bloginfo( 'name' ).(get_bloginfo('description')!=''?' - '.get_bloginfo('description'):'');
    return $title;
}

/**
 * Replaces the login header logo
 */
function namespace_login_style() {
    $login_filename = get_template_directory().'/images/theme-login.png';
    if (file_exists($login_filename)){
        list($img_width, $img_height) = getimagesize($login_filename);
        echo '<style>.login h1 a {
        background-image: url("' . get_template_directory_uri() . '/images/theme-login.png");
        width:'.$img_width.'px !important;
        height:'.$img_height.'px !important;
        background-size: '.$img_width.'px '.$img_height.'px;
        }';
        $login_filename_retina = get_template_directory().'/images/theme-login@2x.png';
        if (file_exists($login_filename_retina)){
            echo '@media all and (-webkit-min-device-pixel-ratio : 1.5),
            all and (-o-min-device-pixel-ratio: 3/2),
            all and (min--moz-device-pixel-ratio: 1.5),
            all and (min-device-pixel-ratio: 1.5) {
            .login h1 a{
            background-image: url("' . get_template_directory_uri() . '/images/theme-login@2x.png");
            }}';
        }
        echo'</style>';
    }
}
add_action( 'login_head', 'namespace_login_style' );

function public_holiday() {
    $date = date('d-m');
    switch($date) {
        case '01-01':
            $message = 'Happy New Year';
            break;
        case '25-12':
            $message = 'Merry Christmas';
            break;
        default:
            $message = 'Welcome';
    }
    return $message;
}
function howdy_message($translated_text, $text, $domain) {
    $message = public_holiday();
    $new_message = str_replace('Howdy', $message, $text);
    return $new_message;
}
add_filter('gettext', 'howdy_message', 10, 3);
/**
 * Admin Footer filter to add in custom Falkon details about theme/site.
 */
function falkon_footer_admin () {
    echo 'Powered by <a href="http://www.wordpress.org" target="_blank" rel="nofollow">WordPress</a> ('.get_bloginfo('version').') | Designed & Built by <a href="http://www.falkondigital.com/?utm_source='.sanitize_title_with_dashes(get_bloginfo('name')).'" target="_blank">Falkon Digital</a> | email <a href="mailto:'.antispambot('info@falkondigital.com',0).'?subject=Help! I need technical support with '.get_bloginfo('name','display').'" target="" rel="nofollow">info@falkondigital.com</a> for support.</p>';
}
add_filter('admin_footer_text', 'falkon_footer_admin');

/*Custom log function to log messages to log file*/
if(!function_exists('_log')){
    function _log( $message ) {
        if( WP_DEBUG === true ){
            if( is_array( $message ) || is_object( $message ) ){
                error_log( print_r( $message, true ) );
            }
            elseif(is_bool( $message )) {
                error_log( ($message?'TRUE':'FALSE') );
            }
            elseif(is_null($message)){
                error_log('!NULL!');
            }
            else
            {
                error_log( $message );
            }
        }
    }
}

function hide_update_notice_to_all_but_admin_users()
{
    if (!current_user_can('update_core')) {
        remove_action( 'admin_notices', 'update_nag', 3 );
    }
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin_users', 1 );

/**
 * Return single array value as not array
 *
 * @param $a
 * @return mixed
 */
function dba_mapusers( $a ){ return $a[0]; }


function flkn_read_more($buttontext='Read More') {
    printf( __( '%1$s', 'twentyten' ),

        sprintf( '<a href="%1$s" title="%2$s" rel="nofollow" class="btn-more btn btn-outline-primary">%3$s</a>',get_permalink(),esc_attr( get_the_title() ),	$buttontext
        )
    );
}
function get_flkn_read_more($buttontext='Read More'){
    return sprintf( '<a href="%1$s" title="%2$s" rel="nofollow" class="btn-more btn btn-outline-primary">%3$s</a>',get_permalink(),esc_attr( get_the_title() ),	$buttontext);
}

function flkn_read_more_small($buttontext='Read More')
{
    printf(__('%1$s', 'twentyten'),

        sprintf('<a href="%1$s" title="%2$s" rel="nofollow" class="btnbtn-more btn-sm btn-outline-primary">%3$s <i class="fa fa-angle-right" aria-hidden="true"></i></a>', get_permalink(), esc_attr(get_the_title()), $buttontext
        )
    );
}


add_action( 'pre_get_posts', function ( $q )
{

    if ( $q->is_main_query() && $q->is_home() ) {
        $q->set('ignore_sticky_posts',true);
    }

});
//add_action( 'pre_get_posts', function ( $q )
//{
//
//    if ( $q->is_main_query() && $q->is_home() ) {
//
//        $count_stickies = count( get_option( 'sticky_posts' ) );
//        $ppp = get_option( 'posts_per_page' );
//        $offset = ( $count_stickies <= $ppp ) ? ( $ppp - ( $ppp - $count_stickies ) ) : $ppp;
//
//        if (!$q->is_paged()) {
//            $q->set('posts_per_page', ( $ppp - $offset ));
//        } else {
//            $offset = ( ($q->query_vars['paged']-1) * $ppp ) - $offset;
//            $q->set('posts_per_page',$ppp);
//            $q->set('offset',$offset);
//        }
//
//    }
//
//});

//add_filter( 'found_posts', function ( $found_posts, $q )
//{
//
//    if( $q->is_main_query() && $q->is_home() ) {
//
//        $count_stickies = count( get_option( 'sticky_posts' ) );
//        $ppp = get_option( 'posts_per_page' );
//        $offset = ( $count_stickies <= $ppp ) ? ( $ppp - ( $ppp - $count_stickies ) ) : $ppp;
//
//        $found_posts = $found_posts + $offset;
//    }
//    return $found_posts;
//
//}, 10, 2 );


function get_relative_permalink( $url ) {
    return str_replace( home_url(), "", $url );
}


function convert_number_to_words($number) {

    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}


function falkon_encode($string,$key) {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    $j = 0;
    $hash = '';
    for ($i = 0; $i < $strLen; $i++) {
        $ordStr = ord(substr($string,$i,1));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
    }
    return $hash;
}
function falkon_decode($string,$key) {
    $key = sha1($key);
    $strLen = strlen($string);
    $keyLen = strlen($key);
    for ($i = 0; $i < $strLen; $i+=2) {
        $ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        $j++;
        $hash .= chr($ordStr - $ordKey);
    }
    return $hash;
}

function is_page_child($pid) {// $pid = The ID of the page we're looking for pages underneath
    global $post;         // load details about this page
    $anc = get_post_ancestors( $post->ID );
    foreach($anc as $ancestor) {
        if(is_page() && $ancestor == $pid) {
            return true;
        }
    }
    if(is_page()&&(is_page($pid)))
        return true;   // we're at the page or at a sub page
    else
        return false;  // we're elsewhere
};