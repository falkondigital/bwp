<?php
/**!
 * Enqueues
 */

if ( ! function_exists('b4st_enqueues') ) {
	function b4st_enqueues() {

		// Styles

//		wp_register_style('bootstrap-css', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css', false, '4.1.1', null);
//		wp_enqueue_style('bootstrap-css');

		//<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i&display=swap" rel="stylesheet">
		//Include theme support for all fonts from Google APIS
		$query_args = array(
			'family'	=>	'Lato:300,300i,400,400i,700,700i,900,900i',
			'subset'	=>	'latin-ext',
			'display'	=>	'swap'
		);
		wp_enqueue_style( 'google_fonts_all', add_query_arg( $query_args, "https://fonts.googleapis.com/css" ), array(), null );


		/* Scripts */
		wp_enqueue_script( 'jquery' );

		wp_enqueue_script('js-theme-apps',get_stylesheet_directory_uri().'/js/apps.js', array('jquery'), filemtime( get_stylesheet_directory().'/js/apps.js'), true);
		//Set Admin Ajax URL
		wp_localize_script( 'js-theme-apps', 'the_ajax_script_app', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

		wp_register_style('theme-css', get_template_directory_uri() . '/css/style.css', false, filemtime( get_template_directory() . '/css/style.css'), null);
		wp_enqueue_style('theme-css');

		// Scripts
		wp_register_script('font-awesome-config-js', get_template_directory_uri() . '/js/font-awesome-config.js', false, null, null);
		wp_enqueue_script('font-awesome-config-js');

		wp_register_script('font-awesome', 'https://use.fontawesome.com/releases/v5.10.2/js/all.js', false, '5.10.2', null);
//		wp_enqueue_script('font-awesome');

		wp_register_style('font-awesome-css', 'https://use.fontawesome.com/releases/v5.10.2/css/all.css', false, '5.10.2', null);
		wp_enqueue_style('font-awesome-css');

		wp_register_script('modernizr',  'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', false, '2.8.3', true);
		wp_enqueue_script('modernizr');

//		wp_register_script('jquery-3.3.1', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, '3.3.1', true);
//		wp_enqueue_script('jquery-3.3.1');

		wp_register_script('popper',  'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', false, '1.14.3', true);
		wp_enqueue_script('popper');

		wp_register_script('bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js', false, '4.1.1', true);
		wp_enqueue_script('bootstrap-js');

		wp_register_script('b4st-js', get_template_directory_uri() . '/js/b4st.js', false, null, true);
		wp_enqueue_script('b4st-js');


		wp_enqueue_script('jquery-owl',get_template_directory_uri().'/plugins/owlcarousel2/owl.carousel.js', array('jquery'), '2.3.4', true );
		wp_register_style('owl-css', get_template_directory_uri() . '/plugins/owlcarousel2/assets/owl.carousel.css', false, '2.3.4', null);
		wp_enqueue_style('owl-css');
		wp_register_style('owl-css-default-theme', get_template_directory_uri() . '/plugins/owlcarousel2/assets/owl.theme.default.min.css', false, '2.3.4', null);
		wp_enqueue_style('owl-css-default-theme');

		//Include Retina JS to replace embedded images with higher ...@2x images for HD displays.
		wp_enqueue_script('jquery-retina',get_template_directory_uri().'/js/retina.js', array('jquery'), '1', true );

		if (is_singular() && comments_open() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}
	}
}
add_action('wp_enqueue_scripts', 'b4st_enqueues', 100);