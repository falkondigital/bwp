<?php
/**
 *
 */


function yourtheme_nav_menu( $theme_location, $class = 'menu' )
{
    if(FALKON_SHOW_TIMERS) $menu_timer = microtime(true);        //TESTING
    $menu = get_transient( 'nav-' . $theme_location );
//    $menu = false; //TESTING - FORCE BUILD NOT TRANSIENT
    if(FALKON_SHOW_TIMERS) $cache = ' C';
    if( $menu === false  or FALKON_USE_TRANSIENTS===false)
    {
        if(FALKON_SHOW_TIMERS) $cache = '';
        if( has_nav_menu( $theme_location ) )
        {
            $menu = wp_nav_menu( array(
                'theme_location'	=> $theme_location,
                'menu_class'		=> $class,
                'fallback_cb' => false,
                'container' => false,
                'echo'            => false,
                'items_wrap'      => '<ul id="%1$s" class="'.$class.' %2$s">%3$s</ul>',

            ) );
            set_transient( 'nav-' . $theme_location, $menu, YEAR_IN_SECONDS );
        }
    }
    if(FALKON_SHOW_TIMERS) echo number_format( microtime(true) - $menu_timer, 5 ).$cache;//TESTING
    return $menu;
}
function yourtheme_nav_menu_args( $theme_location,$menu_args )
{
    if(FALKON_SHOW_TIMERS) $menu_timer = microtime(true);        //TESTING
    $menu = get_transient( 'nav-' . $theme_location );
//    $menu = false; //TESTING - FORCE BUILD NOT TRANSIENT

    if(FALKON_SHOW_TIMERS) $cache = ' C';

    if( $menu === false  or FALKON_USE_TRANSIENTS===false)
    {
        if(isset($menu_args['echo']) or $menu_args['echo']!==false) $menu_args['echo'] = false; //Has to be false to save the string to the DB.
        if(FALKON_SHOW_TIMERS) $cache = '';
        if( has_nav_menu( $theme_location )
            or $theme_location == 'navbar-mobile') //extra hack to make mobile work as cache
        {
            $menu = wp_nav_menu( $menu_args );
            set_transient( 'nav-' . $theme_location, $menu, YEAR_IN_SECONDS );
        }
    }
    if(FALKON_SHOW_TIMERS) echo number_format( microtime(true) - $menu_timer, 5 ).$cache;//TESTING
//    echo "I am from ".$cache;
    return $menu;
}


function yourtheme_invalidate_nav_cache( $menu_id )
{
    $menu_object = wp_get_nav_menu_object( $menu_id );

    $theme_locations = get_registered_nav_menus();
    $locations = array_keys( $theme_locations, $menu_object->name );

    if( $locations )
    {
        foreach( $locations as $location )
        {
            delete_transient( 'nav-' . $location );

            if($location=='navbar-mobile')
                delete_transient( 'nav-navbar' );
            if($location=='navbar')
                delete_transient( 'nav-navbar-mobile' );
        }
    }
    wp_cache_flush();
}
add_action( 'wp_update_nav_menu', 'yourtheme_invalidate_nav_cache',10,1 );

//Fix the caching of the "current/active" menu items (fix - read as "remove"!)
define('FALKON_USE_NAV_MENU_TRANSIENTS', true);
function yourtheme_fix_nav_menu_css_class( $classes, $item, $args  )
{
//    var_dump($args);
    if ( 'footmenu' === $args->menu_class or 'navbarX' === $args->theme_location ) {
        return preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', '', $classes);
    }
    return $classes;
//    if ( 'secondary' === $args->theme_location ) {
}
if( FALKON_USE_NAV_MENU_TRANSIENTS )
{
    add_filter( 'nav_menu_css_class', 'yourtheme_fix_nav_menu_css_class', 10, 3);
}


function falkon_get_transient( $block_location )
{
    if(FALKON_SHOW_TIMERS) $menu_timer = microtime(true);        //TESTING
    $html = get_transient( 'flkn_block-' . $block_location );
//    $html = false; //TESTING - FORCE BUILD NOT TRANSIENT
    if(FALKON_SHOW_TIMERS) $cache = ' C';
    if( $html === false or FALKON_USE_TRANSIENTS===false )
    {
        if(FALKON_SHOW_TIMERS) $cache = '';
        switch($block_location){
            case 'carousel-integrations':
                ob_start();
                ?>
                <?php include(locate_template('includes/blocks/carousel-hp-integrations.php'));?>
                <?php
                $html = ob_get_contents();
                ob_end_clean();
                set_transient( 'flkn_block-' . $block_location, $html, YEAR_IN_SECONDS );
                break;
            case 'carousel-clients':
                ob_start();
                ?>
                <?php include(locate_template('includes/blocks/carousel-hp-brands.php'));?>
                <?php
                $html = ob_get_contents();
                ob_end_clean();
                set_transient( 'flkn_block-' . $block_location, $html, YEAR_IN_SECONDS );
                break;
            case 'carousel-testimonials':
                ob_start();
                ?>
                <?php include(locate_template('includes/blocks/carousel-hp-testimonials.php'));?>
                <?php
                $html = ob_get_contents();
                ob_end_clean();
                set_transient( 'flkn_block-' . $block_location, $html, YEAR_IN_SECONDS );
                break;
            case 'news-homepage':
                ob_start();
                ?>
                <?php include(locate_template('includes/blocks/posts-homepage.php'));?>
                <?php
                $html = ob_get_contents();
                ob_end_clean();
                set_transient( 'flkn_block-' . $block_location, $html, YEAR_IN_SECONDS );
                break;
            case 'news-sidebar':
                $hp_news_num = 1;
                $args = array(
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'posts_per_page' => $hp_news_num,
                    'ignore_sticky_posts'	=> 1,
                    'post__not_in' => get_option( 'sticky_posts' ),
                );
                $recent_posts = new WP_Query( $args );
                ob_start();
                if ( $recent_posts->have_posts() ) :
                    while ( $recent_posts->have_posts() ) : $recent_posts->the_post();
                        include(locate_template('includes/loops/loop-post-sidebar.php'));
                    endwhile;
                endif;
                wp_reset_postdata();
                $html = ob_get_contents();
                ob_end_clean();
                set_transient( 'flkn_block-' . $block_location, $html, YEAR_IN_SECONDS );
                break;
//            case 'resources-index':
            case (preg_match('/resources-index.*/', $block_location) ? true : false):
//                $taxonomy_work_type = get_terms(
//                    array(
//                        'taxonomy' => 'resource-type',
//                        'hide_empty' => false,
//                    )
//                );
//                if(is_array($taxonomy_work_type)){	//get_the_terms will return an array if there are any, otherwise false or wp_error
//                    foreach($taxonomy_work_type as $term) :
//                        array_push($locations,'resource-index-'.$term->slug);
//                    endforeach;
//                }
                if($block_location=='') break;
                ob_start();
                ?>
                <?php include(locate_template('includes/blocks/resources-index.php'));?>
                <?php
                $html = ob_get_contents();
                ob_end_clean();
                set_transient( 'flkn_block-' . $block_location, $html, YEAR_IN_SECONDS );
                break;
            case 'home-brands':
                if ( function_exists( 'falkon_get_global_options' ) ) $falkon_option = falkon_get_global_options();
                $brands_root_id = (int)$falkon_option['falkon_brands_root_page_id'];
                if($brands_root_id!=0){
                    $args = array(
                        'post_type'      => 'page',
                        'posts_per_page' => -1,
                        'post_parent'    => $brands_root_id,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order',
                        'meta_key'      =>  '_page_brand_meta_box_tag_line',
                    );
                    $page_children = new WP_Query( $args );
                    if ( $page_children->have_posts() ) : $post_num = (int)$page_children->post_count;
                        $col_num = floor(12/$post_num);
                        if($col_num<3)
                            $col_num = 3;
                        if($col_num>6)
                            $col_num = 6;
                        ob_start();
                        ?>
                        <div class="container margin-top">
                            <div class="row">
                                <?php while ( $page_children->have_posts() ) : $page_children->the_post(); ?>
                                    <?php
                                    global $page_brand_mb, $post;
                                    $page_brand_meta = $page_brand_mb->the_meta();
                                    $page_brand_text = $page_brand_meta['box_tag_line'];
                                    global $page_mb;
                                    $page_meta = $page_mb->the_meta();
                                    $page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
                                    ?>
                                    <div class="col-sm-<?php echo $col_num;?> brands-block">
                                        <div class="bg-white ">
                                            <a href="<?php the_permalink(); ?>" class="" title="<?php echo esc_attr(sprintf(__("Find out more about %s","admedsol"),get_the_title()));?>">
                                                <img src="<?php echo get_template_directory_uri();?>/images/tmp/brands-<?php echo $post->post_name;?>.jpg" class="">
                                                <img src="<?php echo get_template_directory_uri();?>/images/tmp/brands-<?php echo $post->post_name;?>-brand.jpg" class="">
                                            </a>
                                            <a href="<?php the_permalink(); ?>" class="brand-link" title="<?php echo esc_attr(sprintf(__("Find out more about %s","admedsol"),get_the_title()));?>">
                                                <?php printf(__("Find out more about %s","admedsol"),$page_title);?>
                                            </a>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    <?php endif; wp_reset_query(); ?>
                    <?php
                    $html = ob_get_contents();
                    ob_end_clean();
                }
                set_transient( 'flkn_block-' . $block_location, $html, YEAR_IN_SECONDS );
                break;
        }
    }
    if(FALKON_SHOW_TIMERS) echo number_format( microtime(true) - $menu_timer, 5 ).$cache;//TESTING
    return $html;
}

function falkon_invalidate_block_cache( $post_id, $post, $update)
{
    $post_type = get_post_type($post_id);
    $locations = array();
    if($post_type == 'testimonial'){
        $locations = array('carousel-testimonials');
    }
//    if($post_type == 'resource'){
//        $locations = array('resource-index');
//
//        $taxonomy_work_type = get_terms(
//            array(
//                'taxonomy' => 'resource-type',
//                'hide_empty' => false,
//            )
//        );
//        if(is_array($taxonomy_work_type)){	//get_the_terms will return an array if there are any, otherwise false or wp_error
//            foreach($taxonomy_work_type as $term) :
//                array_push($locations,'resource-index-'.$term->slug);
//            endforeach;
//        }
//
//    }
    if(!empty($locations)) {
        foreach ($locations as $location) {
            delete_transient('flkn_block-' . $location);
            wp_cache_flush();
        }
    }

    $locations = array('news-homepage','news-sidebar','home-brands');
    if( is_array( $locations ) && $locations )
    {
        foreach( $locations as $location )
        {
            delete_transient( 'flkn_block-' . $location );
            wp_cache_flush();
        }
    }
}
add_action( 'save_post', 'falkon_invalidate_block_cache',100,3 );

function falkon_invalidate_transient_block_image_edit($post_id) {
//    global $post;
    _log("ACTION FIRED - falkon_invalidate_transient_block");
    $post_type = get_post_type($post_id) ;
    _log($post_type);
    $locations = array();
    if('attachment' == $post_type){
        $locations = array('carousel-clients','carousel-integrations');
    }
    if(!empty($locations)) {
        foreach ($locations as $location) {
            delete_transient('flkn_block-' . $location);
            wp_cache_flush();
        }
    }
}
add_action('edit_attachment', 'falkon_invalidate_transient_block_image_edit', 100,1);


//
//add_action('added_option', 'falkon_invalidate_transient_block_carousel_clients', 10, 0);
//add_action('updated_option', 'falkon_invalidate_transient_block_carousel_clients', 10, 0);
//
//function falkon_invalidate_transient_block_carousel_clients( $option_name, $option_value ) {
//
//
//}

add_action('added_option', 'falkon_invalidate_transient_block_options_added', 20, 2);
add_action('updated_option', 'falkon_invalidate_transient_block_options_updated', 20, 3);

function falkon_invalidate_transient_block_options_added( $option_name, $option_value ) {

    // do stuff on add_option
    $locations = array();
    if ($option_name == 'falkon_options_homepage') {
        _log("ACTION FIRED - falkon_invalidate_transient_block_options_added");
        _log($option_name);
        $locations = array('news-homepage','carousel-clients','carousel-integrations');
    }
    if(!empty($locations)) {
        foreach ($locations as $location) {
            delete_transient('flkn_block-' . $location);
            wp_cache_flush();
        }
    }
}

function falkon_invalidate_transient_block_options_updated( $option_name, $old_value, $option_value )
{
    // do stuff on update_option
    $locations = array();
    if ($option_name == 'falkon_options_homepage') {
        _log("ACTION FIRED - falkon_invalidate_transient_block_options_updated");
        _log($option_name);
        $locations = array('news-homepage','carousel-clients','carousel-integrations');
    }
    if(!empty($locations)){
        foreach( $locations as $location )
        {
            delete_transient( 'flkn_block-' . $location );
            wp_cache_flush();
        }
    }
}