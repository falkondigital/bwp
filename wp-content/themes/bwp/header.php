<?php
global $falkon_option;
if(FALKON_SHOW_SITE_TIMER){
  global $global_timer;
  $global_timer = microtime(true);        //TESTING
}
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta name="description" content="<?php if ( is_single() ) {
    single_post_title('', true);
  } else {
    bloginfo('name'); echo " - "; bloginfo('description');
  }
  ?>" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php   //Favicon
  $favicon_filename = get_template_directory().'/images/ico/favicon.ico';
  if (file_exists($favicon_filename)){
    $favicon_url = get_template_directory_uri().'/images/ico/favicon.ico';
    echo '<link rel="icon" href="'.$favicon_url.'">';
    echo '<link rel="apple-touch-icon-precomposed" sizes="144x144" href="'.get_template_directory_uri().'/images/ico/apple-touch-icon-144-precomposed.png">';
    echo '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="'.get_template_directory_uri().'/images/ico/apple-touch-icon-114-precomposed.png">';
    echo '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="'.get_template_directory_uri().'/images/ico/apple-touch-icon-72-precomposed.png">';
    echo '<link rel="apple-touch-icon-precomposed" href="'.get_template_directory_uri().'/images/ico/apple-touch-icon-57-precomposed.png">'."\n";
  }
  ?>
  <?php
  if(isset($falkon_option['falkon_theme_colour'])){
    echo '<meta name="theme-color" content="'.(substr($falkon_option['falkon_theme_colour'], 0,1)!='#'?'#'.$falkon_option['falkon_theme_colour']:$falkon_option['falkon_theme_colour']).'" />';
    echo "\n";
  }
  ?>
  <?php
  $company_name = $falkon_option['falkon_company_name']!=''? $falkon_option['falkon_company_name']: get_bloginfo( 'name' );
  $social_links_header = $falkon_option['falkon_multicheckbox_inputs'];
  $sameas = '';
  if(count($social_links_header)>0){
    $sameas = '"sameAs" : [';
    foreach($social_links_header as $social_url => $value){
      if($value and $falkon_option[$social_url]!='')
        $sameas .= '"'.esc_url($falkon_option[$social_url]).'",';
    }
    $sameas = substr($sameas,0,-1);
    $sameas .= ' ]';
  }

  $street_address = '"'.$falkon_option['falkon_schema_location_streetaddress1'].'"'.($falkon_option['falkon_schema_location_streetaddress2']?', "'.$falkon_option['falkon_schema_location_streetaddress2'].'"':'');
  ?>
  <script type="application/ld+json">
		{
		  "@context": {
			"@vocab": "http://schema.org/"
		  },
		  "@graph": [
			{
			  "@id": "<?php echo get_site_url();?>",
			  "@type": "Organization",
			  "name": "<?php echo $company_name;?>",
			  "url" : "<?php echo get_site_url();?>",
			  "logo" : "<?php echo get_stylesheet_directory_uri();?>/images/bwp-logo.png",
			  <?php echo $sameas; ?>
			},
			{
			  "@type": "LocalBusiness",
			  "parentOrganization": {
				  "name" : "<?php echo $company_name;?>"
			  },
			 "name" : "<?php echo $company_name;?>",
			 "image" : "<?php echo get_stylesheet_directory_uri();?>/images/bwp-logo.png",
			  "address": {
				  "@type" : "PostalAddress",
				  <?php if($falkon_option['falkon_schema_location_streetaddress1']) echo '"streetAddress": ['.$street_address.'],'; ?>
				  <?php if($falkon_option['falkon_schema_location_addressLocality']) echo '"addressLocality": "'.$falkon_option['falkon_schema_location_addressLocality'].'",'; ?>
				  <?php if($falkon_option['falkon_schema_location_addressRegion']) echo '"addressRegion": "'.$falkon_option['falkon_schema_location_addressRegion'].'",'; ?>
				  <?php if($falkon_option['falkon_schema_location_postalCode']) echo '"postalCode": "'.$falkon_option['falkon_schema_location_postalCode'].'",'; ?>
				  <?php if($falkon_option['falkon_schema_location_country']) echo '"addressCountry": "'.$falkon_option['falkon_schema_location_country'].'",'; ?>
                  <?php if($falkon_option['falkon_company_phone_no']) echo '"telephone": "'.$falkon_option['falkon_company_phone_no'].'"'; ?>
				  },
			  <?php if($falkon_option['falkon_schema_opening_hours']) echo '"openingHours": [ "'.$falkon_option['falkon_schema_opening_hours'].'" ],'; ?>
			  <?php if($falkon_option['falkon_company_vat_no']) echo '"vatID": "'.$falkon_option['falkon_company_vat_no'].'",'; ?>
			  <?php if($falkon_option['falkon_schema_location_geo']) echo '"geo": "'.$falkon_option['falkon_schema_location_geo'].'",'; ?>
			  <?php if($falkon_option['falkon_schema_has_map']) echo '"hasmap": "'.$falkon_option['falkon_schema_has_map'].'"'; ?>

			}
		  ]
		}
	</script>
  <?php do_action('falkon_hook_before_wp_head');?>
  <?php //include(locate_template('includes/blocks/google-analytics.php'));?>
  <?php wp_head(); ?>
</head>
<body <?php body_class('bg-light'); ?>>
<?php do_action('falkon_hook_after_body');?>
<?php /* ?><header>
  <div id="header">
    <div class="d-sm-none">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar top-bar"></span>
        <span class="icon-bar middle-bar"></span>
        <span class="icon-bar bottom-bar"></span>

      </button>


      <div class="collapse" id="menu">

        <?php
        wp_nav_menu( array(
                'theme_location'    => 'navbar',
                'depth'             => 2,
                'menu_class'        => '',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new b4st_walker_nav_menu())
        );
        ?>
        <ul style="padding-top:0px; padding-right:40px;" class="text-right">
          <li>
            <i class="fa fa-envelope" aria-hidden="true" style="color:#fff;"></i> <a href="mailto:<?php echo antispambot($falkon_option['falkon_contact_email'],1);?>" title="<?php echo antispambot($falkon_option['falkon_contact_email'],0);?>" rel="nofollow">Email Us</a>
          </li>
        </ul>
      </div>

    </div>
    <div class="container">
      <div class="col-xs-12 col-sm-12 col-md-10 text-center">
        <nav class="navbar navbar-default">
          <div class="navbar-header">

            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name','display' ).( get_bloginfo('description')!=''? ' - '.get_bloginfo('description'): '')); ?>" rel="nofollow home" class="navbar-brand">
              <img src="<?php echo get_template_directory_uri(); ?>/images/logo-sarclad.svg"
                   width="241px"
                   height="64px"
                   alt="<?php echo get_bloginfo('name').' '.get_bloginfo('description'); ?>"
                   onerror="this.src='<?php echo get_template_directory_uri(); ?>/images/logo-sarclad.png';this.onerror=null;">
            </a>

          </div>
          <div class="collapse navbar-collapse" id="navbar">
            <?php
            wp_nav_menu( array(
                    'theme_location'    => 'navbar-main',
                    'depth'             => 2,
                    'menu_class'        => 'nav navbar-nav navbar-left',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                    'walker'            => new b4st_walker_nav_menu())
            );
            ?>
            <?php // get_template_part('includes/navbar-search'); ?>
          </div><!-- /.navbar-collapse -->
      </div><!-- /.navbar-collapse -->
      <?php if($falkon_option['falkon_contact_address']!='')
        echo
            '<div style="float:right;" class="hidden-xs hidden-sm">

                    <a href="mailto:'.antispambot($falkon_option['falkon_contact_email'],1).'" title="'.antispambot($falkon_option['falkon_contact_email'],0).'" rel="nofollow"><button class="btn-top">Email Us</button></a>

	           </div>'
        ;?>
    </div>
  </div><!-- // #header -->
</header><?php */ ?>
<?php /* ?><div id="header-topper" class="bg-light">
  <div class="container">
    <div class="row align-items-center py-2">
      <div class="col-12 col-md-6 text-center text-md-left">
        <?php if(get_bloginfo('description')!='') echo '<p class="mb-0 text-uppercase small font-weight-semi-bold text-grayer">'.get_bloginfo('description').'</p>';?>
      </div>
      <div class="col-12 col-md-6 text-center text-md-right">
        <?php if($falkon_option['falkon_contact_number']!='') echo '<p class="mb-0 text-uppercase small font-weight-semi-bold text-light-gray">Call us: '.do_shortcode("[company-contact-number link=true international=true]").'</p>';?>
      </div>
    </div>
  </div>
</div><?php */ ?>
<header id="header-wrap" class="bg-transparent py-0">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-6 col-sm-6 col-md-3 col-lg-3 logo-wrapper text-center text-sm-left">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name','display' ).( get_bloginfo('description')!=''? ' - '.get_bloginfo('description'): '')); ?>" rel="nofollow home">
          <img src="<?php echo get_template_directory_uri(); ?>/images/logo-bwp.svg"
               width="260px"
               height="71px"
               alt="<?php echo get_bloginfo('name').' '.get_bloginfo('description'); ?>"
               onerror="this.src='<?php echo get_template_directory_uri(); ?>/images/logo-bwp.png';this.onerror=null;"
               class="logo"
               data-retina/>
        </a>
      </div>
      <?php /* ?><div class="col-sm-8 col-lg-7 text-center d-none d-lg-block">
        <nav class="navbar navbar-expand-lg ">
          <div class="containerX">
            <div class="collapsex navbar-collapse" id="navbarDropdown">
              <?php
              wp_nav_menu( array(
                  'theme_location'  => 'navbar',
                  'container'       => false,
                  'menu_class'      => '',
                  'fallback_cb'     => '__return_false',
                  'items_wrap'      => '<ul id="%1$s" class="navbar-nav mr-auto mt-0 mt-lg-0 %2$s">%3$s</ul>',
                  'depth'           => 0,
                  'walker'          => new b4st_walker_nav_menu()
              ) );
              ?>
            </div>
          </div>
        </nav>
      </div><?php */ ?>
      <div class="col-6 col-sm-6 col-md-9 col-lg-9 text-right align-self-center">
        <?php //if(isset($falkon_option['falkon_login_page_url']) and $falkon_option['falkon_login_page_url']!='') echo '<a href="'.esc_url($falkon_option['falkon_login_page_url']).'" target="_blank" rel="nofollow" title="Login to sarclad" class="d-none d-md-inline-block btn btn-outline-secondary btn-free-trial">Client Login</a>';?>
        <a href="<?php echo get_permalink($falkon_option['falkon_contact_us_page_id']);?>" class="contact-us-link" title="Contact Us"><i class="fas fa-phone-alt"></i></a>
        <button class=" navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
        </button>
      </div>
    </div>
  </div>
</header>
<nav class="nav-mob container-fluidX">
  <div class="row no-gutters">
    <div class="col-12 col-sm-6 offset-sm-6 col-md-5 offset-md-7"  >
  <div class="collapse" id="menu">

        <?php
        $login_link = '';
        //if(isset($falkon_option['falkon_login_page_url']) and $falkon_option['falkon_login_page_url']!='') $login_link .= '<li><a href="'.esc_url($falkon_option['falkon_login_page_url']).'" target="_blank" rel="nofollow" title="Login to sarclad" class="">Client Login</a></li>';
//        $login_link .= '<li><a href="'.get_permalink($falkon_option['falkon_contact_us_page_id']).'" class="">Contact Us</a></li>';

        if($falkon_option['falkon_contact_number']!='') $login_link .= '<li class="lined"></li><li>'.do_shortcode("[company-contact-number link=true international=true]").'</li>';
        $mob_nav = wp_nav_menu( array(

                'theme_location'  => 'navbar',
                'container'       => 'div',
                'container_class' => '',
                'container_id'    => 'headmenu',
                'menu_class'      => 'mainnav',
                'menu_id'         => 'mobmenu',
                'echo'            => false,
                'fallback_cb'     => false,
                'after'           => '<i class="fa fa-chevron-down subclicker"></i>',
                'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s'.$login_link.'</ul>',
                'depth'           => 0
            )
        );
        echo $mob_nav
        ?>


  </div>
    </div>
  </div>
</nav>
<?php /* ?><a class="navbar-brand" href="<?php echo esc_url( home_url('/') ); ?>"><?php bloginfo('name'); ?></a><?php */ ?>
<?php /* ?><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdown" aria-controls="navbarDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button><?php */ ?>
<?php /* ?><button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar top-bar"></span>
  <span class="icon-bar middle-bar"></span>
  <span class="icon-bar bottom-bar"></span>
</button><?php */ ?>
