<?php
/*
All the functions are in the PHP files in the `functions/` folder.
*/

define('FALKON_USE_TRANSIENTS', true);
define('FALKON_SHOW_TIMERS', false);
define('FALKON_SHOW_SITE_TIMER', false);

//require get_template_directory() . '/functions/cleanup.php';
require get_template_directory() . '/functions/setup.php';
require get_template_directory() . '/functions/enqueues.php';
require get_template_directory() . '/functions/navbar.php';
require get_template_directory() . '/functions/widgets.php';
require get_template_directory() . '/functions/search-widget.php';
require get_template_directory() . '/functions/index-pagination.php';
require get_template_directory() . '/functions/single-split-pagination.php';

//Load common shared Falkon functions
require_once( trailingslashit(get_stylesheet_directory()).'functions/functions-falkon.php');

//Load common shared Falkon Hook functions
require_once( trailingslashit(get_stylesheet_directory()).'functions/functions-hooks.php');

//Load common functions to work with
require_once( trailingslashit(get_stylesheet_directory()).'functions/functions-transients-cache.php');

//Load custom shortcodes for the theme
require_once( trailingslashit(get_stylesheet_directory()).'includes/shortcodes.php');


//Load in WPAlchemy Metabox Class
include_once ( trailingslashit(get_stylesheet_directory()).'metaboxes/setup.php');

include_once 'metaboxes/general_spec.php';
//include_once 'metaboxes/norot_banner-spec.php';

/*Custom posts*/
//require_once( trailingslashit(get_stylesheet_directory()).'includes/custom_posts.php');

/*Testimonials*/
require_once( trailingslashit(get_stylesheet_directory()).'includes/cp_staff.php');

/*Testimonials*/
//require_once( trailingslashit(get_stylesheet_directory()).'includes/cp_testimonials.php');

// Main Slider/Carousel Custom Post
require_once( trailingslashit(get_stylesheet_directory()).'includes/cp_slide.php');

//Load Case Study Post type
require_once( trailingslashit(get_stylesheet_directory()).'includes/cp_case-study.php');

// Remove tags support from posts
function myprefix_unregister_tags() {
    unregister_taxonomy_for_object_type('post_tag', 'post');
}
add_action('init', 'myprefix_unregister_tags');


/**
 * Generate custom search form
 *
 * */
function wpdocs_my_search_form( $form ) {
    $form = '<form role="search" method="get" class="form-inline search-form" action="' . esc_url( home_url( '/' ) ) . '">
                <div class="input-group">
                <label class="h5 mr-sm-2" for="s">
                    <span class="screen-reader-text">' . _x( 'Search the Blog', 'label' ) . '</span>
                </label>
                <input type="search" class="form-control search-field" placeholder="' . esc_attr_x( 'Enter search term', 'placeholder' ) . '" aria-label="' . esc_attr_x( 'Enter search term', 'placeholder' ) . '" value="' . get_search_query() . '" name="s" />
                <div class="input-group-append">
                <button type="submit" class="btn btn-outline-secondary search-submit" value="'. esc_attr_x( 'Search', 'submit button' ) .'" ><i class="fas fa-search"></i></button>
                </div>
                </div>
                <input type="hidden" name="post_type" value="post">
            </form>';
return $form;
}
add_filter( 'get_search_form', 'wpdocs_my_search_form' );



/**
 * Responsive Youtube embeds.
 */
add_filter( 'embed_oembed_html', function( $html, $url, $attr, $post_ID ) {
    if ( false !== stripos( $html, '<iframe ' ) && false !== stripos( $html, '.youtube.com/embed' ) ) {
        $html = sprintf('<div class="embed-responsive embed-responsive-16by9">%s</div>', $html );
    }
    else if( false !== stripos( $html, '<iframe ' ) && false !== stripos( $html, 'player.vimeo.com' ) ) {
        $html = sprintf('<div class="embed-responsive embed-responsive-16by9">%s</div>', $html );
    }

    return $html;
}, 10, 4);