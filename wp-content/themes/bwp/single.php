<?php get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php

    $post_categories = wp_get_post_categories($post->ID);
    $cats = array();
    $cat_string = '';
    foreach($post_categories as $c){
        $cat = get_category( $c );
        $cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
        $cat_string .= '<span >'.$cat->name.'</span>, ';
    }
    $cat_string = substr($cat_string, 0, -2);
//var_dump($cats);
$page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
//$page_title_tag_line = get_the_date().' | '.$cat_string.' | Posted by '.get_the_author_posts_link();
//$view_all_news_link = get_permalink(get_option('page_for_posts'));
$page_title_tag_line = '<a href="'.get_permalink(get_option('page_for_posts')).'">'.get_the_title(get_option('page_for_posts')).'</a><span> - <span>'.$cat_string.'</span></span>';

//          _e('Posted by ', 'b4st');
//          get_the_author_posts_link();
//          _e(' on ', 'b4st');
//          b4st_post_date();


if(has_post_thumbnail()){
    $jumbo_img_src = wp_get_attachment_image_src(get_post_thumbnail_id(),$size = 'full');//get_template_directory_uri() . '/images/banner-blog.jpg';
    if(is_array($jumbo_img_src))
        $jumbo_img_src = $jumbo_img_src[0];
}
else{
//    $blog_page_id = get_option('page_for_posts');
//    global $page_mb;
//    $page_meta = $page_mb->the_meta();
//    $page_title = $page_meta['custom_title']? $page_meta['custom_title']: get_the_title();
//    $page_title_tag_line = $page_meta['custom_title_tag_line']? $page_meta['custom_title_tag_line']: false;
    $jumbo_img_src = get_template_directory_uri() . '/images/banner-blog.jpg';
}
endwhile;
endif;
?>
<div class="jumbotron jumbotron-fluid text-white" style="background-image: url('<?php echo $jumbo_img_src;?>');">
    <div class="shadow"></div>
    <div class="container text-center">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2">
                <p class="h1 text-white"><?php echo $page_title;?></p>
            </div>

        <?php
        if($page_title_tag_line!==false){
            echo '<div class="w-100"></div>';
            echo '<div class="col-12">';
            echo '<p class="h5 text-white font-weight-normal header-meta">'.$page_title_tag_line.'</p>';
            echo '</div>';
        }
        ?>
        </div>
    </div>
</div>
<main class="container mt-5 bg-light mb-8">
  <div class="row">
    <div class="col-sm-8">
      <div id="content" role="main">
        <?php get_template_part('loops/single-post', get_post_format()); ?>
      </div><!-- /#content -->
    </div>
  <?php get_sidebar(); ?>
  </div><!-- /.row -->
</main><!-- /.container -->
<?php
if(function_exists('related_posts')){
    related_posts();
}
?>
<?php include(locate_template('includes/blocks/final-cta.php'));?>
<?php
if ( comments_open() || get_comments_number() ) :
?>
    <div class="bg-light">
<div class="container py-4">
  <div class="row"><div class="col">
    <?php
//comments_template();
comments_template('/loops/single-post-comments.php');
    ?>
    </div>
  </div><!-- /.row -->
</div><!-- /.container -->
    </div>
    <?php
endif;
?>
<?php get_footer(); ?>
